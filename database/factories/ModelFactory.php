<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Theme::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'theme_folder' => $faker->word,
        'templates' => $faker->paragraph
    ];
});


$factory->define(App\Element::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'subtitle' => $faker->word,
        'excerpt' => $faker->paragraph,
        'body' => $faker->paragraph,

    ];
});

$factory->define(App\Website::class, function (Faker\Generator $faker) {
    return [
        'theme_id' => 1,
        'default_language_id' => 2,
        'name' => $faker->name
    ];
});


$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'language_id' => 1,
        'name' => $faker->title,
        'slug' => $faker->word
    ];
});
