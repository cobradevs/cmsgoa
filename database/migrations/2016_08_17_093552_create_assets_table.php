<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->default(0);
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('website_id')->unsigned()->default(0);
            $table->string('name');
            $table->string('original_name')->nullable();
            $table->string('title')->nullable();
            $table->string('path')->nullable();
            $table->string('thumb_path')->nullable();
            $table->string('type', 30)->nullable();
            $table->tinyInteger('is_published')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assets');
    }
}
