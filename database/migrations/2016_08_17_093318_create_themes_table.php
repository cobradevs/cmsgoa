<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('themes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('folder')->nullable();
            $table->string('screenshot')->nullable();
            $table->string('screenshot_thumbnail')->nullable();
            $table->string('datasource', 50)->nullable();
            $table->text('templates')->nullable();
            $table->tinyInteger('rank')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('themes');
    }
}
