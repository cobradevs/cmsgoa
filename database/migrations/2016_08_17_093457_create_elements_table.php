<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('website_id')->unsigned()->default(0);
            $table->integer('language_id')->unsigned()->default(0);
            $table->integer('category_id')->unsigned()->default(0);
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->string('slug')->nullable();
            $table->text('excerpt')->nullable();
            $table->text('body')->nullable();
            $table->string('image', 255)->nullable()->default(null);
            $table->string('thumb', 255)->nullable()->default(null);
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->text('other_meta_tags')->nullable();
            $table->integer('read_count')->unsigned()->default(0);
            $table->integer('rank')->unsigned()->default(0);
            $table->tinyInteger('status')->default(0);
            $table->date('published_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('elements');
    }
}
