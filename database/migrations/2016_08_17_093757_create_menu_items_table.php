<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned()->default(0);
            $table->foreign('menu_id')->references('id')->on('menus');
            $table->integer('parent_id')->unsigned()->default(0);
            $table->integer('page_id')->unsigned()->default(0);
            $table->string('external_url')->nullable();
            $table->string('title');
            $table->tinyInteger('type')->unsigned()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu_items');
    }
}
