<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id')->unsigned()->default(0);
            $table->foreign('page_id')->references('id')->on('pages');
            $table->integer('block_type_id')->unsigned()->default(0);
            $table->string('region', 50);
            $table->string('name');
            $table->text('data')->nullable();
            $table->string('link')->nullable();
            $table->string('style')->nullable();
            $table->tinyInteger('rank')->unsigned()->default(0);
            $table->tinyInteger('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blocks');
    }
}
