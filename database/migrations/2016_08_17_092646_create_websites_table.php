<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('websites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('theme_id')->unsigned()->default(0);
            $table->integer('default_language_id')->unsigned()->default(0);
            $table->integer('homepage_id')->unsigned()->default(0);
            $table->string('name', 50);
            $table->string('logo')->nullable();
            $table->string('image')->nullable();
            $table->text('additional_header')->nullable();
            $table->tinyInteger('news_per_page')->unsigned()->default(10);
            $table->tinyInteger('news_latest_limit')->unsigned()->default(5);
            $table->tinyInteger('news_mostread_limit')->unsigned()->default(5);
            $table->string('news_order', 4)->default("ASC");
            $table->tinyInteger('is_live')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('websites');
    }
}
