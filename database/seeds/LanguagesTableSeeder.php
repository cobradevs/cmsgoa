<?php

use App\Language;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages_data_csv = "Global,Global,en,en,1;English (UK),United Kingdom,en_GB,gb,1;Spanish,Spain,es,es,1;Italian,Italy,it,it,1;French,France,fr,fr,0;Russian,Russia,ru,ru,0;German,German,gr,gr,0;Arabic,,ar,ar,0;Bulgarian,Bulgaria,bg,bg,0;Catalan,Spain,ca,ca,0;Chinese,China,zh,zh,0;Croatian,Croatia,hr,hr,0;Czech,Czech Republic,cs,cs,0;Danish,Danish,ds,ds,0;Dutch,Netherlands,nl,nl,1;English (US),United States,en_US,us,1;English (ZA),South Africa,en_ZA,za,0;English (SG),Singapore,en_SG,sg,0;English (PH),Philippines,en_PH,ph,0;English (NZ),New Zealand,en_NZ,nz,1;English (MT),Malta,en_MT,en-mt,0;English (IE),Ireland,en_IE,ie,0;English (IN),India,en_IN,en-in,1;English (CA),Canada,en_CA,en-ca,1;Estonian,Estonia,et,et,0;Finnish,Finland,fi,fi,0;Greek,Greece,el,el,1;Hebrew,Isreal,iw,iw,0;Hindi,India,hi_IN,hi,0;Hungarian,Hungary,hu,hu,0;Icelandic,Iceland,is,is,0;Indonesian,Indonesia,in,in,0;Japanese,Japan,ja,ja,0;Korean,Korea,ko,ko,0;Latvian,Latvia,lv,lv,0;Lithuanian,Lithuania,lt,lt,0;Macedonian,Macedonia,mk,mk,0;Malay,Malaysia,ms,ms,0;Maltese,Malta,mt,mt,0;Norwegian,Norway,no,no,0;Polish,Poland,pl,pl,1;Portuguese,Prtugal,pt,pt,0;Romanian,Romania,ro,ro,0;Serbian,Serbia,sr,sr,0;Slovak,Slovakia,sk,sk,0;Slovenian,Slovenia,sl,sl,0;Swedish,Sweden,sv,sv,1;Thai,Thailand,th,th,0;Turkish,Turkey,tr,tr,0;Ukrainian,Ukraine,uk,uk,0;Vietnamese,Vietnam,vi,vi,0;English (AU),Australia,en_AU,au,1";
        $languages_data_array = explode(";", $languages_data_csv);
        foreach ($languages_data_array as $language_data) {
        	$language_data_array = explode(",", $language_data);
        	Language::create([
        		'name' => $language_data_array[0],
        		'country' => $language_data_array[1],
        		'locale' => $language_data_array[2],
        		'slug' => $language_data_array[3],
        		'is_active' => $language_data_array[4]
        	]);
        }
    }
}
