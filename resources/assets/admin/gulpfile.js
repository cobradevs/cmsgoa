var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.less('app.less', 'css')

    .styles([
		'../bower_components/jquery-ui/themes/smoothness/jquery-ui.css',
		'bs3-theme/bootstrap.css',
		'../bower_components/datatables.net-bs/css/dataTables.bootstrap.css',
		'../bower_components/datatables.net-responsive-bs/css/responsive.bootstrap.css',
		'../bower_components/sweetalert/dist/sweetalert.css',
		'../bower_components/ekko-lightbox/dist/ekko-lightbox.css',
		'ekkolightbox-theme-dark.css',
		'../bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css',
		'../bower_components/bootstrap-toggle/css/bootstrap-toggle.css',
		'../bower_components/summernote/dist/summernote.css',
		// '../bower_components/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
		'../bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
		'app.css',
	])

    .scripts([
		'../bower_components/jquery/dist/jquery.js',
		'../bower_components/jquery-ui/jquery-ui.js',
		'../bower_components/moment/moment.js',
		'../bower_components/bootstrap/dist/js/bootstrap.js',
		'../bower_components/datatables.net/js/jquery.dataTables.js',
		'../bower_components/datatables.net-bs/js/dataTables.bootstrap.js',
		'../bower_components/datatables.net-responsive/js/dataTables.responsive.js',
		'../bower_components/datatables.net-responsive-bs/js/responsive.bootstrap.js',
		'../bower_components/sprintf/src/sprintf.js',
		'../bower_components/sweetalert/dist/sweetalert.min.js',
		'../bower_components/ekko-lightbox/dist/ekko-lightbox.js',
		'../bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect.js',
		'../bower_components/bootstrap-toggle/js/bootstrap-toggle.js',
		'../bower_components/summernote/dist/summernote.js',
		'../bower_components/summernote-image-title/summernote-image-title.js',
		'../bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
		'app.js',
	]);

});
