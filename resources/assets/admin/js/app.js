$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});

$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

var populateSelectbox = function(target, target_name, url, default_value) {
	target.attr("disabled", true);
	target.find("option").remove();
	target.append('<option value=null>Loading...</option>');
	$.ajax({ 
		url: url,
		success: function(data){
			if(data == ""){
				target.find("option[value=null]").text("There are no results for this selection");
			}else{
				target.append('<option value="">Please select a ' + target_name + '</option>')
				$.each(data, function(key, value) 
				{
					option = '';
					option = option + '<option value=' + key;
					if(key == default_value) option = option + ' selected="selected"';
					option = option + '>' + value + '</option>';
				    target.append(option);
				});
				target.find("option[value=null]").remove();
				target.attr("disabled", false);
			}
		}
	});
}

var sendFile = function(file, el) {
	var form_data = new FormData();
	form_data.append('file', file);
	$.ajax({
		data: form_data,
		type: "POST",
		url: '/admin/assets/upload_image',
		cache: false,
		contentType: false,
		enctype: 'multipart/form-data',
		processData: false,
		success: function(data) {
			if(data.status == "failed"){
				var errors = data.errors;
				errorsList = "Upload error(s): \n\n";
				$.each(errors, function(key, value){
				    errorsList += "> " + value[0] + "\n";
				});
				alert(errorsList);
			}else{
				$(el).summernote('editor.insertImage', data);
			}
		},
		error: function(data) {

		}
	});
}

var cleanPastedHTML = function(input){
	// 1. remove line breaks / Mso classes
	var stringStripper = /(\n|\r| class=(")?Mso[a-zA-Z]+(")?)/g;
	var output = input.replace(stringStripper, ' ');
	// 2. strip Word generated HTML comments
	var commentSripper = new RegExp('<!--(.*?)-->','g');
	var output = output.replace(commentSripper, '');
	var tagStripper = new RegExp('<(/)*(meta|link|span|\\?xml:|st1:|o:|font)(.*?)>','gi');
	// 3. remove tags leave content if any
	output = output.replace(tagStripper, '');
	// 4. Remove everything in between and including tags '<style(.)style(.)>'
	var badTags = ['style', 'script','applet','embed','noframes','noscript'];
	for (var i=0; i< badTags.length; i++) {
	tagStripper = new RegExp('<'+badTags[i]+'.*?'+badTags[i]+'(.*?)>', 'gi');
	output = output.replace(tagStripper, '');
	}
	// 5. remove attributes ' style="..."'
	var badAttributes = ['style', 'start'];
	for (var i=0; i< badAttributes.length; i++) {
	var attributeStripper = new RegExp(' ' + badAttributes[i] + '="(.*?)"','gi');
	output = output.replace(attributeStripper, '');
	}
	return output;
}

var countChar = function(val, limit, id) {
	var len = val.value.length;
	if (len >= limit) {
	  	val.value = val.value.substring(0, limit);
	} else {
		$('.'+id).css('opacity', 1);
	  	$('#'+id).text(limit - len);
	}
}

var showMediaLibrary = function(button) {
	if(button.data("target")){
		var targetElementID = button.data("target");
	}else{
		var targetElementID = button.closest(".note-editor").siblings("textarea:hidden").attr('id');
	}
	$('#' + targetElementID + 'MediaLibrary').modal("show");
}

var mediaLibraryButton = function (context) {
	var ui = $.summernote.ui;
	var button = ui.button({
	contents: '<i class="glyphicon glyphicon-folder-open"></i>',
	tooltip: 'Media Library',
	click: function () {
			showMediaLibrary($(this));
		}
	});
	return button.render();   // return button as jquery object 
}

var processSelectedFile = function(file, requestingField){
	var fileElement = $('#' + requestingField);
	var a = $('<a>', {href: file.url});
	var fileFullPath = a.prop('pathname');
    if (fileElement.is("input")) {
        $('#' + requestingField).val(file.path);
    } else if (fileElement.is("textarea")) {
    	$('#' + requestingField).summernote('editor.insertImage', fileFullPath);
    	$('img[src="' + fileFullPath + '"]').prop("alt", file.name).prop("title", file.name);
    }
}

var setTemplateFields = function(){
	$('.region-list').sortable();
    $('.region-list').disableSelection();
	$('.template').on("click", ".btn-add-region", function(){
    	$(this).closest('.panel-body').find('.region-list li').last().clone().appendTo($(this).closest('.panel-body').find('.region-list')).find('input').val('New Region');
    });
    $('.region-list').on("click", ".btn-delete-region", function(){
    	$(this).closest('li').remove();
    });
    $('.template-list').on("click", ".btn-delete-template", function(){
    	$(this).closest('.template').remove();
    });
}

var fillBlocksData = function(block_meta, block_data){
	var block_html = $('<div class="block">').attr({
		"data-block_id": block_data.block_id,
	})
	.append(
		$('<div class="block-toolbar pull-right small text-muted">')
			.append(
				$('<button type="button" class="glyphicon glyphicon-edit button-edit-block">').attr({
					"data-toggle": "modal",
					"data-target": "#addBlockModal",
					"data-block_id": block_data.block_id,
					"data-website_id": block_meta.website_id,
					"data-template": block_meta.template,
					"data-region_id": block_meta.region_id,
					"data-region_title": block_meta.region_title
				})
			)
			.append(
				$('<button type="button" class="glyphicon glyphicon-remove button-delete-block">')
			)
	)
	.append(
		$('<div class="block-content">').html(sprintf(block_type_template[block_meta.block_type], {block_data: block_data, block_meta: block_meta}))
	)
	.append(
		$('<input type="hidden">').attr({
		    name: 'blocks[' + block_meta.template +'][' + block_meta.region_id +'][]',
		    value: JSON.stringify(block_data)
		})
	)
	// if( (block_data.block_id) && $("div.block[data-block_id='" + block_data.block_id + "']").length && block_meta.action=="update" )
	if( block_meta.action=="update" )
		$("div.block[data-block_id='" + block_data.block_id + "']").replaceWith(block_html);
	else
		$("div[data-template='" + block_meta.template + "'][data-region_id='" + block_meta.region_id + "'] .panel-body").append(block_html);

	$(".block-list").sortable({
		placeholder: 'well tile',
        forceHelperSize: true
	});
}

$(document).ready(function(){

    $('.row').on("click", ".btn-delete-warning", function(e){
        e.preventDefault();
        delete_action = $(this).data('action');
        swal({
           title: "Are you sure?",
           text: "This item will be deleted and this action cannot be undone!",
           type: "warning",
           showCancelButton: true,
           confirmButtonText: "Yes, delete!",
           cancelButtonText: "Cancel",
           closeOnConfirm: false
        }, function(response){
            if(response==true){
                $("#form-delete").attr("action", delete_action);
                $("#form-delete").submit();
            }
        });
    });

    $(".media-library-button").click(function(){
    	showMediaLibrary($(this));
    });

    $("select[multiple=multiple]").multiselect({
    	inheritClass: true
    });

    $('.datepicker').datetimepicker({
    	format: "DD.MM.YYYY"
    });

    $(".textarea-editor").summernote({
    	height: 400,
    	toolbar: [
		    ['style', ['style']],
		    ['misc', ['undo', 'redo']],
		    ['font', ['bold', 'italic', 'underline', 'clear']],
		    ['font2', ['strikethrough', 'superscript', 'subscript']],
		    ['para', ['ul', 'ol', 'paragraph']],
		    ['insert2', ['table', 'hr']],
		    ['insert', ['link', 'medialibrary']],
		    ['misc2', ['codeview']],
		],
		imageTitle: {
          specificAltField: true,
        },
        popover: {
            image: [
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['custom', ['imageTitle']],
                ['remove', ['removeMedia']],
            ],
        },
		buttons: {
			medialibrary: mediaLibraryButton
		},
		dialogsInBody: true,
		callbacks: {
			onImageUpload: function(files){
				for (var i = files.length - 1; i >= 0; i--) {
					sendFile(files[i], $(this));
				}
			},
			onPaste: function(e) {
	            var thisNote = $(this);
	            var updatePastedText = function(someNote){
	                var original = someNote.summernote("code");
	                var cleaned = cleanPastedHTML(original); //this is where to call whatever clean function you want. I have mine in a different file, called CleanPastedHTML.
	                someNote.summernote("code", cleaned); //this sets the displayed content editor to the cleaned pasted code.
	            };
	            setTimeout(function () {
	                updatePastedText(thisNote);
	            }, 10);
	        }
		}
    });

    $('.template-list').on("click", ".btn-add-template", function(){
    	template_count++;
    	new_template = $('.template-list .row div.template').last().clone().appendTo('.template-list .row');
    	new_template.find(".panel-heading input").attr("name", "templates["+template_count+"][name]").val("New Template");
    	new_template.find(".region-list input").attr("name", "templates["+template_count+"][regions][]").val("New Region");
    	setTemplateFields();
    });
    setTemplateFields();

    $('.theme-option-list').on("click", ".btn-add-theme-option", function(){
    	option_count++;
    	new_theme_option = $('.theme-option-list div.theme-option').last().clone().appendTo('.theme-option-list');
    	new_theme_option.find("input").attr("name", "options["+option_count+"][name]").val("");
    	new_theme_option.find("select").attr("name", "options["+option_count+"][type]");
    });
    $('.theme-option-list').on("click", ".btn-delete-theme-option", function(){
    	$(this).closest('.theme-option').remove();
    });

    $(".pages-list ul, .menu_items-list ul").sortable({
		toleranceElement: '> div',
		handle: '.glyphicon',
		update: function (event, ui) {
	        var sorted_data = {};
        	sorted_data['parent_id'] = $(this).data('parent_id');
        	sorted_data['items'] = $(this).sortable('toArray');
        	$("input[name='list_order[" + $(this).data('parent_id') + "]']").val($(this).sortable('toArray'));
	        console.log(JSON.stringify(sorted_data));
	    }
	});
	$(".pages-list ul, .menu_items-list ul").disableSelection();

	$(".pages-list-toggler").change(function(){
		if($(this).prop('checked'))
			$($(this).data('target')).slideDown();
		else
			$($(this).data('target')).hide();
	});
	$(".pages-list-toggler").trigger("change");

	$(".template-picker").change(function(){
		template = $(this).val();
		$(".page-regions").addClass("hidden").hide();
		$(".page-regions[rel='"+template+"']").removeClass("hidden").slideDown();
	});
	$(".template-picker:checked").trigger("change");

	$("#addBlockModal").on("show.bs.modal", function (event) {
		var button = $(event.relatedTarget);
		var website_id = button.data("website_id");
		var template = button.data("template");
		var region_id = button.data("region_id");
		var region_title = button.data("region_title");
		var modal = $(this);
		if(website_id) modal.find("input[name='website_id']").val(website_id);
		if(template) modal.find("input[name='template']").val(template);
		if(region_id) modal.find("input[name='region_id']").val(region_id);
		if(region_title) modal.find("span[rel='region_title']").text(region_title);
		if(button.hasClass("button-edit-block")){
			modal.find("input[name='block_action']").val("update");
			modal.find("input[name='block_id']").val(button.data("block_id"));
			modal.find(".modal-title").text("Edit Block");
			modal.find(".button-add-block-content").text("Update");
			modal.find(".elements-list *[name^='elements-list']").prop('checked', false);
			var block_data = jQuery.parseJSON(button.parent().siblings("input[type='hidden']").val());
			$.each( block_data, function( key, value ) {
				if(key == "block_type"){
					block_type = value;
					modal.find(".block-type-picker").val(block_type);
					modal.find(".block-type-picker").trigger("change");
				}else{
					modal.find("*[name='" + block_type + "-" + key + "']").val(value);
					if(block_type == "elements" && key == "category") elements_category = value;
					if(block_type == "elements" && key == "list"){
						$.each(value, function( index, element_id ) {
							modal.find(".elements-list *[name='" + block_type + "-" + key + "[" + elements_category + "][" + element_id + "]']").prop('checked', true);
						});
					}
					modal.find(".textarea-editor[name='" + block_type + "-" + key + "']").summernote('code', value);
				}
			});
			modal.find("#elements-category").trigger("change");
		}else{
			modal.find("input[name='block_action']").val("insert");
			modal.find("input[name='block_id']").val($.now());
			modal.find(".modal-title").text("Add Block");
			modal.find(".button-add-block-content").text("Insert");
			modal.find("form")[0].reset();
			modal.find(".textarea-editor").summernote("code", "");
			modal.find(".block-type-picker").trigger("change");
			modal.find("#elements-category").trigger("change");
		}
	});

	$(".page-regions").on("click", ".button-delete-block", function(){
		$(this).closest(".block").remove();
	});

	$('#addBlockModal').on('click', '.button-add-block-content', function(){
		block_meta = {};
		block_meta.website_id = $("input[name='website_id']").val();
		block_meta.template = $("input[type='hidden'][name='template']").val();
		block_meta.region_id = $("input[name='region_id']").val();
		block_meta.region_title = $("span[rel='region_title']").text();
		block_meta.block_type = $("select[name='block_type']").val();
		block_meta.action = $("input[name='block_action']").val();

		block_data = {};
		block_data.block_type = block_meta.block_type;
		block_type_fields[block_meta.block_type].forEach(function(field, index){
			switch (field.type) { 
				case 'elements_category':
					block_data[field.id] = $(".block-fields[rel='" + block_meta.block_type + "'] [name='" + block_meta.block_type + "-" + field.id + "']").val();
					block_data[field.id + '_text'] = $(".block-fields[rel='" + block_meta.block_type + "'] [name='" + block_meta.block_type + "-" + field.id + "'] option:selected").text();
				break;
				case 'elements_list': 
					data = [];
					data_text = [];
					category_id = $(".block-fields[rel='" + block_meta.block_type + "'] [name='elements-category']").val();
					$(".block-fields[rel='" + block_meta.block_type + "'] [name^='" + block_meta.block_type + "-" + field.id + "[" + category_id + "]']").each(function(){
						if($(this).is(':checked')){
							data.push( $(this).data("element_id") );
							data_text.push( $(this).data("element_title") );
						}
					});
					block_data[field.id] = data;
					block_data[field.id + '_text'] = data_text;
				break;
				case 'page': 
					block_data[field.id] = $(".block-fields[rel='" + block_meta.block_type + "'] [name='" + block_meta.block_type + "-" + field.id + "']").val();
					block_data[field.id + '_text'] = $(".block-fields[rel='" + block_meta.block_type + "'] [name='" + block_meta.block_type + "-" + field.id + "']  option:selected").text();
				break;
				default:
					block_data[field.id] = $(".block-fields[rel='" + block_meta.block_type + "'] [name='" + block_meta.block_type + "-" + field.id + "']").val();
			}
		});
		block_data.block_id = $("input[name='block_id']").val();

		fillBlocksData(block_meta, block_data);

		$('#addBlockModal').modal('hide');
	});

	$('#addBlockModal').on('change', '.block-type-picker', function(){
		block_type = $(this).val();
		$(".block-fields").addClass("hidden").hide();
		$(".block-fields[rel='"+block_type+"']").removeClass("hidden").slideDown();
	});
	$(".block-type-picker").trigger("change");

	$('#addBlockModal').on('change', '#elements-category', function(){
		category = $(this).val();
		$(".elements-list").addClass("hidden").hide();
		$(".elements-list[rel='"+category+"']").removeClass("hidden").slideDown();
	});
	$("#elements-category").trigger("change");

	$('body').tooltip({
	    selector: '[data-toggle="tooltip"]'
	});

});
