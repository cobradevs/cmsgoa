var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    mix.less('app.less', 'css')

    .styles([
		'../bower_components/bootstrap/dist/css/bootstrap.css',
		'app.css',
	])

    .scripts([
		'../bower_components/jquery/dist/jquery.js',
		'../bower_components/bootstrap/dist/js/bootstrap.js',
		'app.js',
	]);

});
