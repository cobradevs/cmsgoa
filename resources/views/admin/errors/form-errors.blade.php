@if(isset($errors) && $errors->any())
	<div class="alert alert-danger fade in alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p><strong>Your action could not be completed because of the following error{{ ($errors->count()>1) ? "s" : "" }}:</strong></p>
		<ul>
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif