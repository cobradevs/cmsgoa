@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

        	@include("Admin::errors.form-errors")

            <div class="panel panel-default">
                <div class="panel-heading">
                	<h2 class="panel-title">Settings</h2>
                </div>

                <div class="panel-body">

	                @include("Admin::errors.form-errors")

	                {!! Form::open(['action' => ['Admin\WebsiteThemeOptionController@save', $website->id, $language->id], 'method' => 'POST']) !!}

					@foreach(json_decode($website->theme->options) as $option_data)
						<?php
						$option = $option_data->option;
						switch($option->type){
							case("text"):
							?>
		        		<div class="form-group">
		        		    {!! Form::label($option->id, $option->name) !!}
		        		    {!! Form::textarea($option->id, $option_values[$option->id], ['class' => 'form-control']) !!}
		        		</div>
							<?php
							break;
							case("file"):
							?>
	            		<div class="form-group">
	            		    {!! Form::label($option->id, $option->name) !!} <button type="button" class="media-library-button" data-target="{{ $option->id }}"><span class="glyphicon glyphicon-folder-open"></span></button>
	            		    {!! Form::text($option->id, $option_values[$option->id], ['class' => 'form-control']) !!}
							@include("Admin::partials.media-library-popup", ["field_id" => $option->id])

	            		</div>
             		<!-- Alt Field -->
	                		<div class="form-group">
	                		    {!! Form::label('alttext', 'Alt-text:') !!}
	                		    {!! Form::text('alttext', null, ['class' => 'form-control']) !!}
	                		</div>
							<?php
							break;
							case("string"):
							default:
							?>
		        		<div class="form-group">
		        		    {!! Form::label($option->id, $option->name) !!}
		        		    {!! Form::text($option->id, $option_values[$option->id], ['class' => 'form-control']) !!}
		        		</div>
							<?php
							break;
						}
						?>
					@endforeach

					<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>

					{!! Form::close() !!}

                </div>
            </div>

        </div>
    </div>
</div>
@endsection

