@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Category's Details: {{ $category->name }}</div>

                <div class="panel-body">
                	<p>Name: {{ $category->name }}</p>
                	<p>Language: {{ $category->language->name }}</p>
                	<p>Slug: {{ $category->slug }}</p>
                </div>

                <div class="panel-footer">
        			{!! action_buttons('edit', $category, 'Category') !!}
        			{!! action_buttons('delete', $category, 'Category') !!}
            		<div class="pull-right">
                		<a href="{{ action('Admin\CategoryController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>  Back to Categories List</a>
                	</div>
	                {!! Form::open(['action' => ['Admin\CategoryController@destroy', $category->id], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!}{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
