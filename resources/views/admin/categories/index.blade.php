@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	
        	@include("Admin::errors.form-errors")

            <div class="panel panel-default">
                <div class="panel-heading">
                	<div class="pull-right">{!! action_buttons('create', null, 'Category') !!}</div>
                	<h2 class="panel-title">Categories</h2>
                </div>
            </div>

			<table class="table table-striped" id="categories-table">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Language</th>
					<th class="text-right"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</tfoot>
			</table>
			{!! Form::open(['action' => ['Admin\CategoryController@destroy', 0], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!} {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
    $('#categories-table').DataTable({
        ajax: '{!! action('Admin\CategoryController@data') !!}',
        order: [[1, 'asc']],
        @include("Admin::partials.datatables-options")
        @include("Admin::partials.datatables-filters", ['columns' => "[2]"])
        columns: [
        	{ data: 'id', name: 'categories.id', 'className': 'all' },
            { data: 'name', name: 'categories.name', 'className': 'all' },
            { data: 'language.name', name: 'language.name', 'className': 'min-tablet' },
            { data: 'action', name: 'action', 'className': 'text-right text-nowrap all', 'orderable': false, 'searchable': false }
        ]
    });
});
</script>
@endpush
