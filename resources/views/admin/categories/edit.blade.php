@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit a Category: {{ $category->name }}</div>

                <div class="panel-body">

                	@include("Admin::errors.form-errors")

                	{!! Form::model($category, ['action' => ['Admin\CategoryController@update', $category->id], 'method' => 'PUT', 'files' => true]) !!}

                		<!-- Language Field -->
                		<div class="form-group">
                		    {!! Form::label('language_id', 'Language:') !!}
                		    {!! Form::select('language_id', $languages, null, ['class' => 'form-control']) !!}
                		</div>

                		<!-- Name Field -->
                		<div class="form-group">
                		    {!! Form::label('name', 'Name:') !!}
                		    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                		</div>

                		<!-- Slug Field -->
						<p class="small"><a role="button" data-toggle="collapse" href="#slugField" aria-expanded="false" aria-controls="slugField">Change slug</a></p>
						<div class="collapse" id="slugField">
							<div class="form-group">
							    {!! Form::label('slug', 'Slug:') !!}
                		    	{!! Form::text('slug', null, ['class' => 'form-control']) !!}
							</div>
						</div>
						@if(strlen(old('slug'))>0)
							@push('scripts')
								<script type="text/javascript">
							    $(document).ready(function(){
							    	$('#slugField').collapse('show');
							    });
								</script>
							@endpush
						@endif

	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\CategoryController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to Categories List</a>
	                	</div>

                	{!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
