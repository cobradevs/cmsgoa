@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add a New Category</div>

                <div class="panel-body">
                	
                	@include("Admin::errors.form-errors")

                	{!! Form::open(['action' => 'Admin\CategoryController@store', 'files' => true]) !!}
                		<!-- Language Field -->
                		<div class="form-group">
                		    {!! Form::label('language_id', 'Language:') !!}
                		    {!! Form::select('language_id', $languages, old('language_id'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Name Field -->
                		<div class="form-group">
                		    {!! Form::label('name', 'Name:') !!}
                		    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
                		</div>
	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\CategoryController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to Categories List</a>
	                	</div>
                	{!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
