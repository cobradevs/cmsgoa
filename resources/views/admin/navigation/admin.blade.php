<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Users <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{{ action('Admin\UserController@index') }}"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> List Users</a></li>
        <li><a href="{{ action('Admin\UserController@create') }}"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Add a New User</a></li>
    </ul>
</li>

<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Websites <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{{ action('Admin\WebsiteController@index') }}"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> List Websites</a></li>
        <li><a href="{{ action('Admin\WebsiteController@create') }}"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Add a New Website</a></li>
        <li><a href="{{ action('Admin\ThemeController@index') }}"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Themes</a></li>
        <li><a href="{{ action('Admin\LanguageController@index') }}"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span> Languages</a></li>
    </ul>
</li>

<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Elements <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{{ action('Admin\ElementController@index') }}"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> List Global Elements</a></li>
        <li><a href="{{ action('Admin\ElementController@create') }}"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Add a New Global Element</a></li>
        <li><a href="{{ action('Admin\CategoryController@index') }}"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> Element Categories</a></li>
    </ul>
</li>
