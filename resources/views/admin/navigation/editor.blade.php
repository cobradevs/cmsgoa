<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Pages <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{{ action('Admin\WebsitePageController@index', [$website->id, $language->id]) }}"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> List Pages</a></li>
        <li><a href="{{ action('Admin\WebsitePageController@create', [$website->id, $language->id]) }}"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Add a New Page</a></li>
    </ul>
</li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Elements <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{{ action('Admin\WebsiteElementController@index', [$website->id, $language->id]) }}"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> List Elements</a></li>
        <li><a href="{{ action('Admin\WebsiteElementController@create', [$website->id, $language->id]) }}"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Add a New Element</a></li>
    </ul>
</li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        News <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{{ action('Admin\WebsiteNewsController@index', [$website->id, $language->id]) }}"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> List News</a></li>
        <li><a href="{{ action('Admin\WebsiteNewsController@create', [$website->id, $language->id]) }}"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Add News</a></li>
    </ul>
</li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Menus <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
    	@foreach(menus() as $menu_id => $menu_name)
        <li><a href="{{ action('Admin\WebsiteMenuItemController@index', [$website->id, $language->id, $menu_id]) }}"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span> {{ $menu_name }}</a></li>
    	@endforeach
    </ul>
</li>
<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        Website <span class="caret"></span>
    </a>
    <ul class="dropdown-menu" role="menu">
        @if(count(json_decode($website->theme->options)))
        <li><a href="{{ action('Admin\WebsiteThemeOptionController@index', [$website->id, $language->id]) }}" role="button" aria-expanded="false">Settings</a></li>
        @endif
        <li><a href="{{ action('Admin\WebsiteAssetController@index', [$website->id, $language->id]) }}" role="button" aria-expanded="false">Media Library</a></li>
    </ul>
</li>
