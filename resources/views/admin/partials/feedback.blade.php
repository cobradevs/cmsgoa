@if(session()->has('status.type') && session()->has('status.message'))

    <script>
        swal({
            title:"{!! session('status.title') !!}",
            text: "{!! session('status.message') !!}",
            type: "{!! session('status.type') !!}",
            timer: 3250,
            allowOutsideClick: true,
            showConfirmButton: true,
            confirmButtonText: 'OK',
        });
    </script>

@endif
