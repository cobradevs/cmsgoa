							<div id="{{ $field_id }}MediaLibrary" class="modal fade media-library" tabindex="-1" role="dialog">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-body" style="min-height:640px;">
											<iframe src="/admin/website/{{ $website->id or 0 }}/{{ $language->id or 0 }}/elfinder/popup/{{ $field_id }}" width="99.6%" height="640" frameborder="0"></iframe>
										</div>
									</div><!-- /.modal-content -->
								</div><!-- /.modal-dialog -->
							</div><!-- /.modal -->
