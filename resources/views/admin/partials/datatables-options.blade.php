        processing: true,
        serverSide: true,
        responsive: true,
        language: {
        	paginate: {
        		previous: "&laquo;",
        		next: "&raquo;"
        	},
        	emptyTable: 'No records found'
        },
		drawCallback: function(settings) {
			var pagination = $(this).closest('.dataTables_wrapper').find('.dataTables_paginate');
			pagination.toggle(this.api().page.info().pages > 1);
		},
