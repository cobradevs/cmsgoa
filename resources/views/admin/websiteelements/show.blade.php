@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ str_singular($item_title) }} Details: {{ $element->title }}</div>

                <div class="panel-body">
                	<p>Title: {{ $element->title }}</p>
                	<p>Subtitle: {{ $element->subtitle }}</p>
                	@if(!$is_news)
                	<p>Category: {{ $element->category->name }}</p>
                	@endif
                	<p>Excerpt: {{ $element->excerpt }}</p>
                	<p>Image: <a href="{{ $element->image }}" data-toggle="lightbox">{{ $element->image }}</a></p>
                	<p>Alt Text: {{ $element->alttext }}</p>
                	<p>Thumb: {{ $element->thumb }}</p>
                	<p>Body: {{ $element->body }}</p>
                	<p>Meta Title: {{ $element->meta_title }}</p>
                	<p>Meta Description: {{ $element->meta_description }}</p>
                	<p>Other Meta Tags: {{ $element->other_meta_tags }}</p>
                	<p>Read Count: {{ $element->read_count }}</p>
                	<p>Rank: {{ $element->rank }}</p>
                	<p>Date: {{ $element->published_date }}</p>
                	<p>Is active: {{ boolean_text($element->status) }}</p>
                </div>

                <div class="panel-footer">
        			{!! action_buttons('edit', [$website->id, $language->id, $element->id], 'Website'.$item_title) !!}
        			{!! action_buttons('delete', [$website->id, $language->id, $element->id], 'Website'.$item_title) !!}
            		<div class="pull-right">
                		<a href="{{ action('Admin\Website'.$item_title.'Controller@index', [$website->id, $language->id]) }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>  Back to {{ str_plural($item_title) }} List</a>
                	</div>
	                {!! Form::open(['action' => ['Admin\Website'.$item_title.'Controller@destroy', $website->id, $language->id, $element->id], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!}{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
