@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	
        	@include("Admin::errors.form-errors")

            <div class="panel panel-default">
                <div class="panel-heading">
                	<div class="pull-right">{!! action_buttons('create', [$website->id, $language->id], 'Website'.$item_title) !!}</div>
                	<h2 class="panel-title">{{ str_plural($item_title) }}</h2>
                </div>
            </div>

			<table class="table table-striped table-hover" id="elements-table">
			<thead>
				<tr>
					<th>#</th>
					<th>Title</th>
					@if($is_news)
					<th>Date</th>
					@else
					<th>Category</th>
					@endif
					<th class="text-right"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</tfoot>
			</table>
			{!! Form::open(['action' => ['Admin\Website'.$item_title.'Controller@destroy', $website->id, $language->id, 0], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!} {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
    $('#elements-table').DataTable({
        ajax: '{!! action('Admin\Website'.$item_title.'Controller@data', [$website->id, $language->id]) !!}',
        order: [[1, 'asc']],
        @include("Admin::partials.datatables-options")
        @if(!$is_news)
        @include("Admin::partials.datatables-filters", ['columns' => "[2]"])
        @endif
        columns: [
        	{ data: 'id', name: 'elements.id', 'className': 'all' },
            { data: 'title', name: 'title', 'className': 'all' },
            @if($is_news)
            { data: 'published_date', name: 'published_date', 'className': 'min-desktop' },
            @else
            { data: 'category.name', name: 'category.name', 'className': 'min-desktop' },
            @endif
            { data: 'action', name: 'action', 'className': 'text-right text-nowrap all', 'orderable': false, 'searchable': false }
        ]
    });
});
</script>
@endpush
