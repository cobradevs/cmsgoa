@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add {{ str_singular($item_title) }}</div>

                <div class="panel-body">

                	@if(!$is_news && count($categories)<=1)

						<div class="alert alert-warning">
							<p>There is no element categories set for <strong>{{ $language->name }}</strong>, therefore you cannot add elements to this website.</p>
							<p>Contact the system administrator and ask them to add element categories for <strong>{{ $language->name }}</strong>.</p>
						</div>

                	@else

	                	@include("Admin::errors.form-errors")

	                	{!! Form::open(['action' => ['Admin\Website'.$item_title.'Controller@store', $website->id, $language->id], 'files' => true]) !!}
	                		@if(!$is_news)
	                		<!-- Category Field -->
	                		<div class="form-group">
	                		    {!! Form::label('category_id', 'Category:') !!}
	                		    {!! Form::select('category_id', $categories, old('category_id'), ['class' => 'form-control']) !!}
	                		</div>
	                		@endif
	                		<!-- Title Field -->
	                		<div class="form-group">
	                		    {!! Form::label('title', 'Title:') !!}
	                		    {!! Form::text('title', old('title'), ['class' => 'form-control']) !!}
	                		</div>
	                		<!-- Subtitle Field -->
	                		<div class="form-group">
	                		    {!! Form::label('subtitle', 'Subtitle:') !!}
	                		    {!! Form::text('subtitle', old('subtitle'), ['class' => 'form-control']) !!}
	                		</div>
	                		<!-- Excerpt Field -->
	                		<div class="form-group">
	                		    {!! Form::label('excerpt', 'Excerpt:') !!}
	                		    {!! Form::textarea('excerpt', old('excerpt'), ['class' => 'form-control textarea-sm']) !!}
	                		</div>
	                		<!-- Image Field -->
	                		<div class="form-group">
	                		    {!! Form::label('image', 'Image:') !!} <button type="button" class="media-library-button" data-target="image"><span class="glyphicon glyphicon-folder-open"></span></button>
	                		    {!! Form::text('image', old('image'), ['class' => 'form-control']) !!}
								@include("Admin::partials.media-library-popup", ["field_id" => "image"])
	                		</div>
	                		<!-- Alt Field -->
	                		<div class="form-group">
	                		    {!! Form::label('alttext', 'Alt-text:') !!}
	                		    {!! Form::text('alttext', null, ['class' => 'form-control']) !!}
	                		</div>
	                		<!-- Content Field -->
	                		<div class="form-group">
	                		    {!! Form::label('body', 'Content:') !!}
	                		    {!! Form::textarea('body', old('body'), ['class' => 'form-control textarea-lg textarea-editor']) !!}
								@include("Admin::partials.media-library-popup", ["field_id" => "body"])
	                		</div>
	                		<fieldset>
	                			<legend class="scheduler-border">SEO</legend>
		                		<!-- Meta Title Field -->
	                            <div class="form-group">
	                                {!! Form::label('meta_title', 'Meta Title:') !!}
	                                {!! Form::text('meta_title', old('meta_title'), ['class' => 'form-control', 'onkeyup' => 'countChar(this, 60, "titleChars")']) !!}

	                                <small class="form-text text-muted titleChars" style="opacity:0;"><span id="titleChars"></span> characters remaining.</small>
	                            </div>
	                            <!-- Meta Description Field -->
	                            <div class="form-group">
	                                {!! Form::label('meta_description', 'Meta Description:') !!}
	                                {!! Form::textarea('meta_description', old('meta_description'), ['class' => 'form-control',  'onkeyup' => 'countChar(this, 120, "DescChars")', 'size' => '30x3']) !!}
	                                <small class="form-text text-muted DescChars" style="opacity:0;"><span id="DescChars"></span> characters remaining.</small>
	                            </div>
		                		<!-- Other Meta Tags Field -->
		                		<div class="form-group">
		                		    {!! Form::label('other_meta_tags', 'Other Meta Tags:') !!}
		                		    {!! Form::textarea('other_meta_tags', old('other_meta_tags'), ['class' => 'form-control textarea-sm']) !!}
		                		</div>
	                		</fieldset>
	                		<!-- Read Count Field -->
	                		<div class="form-group">
	                		    {!! Form::label('read_count', 'Read Count:') !!}
	                		    {!! Form::number('read_count', old('read_count'), ['class' => 'form-control']) !!}
	                		</div>
	                		<!-- Rank Field -->
	                		<div class="form-group">
	                		    {!! Form::label('rank', 'Rank:') !!}
	                		    {!! Form::number('rank', old('rank'), ['class' => 'form-control']) !!}
	                		</div>
	                		<!-- Date Field -->
	                		<div class="form-group">
	                		    {!! Form::label('published_date', 'Date:') !!}
	                		    {!! Form::text('published_date', old('published_date', \Carbon\Carbon::now()->format('d.m.Y')), ['class' => 'form-control datepicker']) !!}
	                		</div>
	                		<!-- Is active Field -->
	                		<div class="form-group">
	                		    {!! Form::checkbox('status', 1, old('status', true), ['data-toggle' => "no-toggle", 'id' => "status"]) !!}
	                		    {!! Form::label('status', 'Is active') !!}
	                		</div>
		                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
		                	<div class="pull-right">
		                		<a href="{{ action('Admin\Website'.$item_title.'Controller@index', [$website->id, $language->id]) }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to {{ str_plural($item_title) }} List</a>
		                	</div>
	                	{!! Form::close() !!}
	                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
