@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ studly_case($save_type) }} {{ str_singular($item_title) }}: {{ $element->title }}</div>

                <div class="panel-body">

                	@include("Admin::errors.form-errors")

					@if($save_type=="duplicate")
                		{!! Form::model($element, ['action' => ['Admin\Website'.$item_title.'Controller@store', $website->id, $language->id], 'method' => 'POST', 'files' => true]) !!}
                	@else
						{!! Form::model($element, ['action' => ['Admin\Website'.$item_title.'Controller@update', $website->id, $language->id, $element->id], 'method' => 'PUT', 'files' => true]) !!}
                	@endif

                		@if(!$is_news)
                		<!-- Category Field -->
                		<div class="form-group">
                		    {!! Form::label('category_id', 'Category:') !!}
                		    {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
                		</div>
                		@endif
                		<!-- Title Field -->
                		<div class="form-group">
                		    {!! Form::label('title', 'Title:') !!}
                		    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- Subtitle Field -->
                		<div class="form-group">
                		    {!! Form::label('subtitle', 'Subtitle:') !!}
                		    {!! Form::text('subtitle', null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- Excerpt Field -->
                		<div class="form-group">
                		    {!! Form::label('excerpt', 'Excerpt:') !!}
                		    {!! Form::textarea('excerpt', null, ['class' => 'form-control textarea-sm']) !!}
                		</div>
                		<!-- Image Field -->
                		<div class="form-group">
                		    {!! Form::label('image', 'Image:') !!} <button type="button" class="media-library-button" data-target="image"><span class="glyphicon glyphicon-folder-open"></span></button>
                		    {!! Form::text('image', null, ['class' => 'form-control']) !!}
							@include("Admin::partials.media-library-popup", ["field_id" => "image"])
                		</div>
                		<!-- Alt Field -->
                		<div class="form-group">
                		    {!! Form::label('alttext', 'Alt-text:') !!}
                		    {!! Form::text('alttext', null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- Content Field -->
                		<div class="form-group">
                		    {!! Form::label('body', 'Content:') !!}
                		    {!! Form::textarea('body', null, ['class' => 'form-control textarea-lg textarea-editor']) !!}
							@include("Admin::partials.media-library-popup", ["field_id" => "body"])
                		</div>
                		<fieldset>
                			<legend class="scheduler-border">SEO</legend>
                            <!-- Meta Title Field -->
                            <div class="form-group">
                                {!! Form::label('meta_title', 'Meta Title:') !!}
                                {!! Form::text('meta_title', old('meta_title'), ['class' => 'form-control', 'onkeyup' => 'countChar(this, 60, "titleChars")']) !!}

                                <small class="form-text text-muted titleChars" style="opacity:0;"><span id="titleChars"></span> characters remaining.</small>
                            </div>
                            <!-- Meta Description Field -->
                            <div class="form-group">
                                {!! Form::label('meta_description', 'Meta Description:') !!}
                                {!! Form::text('meta_description', old('meta_description'), ['class' => 'form-control',  'onkeyup' => 'countChar(this, 120, "DescChars")', 'size' => '30x3']) !!}
                                <small class="form-text text-muted DescChars" style="opacity:0;"><span id="DescChars"></span> characters remaining.</small>
                            </div>
	                		<!-- Other Meta Tags Field -->
	                		<div class="form-group">
	                		    {!! Form::label('other_meta_tags', 'Other Meta Tags:') !!}
	                		    {!! Form::textarea('other_meta_tags', null, ['class' => 'form-control textarea-sm']) !!}
	                		</div>
                		</fieldset>
                		<!-- Read Count Field -->
                		<div class="form-group">
                		    {!! Form::label('read_count', 'Read Count:') !!}
                		    {!! Form::number('read_count', null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- Rank Field -->
                		<div class="form-group">
                		    {!! Form::label('rank', 'Rank:') !!}
                		    {!! Form::number('rank', null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- Date Field -->
                		<div class="form-group">
                		    {!! Form::label('published_date', 'Date:') !!}
                		    {!! Form::text('published_date', null, ['class' => 'form-control datepicker']) !!}
                		</div>
                		<!-- Is active Field -->
                		<div class="form-group">
                		    {!! Form::checkbox('status', 1, null, ['data-toggle' => "no-toggle", 'id' => "status"]) !!}
                		    {!! Form::label('status', 'Is active') !!}
                		</div>
	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\Website'.$item_title.'Controller@index', [$website->id, $language->id]) }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to {{ str_plural($item_title) }} List</a>
	                	</div>

                	{!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
