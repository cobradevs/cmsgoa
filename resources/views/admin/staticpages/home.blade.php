@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body text-center">
                    Welcome to CMS GOA!
                    @if(isset($websites) && count($websites))
						<h2>Pick website</h2>
						<div class="row">
						@foreach ($websites as $each_website)
						<div class="col-xs-6">
							<fieldset>
							<legend>{{ $each_website->name }}</legend>
							@foreach($each_website->languages as $each_language)
							<a href="{{ action('Admin\WebsitePageController@index', [$each_website->id, $each_language->id]) }}" type="button" class="btn btn-default <?= $each_website->is_live ? 'live':'staging'; ?>">{{ $each_language->name }}</a>
							@endforeach
							</fieldset>
						</div>
						@endforeach
						</div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
