@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	
        	@include("Admin::errors.form-errors")

            <div class="panel panel-default">
                <div class="panel-heading">
                	<div class="pull-right">{!! action_buttons('create', [$website->id, $language->id], 'WebsitePage') !!}</div>
                	<h2 class="panel-title">Pages</h2>
                </div>
            </div>

			@if($pages and count($pages))

				{!! Form::open(['action' => ['Admin\WebsitePageController@save_order', $website->id, $language->id]]) !!}

				<div class="pages-list">
					{!! html_list($pages, "page") !!}
				</div>

				<div class="pages-list-options">
					<strong>Options: &nbsp; &nbsp; </strong>
					<label class="checkbox-inline">
						<input type="checkbox" class="pages-list-toggler" checked data-toggle="toggle" data-on="Show" data-off="Hide" data-size="mini" data-target=".pages-list .draft"> <span class="text-warning">Drafts</span>
					</label>
					&nbsp; &nbsp; 
					<label class="checkbox-inline">
						<input type="checkbox" class="pages-list-toggler" data-toggle="toggle" data-on="Show" data-off="Hide" data-size="mini" data-target=".pages-list .trash"> <span class="text-muted">Deleted pages</span>
					</label>
				</div>

				<button type="submit" name="submit" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save Page Order</button>

				{!! Form::close() !!}

			@endif

        </div>
    </div>
</div>
@endsection

