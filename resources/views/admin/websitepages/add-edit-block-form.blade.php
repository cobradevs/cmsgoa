					<div class="modal fade" id="addBlockModal" tabindex="-1" role="dialog" aria-labelledby="addBlockModalLabel">
					  <div class="modal-dialog modal-lg" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title" id="addBlockModalLabel">Add Block</h4>
					      </div>
					      <div class="modal-body">
					        <form>
					          <div class="form-group">
					            <label for="region" class="control-label">Region:</label>
					            <span rel="region_title"></span>
					            {!! Form::hidden("website_id", $website->id) !!}
					            {!! Form::hidden("template") !!}
					            {!! Form::hidden("region_id") !!}
					            {!! Form::hidden("block_id") !!}
					            {!! Form::hidden("block_action") !!}
					          </div>
					          <div class="form-group">
					            <label for="block_type" class="control-label">Block type:</label>
					            {!! Form::select('block_type', $block_types_list, null, ['class' => 'form-control block-type-picker']) !!}
					          </div>
					          @foreach($block_types as $block_type)
					          <?php
					          $block_type_data = $block_type["type"];
					          ?>
					          <div class="block-fields" rel="{{ $block_type_data["id"] }}">
		                		<fieldset>
		                			<legend class="scheduler-border">{{ $block_type_data["name"] }} Block</legend>
						          	@foreach($block_type_data["fields"] as $field)
						          	<?php
						          	$field_id = $block_type_data["id"] . "-" . $field["id"];
						          	?>						          	
									<div class="form-group">
										{!! Form::label($field_id, $field["name"]) !!}
						          	<?php
						          	switch($field["type"]){
						          		case("select"):
									?>
										{!! Form::select($field_id, $field["options"], null, ['class' => 'form-control']) !!}
									<?php
						          		break;
						          		case("page"):
									?>
										{!! Form::select($field_id, create_nested_selectbox_data($pages, 1, ["" => "-"]), null, ['class' => 'form-control']) !!}
									<?php
						          		break;
						          		case("elements_category"):
									?>
										{!! Form::select($field_id, $categories_list, null, ['class' => 'form-control']) !!}
									<?php
						          		break;
						          		case("elements_list"):
						          	?>
										@foreach($categories as $category)
										<div class="elements-list list-group" rel="{{ $category->id }}">
											@foreach($category->elements as $element)
											<div class="list-group-item">
											{!! Form::checkbox($field_id.'['.$category->id.']['.$element->id.']', 1, null, ['id' => $field_id.'-'.$category->id.'-'.$element->id, 'data-element_id' => $element->id, 'data-element_title' => htmlspecialchars($element->title)]) !!}
											{!! Form::label($field_id.'-'.$category->id.'-'.$element->id, $element->title) !!}
											</div>
											@endforeach
										</div>
										@endforeach
						          	<?php
						          		break;
						          		case("file"):
									?>
										<button type="button" class="media-library-button" data-target="{{ $field_id }}"><span class="glyphicon glyphicon-folder-open"></span></button>
										{!! Form::text($field_id, null, ['class' => 'form-control']) !!} 
										<?php
										$media_library_popup_fields[] = $field_id;
										?>
									<?php
						          		break;
						          		case("text"):
									?>
										{!! Form::textarea($field_id, null, ['class' => 'form-control textarea-editor']) !!}
										<?php
										$media_library_popup_fields[] = $field_id;
										?>
									<?php
						          		break;
						          		case("text-sm"):
									?>
										{!! Form::textarea($field_id, null, ['class' => 'form-control textarea-sm']) !!}
									<?php
						          		break;
						          		case("string"):
						          		default:
									?>
										{!! Form::text($field_id, null, ['class' => 'form-control']) !!}
									<?php
						          		break;
						          	}
						          	?>
									</div>
						          	@endforeach
		                		</fieldset>
					          </div>
					          @endforeach
					        </form>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-primary button-add-block-content">Insert</button>
					      </div>
					    </div>
					  </div>
					</div>
@foreach($media_library_popup_fields as $media_library_popup_field)
	@include("Admin::partials.media-library-popup", ["field_id" => $media_library_popup_field])
@endforeach
