@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Page's Details: {{ $page->name }}</div>

                <div class="panel-body">
                	<p>Name: {{ $page->name }}</p>
                	<p>Folder: {{ $page->folder }}</p>
                	<p>Data source: {{ $page->datasource }}</p>
                	<p>Templates: <pre class="small">{{ $page->templates }}</pre></p>
                </div>

                <div class="panel-footer">
        			{!! action_buttons('edit', $page, 'Page') !!}
        			{!! action_buttons('delete', $page, 'Page') !!}
            		<div class="pull-right">
                		<a href="{{ action('Admin\WebsitePageController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>  Back to Pages List</a>
                	</div>
	                {!! Form::open(['action' => ['Admin\WebsitePageController@destroy', $page->id], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!}{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
