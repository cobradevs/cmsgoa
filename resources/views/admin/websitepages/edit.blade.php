@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{ studly_case($save_type) }} a Page: {{ $page->title }}</div>

                <div class="panel-body">

                	@include("Admin::errors.form-errors")

					@if($save_type=="duplicate")
                		{!! Form::model($page, ['action' => ['Admin\WebsitePageController@store', $website->id, $language->id], 'method' => 'POST', 'files' => true]) !!}
                	@else
						{!! Form::model($page, ['action' => ['Admin\WebsitePageController@update', $website->id, $language->id, $page->id], 'method' => 'PUT', 'files' => true]) !!}
                	@endif

                		<!-- Template Field -->
                		<div class="form-group">
                		    {!! Form::label('template', 'Template:') !!}
                		    <div>
                		    @foreach($templates as $template_id => $template_name)
							{!! Form::radio('template', $template_id, null, ['id' => 'template_'.$template_id, 'class' => 'template-picker']) !!}
							<label for="template_{!! $template_id !!}"><img src="/img/assets/{!! $template_id !!}.png" class="template-thumbnail text-center"><br> &nbsp; &nbsp; {!! $template_name !!}</label>
                		    @endforeach
                		    </div>
                		</div>
                		<!-- Parent Page Field -->
                		<div class="form-group">
                		    {!! Form::label('parent_id', 'Parent Page:') !!}
                		    {!! Form::select('parent_id', create_nested_selectbox_data($pages, 1, ["" => "-"]), null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- Title Field -->
                		<div class="form-group">
                		    {!! Form::label('title', 'Title:') !!}
                		    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                		</div>
                		<fieldset>
                			<legend class="scheduler-border">SEO</legend>
	                		<!-- Meta Title Field -->
	                		<div class="form-group">
	                		    {!! Form::label('meta_title', 'Meta Title:') !!}
	                		    {!! Form::text('meta_title', null, ['class' => 'form-control', 'onkeyup' => 'countChar(this, 60, "titleChars")']) !!}
	                		    <small class="form-text text-muted titleChars" style="opacity:0;"><span id="titleChars"></span> characters remaining.</small>
	                		</div>
	                		<!-- Meta Description Field -->
	                		<div class="form-group">
	                		    {!! Form::label('meta_description', 'Meta Description:') !!}
	                		    {!! Form::textarea('meta_description', null, ['class' => 'form-control',  'onkeyup' => 'countChar(this, 120, "DescChars")', 'size' => '30x3']) !!}
	                		    <small class="form-text text-muted DescChars" style="opacity:0;"><span id="DescChars"></span> characters remaining.</small>	                
	                		</div>
                		</fieldset>
                		<!-- Status Field -->
                		<div class="form-group">
                		    {!! Form::label('status', 'Status:') !!}
                		    {!! Form::select('status', $statuses, null, ['class' => 'form-control']) !!}
                		</div>
                		<fieldset>
                			<legend class="scheduler-border">Page Content</legend>
	                		<?php
	                		$templates = json_decode($page->website->theme->templates);
	                		?>
	                		@foreach($templates as $template_data)
	                		<?php
	                		$template = $template_data->template;
	                		?>
	                		<div class="page-regions hidden" rel="{{ $template->id }}">
	                			@foreach($template->regions as $region_id => $region_title)
	                	    	<div class="region" rel="{{ $template->id.'-'.$region_id }}" data-website_id="{{ $website->id }}" data-template="{{ $template->id }}" data-region_id="{{ $region_id }}" data-region_title="{{ $region_title }}">
									<div class="panel panel-default">
										<div class="panel-heading">
											<p class="pull-right"><button type="button" class="btn btn-default btn-xs" data-toggle="modal" data-target="#addBlockModal" data-website_id="{{ $website->id }}" data-template="{{ $template->id }}" data-region_id="{{ $region_id }}" data-region_title="{{ $region_title }}">+ Add Block</button></p>
											{{ $region_title }}
										</div>
										<div class="panel-body block-list">
											
										</div>
									</div>
	                	    	</div>
	                			@endforeach
	                		</div>
	                		@endforeach
                		</fieldset>
	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\WebsitePageController@index', [$website->id, $language->id]) }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to Pages List</a>
	                	</div>
                	{!! Form::close() !!}

                	@include("Admin::websitepages.add-edit-block-form")

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push("scripts")
<script>
	var block_type_fields = {!! json_encode(array_pluck(block_types(), 'type.fields', 'type.id')) !!};
	var block_type_template = {!! json_encode(array_pluck(block_types(), 'type.template', 'type.id')) !!};
	@foreach($page->blocks as $block)
	var block_meta = {};
	block_meta.block_type = '{{ $block->block_type }}';
	block_meta.website_id = '{{ $website->id }}';
	block_meta.template = '{{ $page->template }}';
	block_meta.region_id = '{{ $block->region }}';
	block_meta.region_title = $("div[data-template='{{ $page->template }}'][data-region_id='{{ $block->region }}']").data("region_title");
	var block_data = {!! $block->data !!};
	block_data.block_id = '{{ $block->id }}';
	fillBlocksData(block_meta, block_data);
	@endforeach
</script>
@endpush
