@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add a New Theme</div>

                <div class="panel-body">
                	
                	@include("Admin::errors.form-errors")

                	{!! Form::open(['action' => 'Admin\ThemeController@store', 'files' => true]) !!}
                		<!-- Name Field -->
                		<div class="form-group">
                		    {!! Form::label('name', 'Name:') !!}
                		    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Description Field -->
                		<div class="form-group">
                		    {!! Form::label('description', 'Description:') !!}
                		    {!! Form::text('description', old('description'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Folder Field -->
                		<div class="form-group">
                		    {!! Form::label('folder', 'Folder:') !!}
                		    {!! Form::text('folder', old('folder'), ['class' => 'form-control']) !!}
                		</div>
	                	<!-- Screenshot -->
	                	<div class="form-group">
	                	    {!! Form::label('screenshot', 'Screenshot:') !!}
	                	    {!! Form::file('screenshot') !!}
	                	</div>
	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\ThemeController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to Themes List</a>
	                	</div>
                	{!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
