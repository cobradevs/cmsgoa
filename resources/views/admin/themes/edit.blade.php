@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit a Theme: {{ $theme->name }}</div>

                <div class="panel-body">

                	@include("Admin::errors.form-errors")

                	{!! Form::model($theme, ['action' => ['Admin\ThemeController@update', $theme->id], 'method' => 'PUT', 'files' => true]) !!}
                		<!-- Name Field -->
                		<div class="form-group">
                		    {!! Form::label('name', 'Name:') !!}
                		    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- Description Field -->
                		<div class="form-group">
                		    {!! Form::label('description', 'Description:') !!}
                		    {!! Form::text('description', null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- Folder Field -->
                		<div class="form-group">
                		    {!! Form::label('folder', 'Folder:') !!}
                		    {!! Form::text('folder', null, ['class' => 'form-control']) !!}
                		</div>
	                	<!-- Screenshot -->
	                	<div class="form-group">
	                	    {!! Form::label('screenshot', 'Screenshot:') !!}
	                	    {!! Form::file('screenshot') !!}
	                	    @if(File::exists($theme->screenshot))
	                	    <span class="small"><a href="{{ asset($theme->screenshot) }}" data-toggle="lightbox" data-title="Theme Screenshot: {{ $theme->name }}">View current screenshot</a></span>
	                	    @endif
	                	</div>
                		<fieldset>
                			<legend class="scheduler-border">Templates</legend>
                			<div class="alert alert-warning" role="alert">
                				<strong>Warning!</strong> Any change here will highly effect the distribution of the contents. Do not mess with this section if you don't know what you're doing!
                			</div>
                			<div class="alert alert-info" role="alert">
                				Every template in a theme needs to have a <strong>unique</strong> name and every region in each template needs to have a <strong>unique</strong> name inside that template.
                			</div>
		                	<!-- Templates Field -->
		                	<div class="template-list">
		                	    <div class="row">
		                	    <?php
		                	    if($theme->templates)
		                	    	$templates = json_decode($theme->templates);
		                	   	else
		                	   		$templates = default_templates();
		                	    ?>
	                	    	<?php $template_count = 0; ?>
	                	    	@foreach ($templates as $template_data)
	                	    	<?php 
	                	    	$template = $template_data->template;
	                	    	$template_count++;
	                	    	?>
	                	    	<div class="col-sm-6 template">
									<div class="panel panel-default">
										<div class="panel-heading">
											{!! Form::text('templates['.$template_count.'][name]', $template->name) !!}
											<span class="glyphicon glyphicon-remove text-muted small btn-delete-template pull-right" aria-hidden="true" title="Delete Template"></span>
										</div>
										<div class="panel-body">
											<ul class="region-list list-unstyled" id="region-list-{{ $template_count }}" data-template_key="{{ $template_count }}">
											@foreach ($template->regions as $region_name)
												<li>
													<span class="glyphicon glyphicon-move text-muted btn-move-region" aria-hidden="true"></span>
													{!! Form::text('templates['.$template_count.'][regions][]', $region_name) !!}
													<span class="glyphicon glyphicon-remove text-muted small btn-delete-region pull-right" aria-hidden="true" title="Delete Region"></span>
												</li>
											@endforeach
											</ul>
											<p class="text-center"><button type="button" class="btn btn-default btn-xs btn-add-region" data-template_key="{{ $template_count }}">+ Add Region</button></p>
										</div>
									</div>
	                	    	</div>
	                	    	@endforeach
	                	    	<script>
	                	    		var template_count = {{ $template_count }};
	                	    	</script>
		                	    </div>
		                	    <button type="button" class="btn btn-default btn-add-template">+ Add Template</button>
		                	</div>
		                </fieldset>
                		<fieldset>
                			<legend class="scheduler-border">Theme Options</legend>
                			<p>Here you can define theme-specific options. Editors can set these options for each website.</p>
                			<div class="theme-option-list">
                				<p><button type="button" class="btn btn-default btn-xs btn-add-theme-option">+ Add Theme Option</button></p>
		                	    <?php
		                	    if($theme->options && count(json_decode($theme->options)))
		                	    	$options = json_decode($theme->options);
		                	   	else
		                	   		$options = default_theme_options();
		                	    ?>
	                	    	<?php $option_count = 0; ?>
	                	    	@foreach ($options as $option_data)
	                	    	<?php 
	                	    	$option = $option_data->option;
	                	    	$option_count++;
	                	    	?>
	                			<div class="row theme-option">
	                				<div class="col-sm-6">
			                		    <div class="form-group">{!! Form::text('options['.$option_count.'][name]', $option->name, ['class' => 'form-control', 'placeholder' => 'Option Title']) !!}</div>
	                				</div>
	                				<div class="col-sm-5">
			                		    <div class="form-group">{!! Form::select('options['.$option_count.'][type]', theme_option_types(), $option->type, ['class' => 'form-control']) !!}</div>
	                				</div>
	                				<div class="col-sm-1">
	                					<div class="form-group"><span class="glyphicon glyphicon-remove text-muted small btn-delete-theme-option" aria-hidden="true" title="Delete Theme Option"></span></div>
	                				</div>
	                			</div>
	                			@endforeach
	                			<script>
	                	    		var option_count = {{ $option_count }};
	                	    	</script>
	                		</div>
                		</fieldset>
	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\ThemeController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to Themes List</a>
	                	</div>
                	{!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
