@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Theme's Details: {{ $theme->name }}</div>

                <div class="panel-body">
                	<p>Name: {{ $theme->name }}</p>
                	<p>Folder: {{ $theme->folder }}</p>
                	<p>Data source: {{ $theme->datasource }}</p>
                	<p>Templates: <pre class="small">{{ $theme->templates }}</pre></p>
                </div>

                <div class="panel-footer">
        			{!! action_buttons('edit', $theme, 'Theme') !!}
        			{!! action_buttons('delete', $theme, 'Theme') !!}
            		<div class="pull-right">
                		<a href="{{ action('Admin\ThemeController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>  Back to Themes List</a>
                	</div>
	                {!! Form::open(['action' => ['Admin\ThemeController@destroy', $theme->id], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!}{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
