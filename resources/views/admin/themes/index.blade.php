@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	
        	@include("Admin::errors.form-errors")

            <div class="panel panel-default">
                <div class="panel-heading">
                	<div class="pull-right">{!! action_buttons('create', null, 'Theme') !!}</div>
                	<h2 class="panel-title">Themes</h2>
                </div>
            </div>

			<table class="table table-striped table-hover" id="themes-table">
			<thead>
				<tr>
					<th></th>
					<th>Name</th>
					<th>Folder</th>
					<th class="text-right"></th>
				</tr>
			</thead>
			</table>
			{!! Form::open(['action' => ['Admin\ThemeController@destroy', 0], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!} {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
    $('#themes-table').DataTable({
    	dom: 't',
        ajax: '{!! action('Admin\ThemeController@data') !!}',
        order: [[1, 'asc']],
        @include("Admin::partials.datatables-options")
        columns: [
        	
            { data: 'screenshot_thumbnail', name: 'screenshot_thumbnail', 'className': 'min-tablet', 'orderable': false, 'searchable': false },
            { data: 'name', name: 'name', 'className': 'all' },
            { data: 'folder', name: 'folder', 'className': 'min-tablet' },
            { data: 'action', name: 'action', 'className': 'text-right text-nowrap all', 'orderable': false, 'searchable': false }
        ]
    });
});
</script>
@endpush
