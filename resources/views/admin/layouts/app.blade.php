<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CMS GOA</title>

    <link rel="stylesheet" href="{{ asset('css/admin/all.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex, nofollow">
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/admin') }}">
                    CMS GOA
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                @if (Auth::check())
                    @if (isset($website) && isset($language))
	                    <ul class="nav navbar-nav">
	                    	<li class="website-title"><a href="{{ action('Admin\WebsitePageController@index', [$website->id, $language->id]) }}">{{ $website->name }} @if(count($website->languages) > 1) <small>{{ $language->name }}</small> @endif </a></li>
							@include("Admin::navigation.editor")
						</ul>
					@elseif(Auth::user()->isAdmin())
						<ul class="nav navbar-nav">
                    		<li><a href="{{ url('/admin') }}">Dashboard</a></li>
						@include("Admin::navigation.admin")
            			</ul>
                    @endif
                @endif

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                            	@if (Auth::user()->isEditor() && isset($websites))
									@foreach ($websites as $each_website)
											@if(count($each_website->languages) == 1)
												<li>
													<a href="{{ action('Admin\WebsitePageController@index', [$each_website->id, $each_website->languages[0]->id]) }}">{{ $each_website->name }}</a>
												</li>
											@else
												<li class="dropdown dropdown-submenu">
													<a tabindex="-1" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
						                                {{ $each_website->name }}
						                            </a>
						                            <ul class="dropdown-menu" role="menu">
													@foreach($each_website->languages as $each_language)
														<li><a href="{{ action('Admin\WebsitePageController@index', [$each_website->id, $each_language->id]) }}">{{ $each_language->name }}</a></li>
													@endforeach
													</ul>
												</li>
											@endif
									@endforeach
                            	@endif
                                
                                <li>
                                    <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Logout  
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>

                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    @yield("content")

    <script src="{{ asset("js/admin/all.js") }}"></script>
    @stack("scripts")
    @include("Admin::partials.feedback")

</body>
</html>
