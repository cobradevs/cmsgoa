@extends('Admin::layouts.app')


@section('content')
<div class="jumbotron">
	<h2>User activity Log</h2>
</div>
<p>&nbsp;</p>
	<ul class="list-group">
			@include('Admin::activities.partials.list')
	</ul>

@stop