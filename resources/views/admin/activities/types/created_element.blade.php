
{{ $event->user->name }} has published an element, <a href="/admin/elements/{{ $event->subject_id}}"> {{ $event->subject->title}} </a>, on {{ $event->created_at->diffForHumans() }}
