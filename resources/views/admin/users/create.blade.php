@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add a New User</div>

                <div class="panel-body">
                	
                	@include("Admin::errors.form-errors")

                	{!! Form::open(['action' => 'Admin\UserController@store', 'class' => '']) !!}
                		<!-- Role Field -->
                		<div class="form-group">
                		    {!! Form::label('role_id', 'Role:') !!}
                		    {!! Form::select('role_id', $roles, old('role_id'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Name Field -->
                		<div class="form-group">
                		    {!! Form::label('name', 'Name:') !!}
                		    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
                		</div>
	                	<!-- E-mail Field -->
	                	<div class="form-group">
	                	    {!! Form::label('email', 'E-mail:') !!}
	                	    {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
	                	</div>
	                	<!-- Password Field -->
	                	<div class="form-group">
	                	    {!! Form::label('password', 'Password:') !!}
	                	    {!! Form::password('password', ['class' => 'form-control']) !!}
	                	</div>
                		<!-- Authorized Websites Field -->
                		<div class="form-group">
                		    {!! Form::label('websites', 'Authorized Website(s):') !!}
                		    {!! Form::select('websites[]', $websites, old('websites'), ['class' => 'form-control', 'multiple' => 'multiple', 'data-include-select-all-option' => 'true']) !!}
                		</div>
	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\UserController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to Users List</a>
	                	</div>
                	{!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
