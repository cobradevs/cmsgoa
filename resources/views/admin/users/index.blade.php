@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	
        	@include("Admin::errors.form-errors")

            <div class="panel panel-default">
                <div class="panel-heading">
                	<div class="pull-right">{!! action_buttons('create', null, 'User') !!}</div>
                	<h2 class="panel-title">Users</h2>
                </div>
            </div>

			<table class="table table-striped table-hover" id="users-table">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>E-mail</th>
					<th>Role</th>
					<th class="text-right"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</tfoot>
			</table>
			{!! Form::open(['action' => ['Admin\UserController@destroy', 0], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!} {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        ajax: '{!! action('Admin\UserController@data') !!}',
        order: [[1, 'asc']],
        @include("Admin::partials.datatables-options")
        @include("Admin::partials.datatables-filters", ['columns' => "[3]"])
        columns: [
            { data: 'id', name: 'users.id', 'className': 'min-tablet' },
            { data: 'name', name: 'users.name', 'className': 'all' },
            { data: 'email', name: 'email', 'className': 'min-tablet' },
            { data: 'role.name', name: 'role.name', 'className': 'min-desktop' },
            { data: 'action', name: 'action', 'className': 'text-right all', 'orderable': false, 'searchable': false }
        ]
    });
});
</script>
@endpush
