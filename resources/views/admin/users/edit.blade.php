@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit a User: {{ $user->name }} ({{ $user->email }})</div>

                <div class="panel-body">

                	@include("Admin::errors.form-errors")

                	{!! Form::model($user, ['action' => ['Admin\UserController@update', $user->id], 'method' => 'PUT']) !!}

                		<!-- Role Field -->
                		<div class="form-group">
                		    {!! Form::label('role_id', 'Role:') !!}
                		    {!! Form::select('role_id', $roles, null, ['class' => 'form-control']) !!}
                		</div>

                		<!-- Name Field -->
                		<div class="form-group">
                		    {!! Form::label('name', 'Name:') !!}
                		    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                		</div>

	                	<!-- E-mail Field -->
	                	<div class="form-group">
	                	    {!! Form::label('email', 'E-mail:') !!}
	                	    {!! Form::email('email', null, ['class' => 'form-control']) !!}
	                	</div>

                		<!-- Authorized Websites Field -->
                		<div class="form-group">
                		    {!! Form::label('websites', 'Authorized Website(s):') !!}
                		    {!! Form::select('websites[]', $websites, $user_websites, ['class' => 'form-control', 'multiple' => 'multiple', 'data-include-select-all-option' => 'true']) !!}
                		</div>

	                	<!-- Password Field -->
						<p class="small"><a role="button" data-toggle="collapse" href="#passwordField" aria-expanded="false" aria-controls="passwordField">Change password</a></p>
						<div class="collapse" id="passwordField">
		                	<div class="form-group">
		                	    {!! Form::checkbox('change_password', 'checked', false, ['id'=>'change_password']) !!} {!! Form::label('change_password', 'Check here to confirm password change', ['class' => 'text-danger']) !!}
		                	</div>
							<div class="form-group">
							    {!! Form::label('password', 'New Password:') !!}
							    {!! Form::password('password', ['class' => 'form-control']) !!}
							</div>
						</div>
						@if(old('change_password') == "checked")
							@push('scripts')
								<script type="text/javascript">
							    $(document).ready(function(){
							    	$('#passwordField').collapse('show');
							    });
								</script>
							@endpush
						@endif

	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\UserController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to Users List</a>
	                	</div>

                	{!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
