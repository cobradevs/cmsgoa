@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">User's Details: {{ $user->name }} ({{ $user->email }})</div>

                <div class="panel-body">
                	<p>Role: {{ $roles[$user->role_id] }}</p>
                	<p>Name: {{ $user->name }}</p>
                	<p>Email: {{ $user->email }}</p>
                </div>

                <div class="panel-footer">
        			{!! action_buttons('edit', $user, 'User') !!}
        			{!! action_buttons('delete', $user, 'User') !!}
            		<div class="pull-right">
                		<a href="{{ action('Admin\UserController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>  Back to Users List</a>
                	</div>
	                {!! Form::open(['action' => ['Admin\UserController@destroy', $user->id], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!}{!! Form::close() !!}
                </div>

            </div>
            <div class="panel panel-default">
                <div class="panel-heading">User activities</div>
                <div class="panel-body">
                   @foreach ($activities as $activity) 
                      <li class="List">{!! $activity['user']['name']  !!} has {!! $activity['name']  !!} at {!! $activity['created_at']  !!}</li>
                   @endforeach
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
