@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	
        	@include("Admin::errors.form-errors")

            <div class="panel panel-default">
                <div class="panel-heading">
                	<h2 class="panel-title">Media Library</h2>
                </div>
            </div>
			
			<iframe src="/admin/website/{{ $website->id or 0 }}/{{ $language->id or 0 }}/elfinder/popup/0" width="99.6%" height="640" frameborder="0"></iframe>

        </div>
    </div>
</div>
@endsection

