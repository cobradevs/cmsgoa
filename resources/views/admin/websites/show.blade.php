@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Website's Details: {{ $website->name }}</div>

                <div class="panel-body">


                <div class="widget-box">
                  <div class="widget-title">
                    <div class="pull-right">
                      <img src="{{ asset($website->logo) }}" class="img-responsive img-thumbnail img-negative">
                    </div>
                  </div>
                  <div class="widget-content nopadding">
                    <ul class="recent-posts">
    
                    <li>
                        Name: {{ $website->name }}
                    </li>
                    <li>Theme: {{ $website->theme->name }}</li>
                    <li>

                                @foreach($domains as $name => $id)
                                    <div class="article-post">Domain name: <a href="{!! $name !!}" target="_blank">{!! $name !!}</a></div> 
                                @endforeach
                     
                    </li>
                      <li>
                            <div class="article-post">Default Language: {{ $website->default_language->name }}</div>
                      </li>
                      <li>
                            <div class="article-post">News Per Page: {{ $website->news_per_page }}</div>
                      </li>
                      <li>
                        <div class="article-post">Latest News Limit: {{ $website->news_latest_limit }}</div>
                      </li>
                      <li>
                          <div class="article-post">Most Read News Limit: {{ $website->news_mostread_limit }}</div>
                      </li>
                     <li>
                          <div class="article-post">News Ordering: {{ $website->news_order }}</div>
                      </li>
                     <li>
                          <div class="article-post">Available Language(s): 
                                @foreach($website->languages as $language)
                                    {{ $language->name }} 
                                @endforeach
                            </div>
                      </li>
                      <li>
                            <div class="article-post">Additional Headers: <pre>{{ $website->additional_header }}<p></p><p></p></pre></div>   
                      </li>   
                      <li>
                            <div class="article-post"><pre><a href="https://analytics.google.com/analytics/web/">Visit Google Analytics</a></pre></div>               
                      </li>               
                    </ul>
                  </div>
                </div>

                </div>

                <div class="panel-footer">
        			{!! action_buttons('edit', $website, 'Website') !!}
        			{!! action_buttons('delete', $website, 'Website') !!}
            		<div class="pull-right">
                		<a href="{{ action('Admin\WebsiteController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>  Back to Websites List</a>
                	</div>
	                {!! Form::open(['action' => ['Admin\WebsiteController@destroy', $website->id], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!}{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
