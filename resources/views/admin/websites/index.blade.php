@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	
        	@include("Admin::errors.form-errors")

            <div class="panel panel-default">
                <div class="panel-heading">
                	<div class="pull-right">{!! action_buttons('create', null, 'Website') !!}</div>
                	<h2 class="panel-title">Websites</h2>
                </div>
            </div>

			<table class="table table-striped table-hover" id="websites-table">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Theme</th>
					<th>Logo</th>
					<th class="text-right"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</tfoot>
			</table>
			{!! Form::open(['action' => ['Admin\WebsiteController@destroy', 0], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!} {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
    $('#websites-table').DataTable({
        ajax: '{!! action('Admin\WebsiteController@data') !!}',
        order: [[1, 'asc']],
        @include("Admin::partials.datatables-options")
        @include("Admin::partials.datatables-filters", ['columns' => "[2]"])
        columns: [
        	{ data: 'id', name: 'id', 'className': 'all' },
            { data: 'name', name: 'websites.name', 'className': 'all' },
            { data: 'theme.name', name: 'theme.name', 'className': 'min-tablet' },
            { data: 'logo', name: 'logo', 'className': 'min-tablet', 'orderable': false, 'searchable': false },
            { data: 'action', name: 'action', 'className': 'text-right text-nowrap all', 'orderable': false, 'searchable': false }
        ]
    });
});
</script>
@endpush
