@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit a Website: {{ $website->name }}</div>

                <div class="panel-body">

                	@include("Admin::errors.form-errors")

					@if($save_type=="duplicate")
                		{!! Form::model($website, ['action' => ['Admin\WebsiteController@store'], 'method' => 'POST', 'files' => true]) !!}
                	@else
						{!! Form::model($website, ['action' => ['Admin\WebsiteController@update', $website->id], 'method' => 'PUT', 'files' => true]) !!}
                	@endif

                		<!-- Name Field -->
                		<div class="form-group">
                		    {!! Form::label('name', 'Name:') !!}
                		    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                		</div>

                		@if($save_type!="duplicate")
                            <!-- domain name theme -->
                        <div class="form-group">
                           {!! link_to_action('Admin\DomainController@create', $title = 'Add a domain', $parameters = array('website_id' =>$website->id)) !!}
                            <table class="table table-striped table-hover" id="domains-table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th class="text-right"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($domains as $name => $id)
                                <tr>
                                   <td>{!! $id !!}</td>
                                    <td>{!! $name !!}</td>
                                    <td class=" text-right all">
                                    {!! link_to_action('Admin\DomainController@edit', $title = 'Edit', $parameters = array('domain_id' =>$id), ['class' => 'btn btn-sm btn-warning']) !!}
                                    <button type="button" data-action="{{ action('Admin\DomainController@destroy',  $id) }}" class="btn btn-sm btn-danger btn-delete-warning"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span><span class="hidden-xs hidden-sm"> Delete</span></button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                        @endif
                		<!-- Theme Field -->
                		<div class="form-group">
                		    {!! Form::label('theme_id', 'Theme:') !!}
                		    {!! Form::select('theme_id', $themes, null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- Default Language Field -->
                		<div class="form-group">
                		    {!! Form::label('default_language_id', 'Default Language:') !!}
                		    {!! Form::select('default_language_id', $languages_with_selector, null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- Available Languages Field -->
                		<div class="form-group">
                		    {!! Form::label('languages', 'Available Language(s):') !!}
                		    {!! Form::select('languages[]', $languages, $website_languages, ['class' => 'form-control', 'multiple' => 'multiple']) !!}
                		</div>
                		@if($save_type!="duplicate")
                		@foreach($website->languages as $website_language)
                		<!-- Home Page for {{ $languages[$website_language->id] }} Field -->
                		<div class="form-group">
                		    {!! Form::label('homepage_id['.$website_language->id.']', 'Home Page for '.$languages[$website_language->id].':') !!}
                			@if($website_pages[$website_language->id] and count($website_pages[$website_language->id]))
                		    	{!! Form::select('homepage_id['.$website_language->id.']', create_nested_selectbox_data($website_pages[$website_language->id], 1), $website_language->pivot->homepage_id, ['class' => 'form-control']) !!}
                		    @else
                		    	<p>No pages added to {{ $languages[$website_language->id] }} yet.</p>
                			@endif
                		</div>
                		@endforeach
                		@endif
	                	<!-- Logo -->
	                	<div class="form-group">
	                	    {!! Form::label('logo', 'Logo:') !!}
	                	    {!! Form::file('logo') !!}
	                	    @if(File::exists($website->logo))
	                	    <span class="small"><a href="{{ asset($website->logo) }}" data-toggle="lightbox" data-title="Website Logo: {{ $website->name }}">View current logo</a></span>
	                	    @endif
	                	</div>
	                	<!-- Image -->
	                	<div class="form-group">
	                	    {!! Form::label('image', 'Image:') !!}
	                	    {!! Form::file('image') !!}
	                	    @if(File::exists($website->image))
	                	    <span class="small"><a href="{{ asset($website->image) }}" data-toggle="lightbox" data-title="Website Image: {{ $website->name }}">View current image</a></span>
	                	    @endif
	                	</div>
	                	<!-- Favicon -->
	                	<div class="form-group">
	                	    {!! Form::label('favicon', 'Favicon:') !!}
	                	    {!! Form::file('favicon') !!}
	                	    @if(File::exists($website->favicon))
	                	    <span class="small"><a href="{{ asset($website->favicon) }}" data-toggle="lightbox" data-title="Website Favicon: {{ $website->name }}" data-type="image">View current favicon</a></span>
	                	    @endif
	                	</div>
                		<!-- Additional Header Text Field -->
                		<div class="form-group">
                		    {!! Form::label('additional_header', 'Additional Header Text:') !!}
                		    {!! Form::textarea('additional_header', null, ['class' => 'form-control textarea-sm']) !!}
                		</div>
                		<!-- News Per Page Field -->
                		<div class="form-group">
                		    {!! Form::label('news_per_page', 'News Per Page:') !!}
                		    {!! Form::number('news_per_page', null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- Latest News Limit Field -->
                		<div class="form-group">
                		    {!! Form::label('news_latest_limit', 'Latest News Limit:') !!}
                		    {!! Form::number('news_latest_limit', null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- News Most Read Limit Field -->
                		<div class="form-group">
                		    {!! Form::label('news_mostread_limit', 'Most Read News Limit:') !!}
                		    {!! Form::number('news_mostread_limit', null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- News Ordering Field -->
                		<div class="form-group">
                		    <strong>News Ordering:</strong> &nbsp; 
                		    {!! Form::radio('news_order', 'ASC', null, ['id' => 'news_order_asc']) !!} {!! Form::label('news_order_asc', 'Ascending', ['class' => 'text-light']) !!} &nbsp; 
                		    {!! Form::radio('news_order', 'DESC', null, ['id' => 'news_order_desc']) !!}  {!! Form::label('news_order_desc', 'Descending', ['class' => 'text-light']) !!} 
                		</div>
                		<!-- Is live Field -->
                		<div class="form-group">
                		    {!! Form::checkbox('is_live', 1, null, ['data-toggle' => "no-toggle", 'id' => "is_live"]) !!}
                		    {!! Form::label('is_live', 'Is live') !!}
                		</div>
                		@if($save_type=="duplicate")
                		<!-- Clone pages Field -->
                		<div class="form-group">
                		    {!! Form::checkbox('clone_pages', $website->id, true, ['data-toggle' => "no-toggle", 'id' => "clone_pages"]) !!}
                		    {!! Form::label('clone_pages', 'Clone pages') !!}
                		</div>
                		<!-- Clone elements Field -->
                		<div class="form-group">
                		    {!! Form::checkbox('clone_elements', $website->id, true, ['data-toggle' => "no-toggle", 'id' => "clone_elements"]) !!}
                		    {!! Form::label('clone_elements', 'Clone elements') !!}
                		</div>
                		@endif
	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\WebsiteController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to Websites List</a>
	                	</div>

                	{!! Form::close() !!}
                    {!! Form::open(['action' => ['Admin\DomainController@destroy', 0], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!} {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
