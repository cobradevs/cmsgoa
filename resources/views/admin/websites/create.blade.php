@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add a New Website</div>

                <div class="panel-body">
                	
                	@include("Admin::errors.form-errors")

                	{!! Form::open(['action' => 'Admin\WebsiteController@store', 'files' => true]) !!}
                		<!-- Name Field -->
                		<div class="form-group">
                		    {!! Form::label('name', 'Name:') !!}
                		    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Theme Field -->
                		<div class="form-group">
                		    {!! Form::label('theme_id', 'Theme:') !!}
                		    {!! Form::select('theme_id', $themes, old('theme_id'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Default Language Field -->
                		<div class="form-group">
                		    {!! Form::label('default_language_id', 'Default Language:') !!}
                		    {!! Form::select('default_language_id', $languages_with_selector, old('default_language_id'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Avilable Languages Field -->
                		<div class="form-group">
                		    {!! Form::label('languages', 'Available Language(s):') !!}
                		    {!! Form::select('languages[]', $languages, old('languages[]'), ['class' => 'form-control', 'multiple' => 'multiple']) !!}
                		</div>
	                	<!-- Logo -->
	                	<div class="form-group">
	                	    {!! Form::label('logo', 'Logo:') !!}
	                	    {!! Form::file('logo') !!}
	                	</div>
	                	<!-- Image -->
	                	<div class="form-group">
	                	    {!! Form::label('image', 'Image:') !!}
	                	    {!! Form::file('image') !!}
	                	</div>
	                	<!-- Favicon -->
	                	<div class="form-group">
	                	    {!! Form::label('favicon', 'Favicon:') !!}
	                	    {!! Form::file('favicon') !!}
	                	</div>
                		<!-- Additional Header Text Field -->
                		<div class="form-group">
                		    {!! Form::label('additional_header', 'Additional Header Text:') !!}
                		    {!! Form::textarea('additional_header', old('additional_header'), ['class' => 'form-control textarea-sm']) !!}
                		</div>
                		<!-- News Per Page Field -->
                		<div class="form-group">
                		    {!! Form::label('news_per_page', 'News Per Page:') !!}
                		    {!! Form::number('news_per_page', old('news_per_page', 10), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Latest News Limit Field -->
                		<div class="form-group">
                		    {!! Form::label('news_latest_limit', 'Latest News Limit:') !!}
                		    {!! Form::number('news_latest_limit', old('news_latest_limit', 5), ['class' => 'form-control']) !!}
                		</div>
                		<!-- News Most Read Limit Field -->
                		<div class="form-group">
                		    {!! Form::label('news_mostread_limit', 'Most Read News Limit:') !!}
                		    {!! Form::number('news_mostread_limit', old('news_mostread_limit', 5), ['class' => 'form-control']) !!}
                		</div>
                		<!-- News Ordering Field -->
                		<div class="form-group">
                		    <strong>News Ordering:</strong> &nbsp; 
                		    {!! Form::radio('news_order', 'ASC', true, ['id' => 'news_order_asc']) !!} {!! Form::label('news_order_asc', 'Ascending', ['class' => 'text-light']) !!} &nbsp; 
                		    {!! Form::radio('news_order', 'DESC', false, ['id' => 'news_order_desc']) !!}  {!! Form::label('news_order_desc', 'Descending', ['class' => 'text-light']) !!} 
                		</div>
                		<!-- Is live Field -->
                		<div class="form-group">
                		    {!! Form::checkbox('is_live', 1, old('is_live'), ['data-toggle' => "no-toggle", 'id' => "is_live"]) !!}
                		    {!! Form::label('is_live', 'Is live') !!}
                		</div>
	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\WebsiteController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to Websites List</a>
	                	</div>
                	{!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
