@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	
        	@include("Admin::errors.form-errors")

            <div class="panel panel-default">
                <div class="panel-heading">
                	<div class="pull-right">{!! action_buttons('create', [$website->id, $language->id, $menu_id], 'WebsiteMenuItem') !!}</div>
                	<h2 class="panel-title">{{ menus($menu_id) }}</h2>
                </div>
            </div>
			
			@if($menu_items and count($menu_items))

				<div class="menu_items-list">
				{!! Form::open(['action' => ['Admin\WebsiteMenuItemController@save_order', $website->id, $language->id, $menu_id]]) !!}
					{!! html_list($menu_items, "menu_item", $menu_id) !!}
					<button type="submit" name="submit" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save Menu Order</button>
				{!! Form::close() !!}
				</div>
				{!! Form::open(['action' => ['Admin\WebsiteMenuItemController@destroy', $website->id, $language->id, $menu_id, 0], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!} {!! Form::close() !!}
			
			@endif

        </div>
    </div>
</div>
@endsection

