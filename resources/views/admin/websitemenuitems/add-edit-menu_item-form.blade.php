					<div class="modal fade" id="addMenuItemModal" tabindex="-1" role="dialog" aria-labelledby="addMenuItemModalLabel">
					  <div class="modal-dialog modal-lg" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title" id="addMenuItemModalLabel">Add Menu Item</h4>
					      </div>
					      <div class="modal-body">
					        <form>
					          <div class="form-group">
					            <label for="region" class="control-label">Region:</label>
					            <span rel="region_title"></span>
					            {!! Form::hidden("template") !!}
					            {!! Form::hidden("region_id") !!}
					            {!! Form::hidden("menu_item_id") !!}
					            {!! Form::hidden("menu_item_action") !!}
					          </div>
					          <div class="form-group">
					            <label for="menu_item_type" class="control-label">Menu Item type:</label>
					            {!! Form::select('menu_item_type', $menu_item_types_list, null, ['class' => 'form-control menu_item-type-picker']) !!}
					          </div>
					          @foreach($menu_item_types as $menu_item_type)
					          <?php
					          $menu_item_type_data = $menu_item_type["type"];
					          ?>
					          <div class="menu_item-fields" rel="{{ $menu_item_type_data["id"] }}">
		                		<fieldset>
		                			<legend class="scheduler-border">{{ $menu_item_type_data["name"] }} Menu Item</legend>
						          	@foreach($menu_item_type_data["fields"] as $field)
						          	<?php
						          	$field_id = $menu_item_type_data["id"] . "-" . $field["id"];
						          	?>						          	
									<div class="form-group">
										{!! Form::label($field_id, $field["name"]) !!}
						          	<?php
						          	switch($field["type"]){
						          		case("select"):
									?>
										{!! Form::select($field_id, $field["options"], null, ['class' => 'form-control']) !!}
									<?php
						          		break;
						          		case("elements_category"):
									?>
										{!! Form::select($field_id, $categories_list, null, ['class' => 'form-control']) !!}
									<?php
						          		break;
						          		case("elements_list"):
						          	?>
										@foreach($categories as $category)
										<div class="elements-list list-group" rel="{{ $category->id }}">
											@foreach($category->elements as $element)
											<div class="list-group-item">
											{!! Form::checkbox($field_id.'['.$category->id.']['.$element->id.']', 1, null, ['id' => $field_id.'-'.$category->id.'-'.$element->id, 'data-element_id' => $element->id, 'data-element_title' => htmlspecialchars($element->title)]) !!}
											{!! Form::label($field_id.'-'.$category->id.'-'.$element->id, $element->title) !!}
											</div>
											@endforeach
										</div>
										@endforeach
						          	<?php
						          		break;
						          		case("text"):
									?>
										{!! Form::textarea($field_id, null, ['class' => 'form-control textarea-editor']) !!}
										<?php
										$media_library_popup_fields[] = $field_id;
										?>
									<?php
						          		break;
						          		case("string"):
						          		default:
									?>
										{!! Form::text($field_id, null, ['class' => 'form-control']) !!}
									<?php
						          		break;
						          	}
						          	?>
									</div>
						          	@endforeach
		                		</fieldset>
					          </div>
					          @endforeach
					        </form>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-primary button-add-menu_item-content">Insert</button>
					      </div>
					    </div>
					  </div>
					</div>
@foreach($media_library_popup_fields as $media_library_popup_field)
	@include("Admin::partials.media-library-popup", ["field_id" => $media_library_popup_field])
@endforeach
