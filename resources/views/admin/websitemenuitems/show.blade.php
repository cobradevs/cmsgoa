@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Menu's Details: {{ $menu->name }}</div>

                <div class="panel-body">
                	<p>Name: {{ $menu->name }}</p>
                	<p>Folder: {{ $menu->folder }}</p>
                	<p>Data source: {{ $menu->datasource }}</p>
                	<p>Templates: <pre class="small">{{ $menu->templates }}</pre></p>
                </div>

                <div class="panel-footer">
        			{!! action_buttons('edit', $menu, 'Menu') !!}
        			{!! action_buttons('delete', $menu, 'Menu') !!}
            		<div class="pull-right">
                		<a href="{{ action('Admin\WebsiteMenuItemController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>  Back to Menus List</a>
                	</div>
	                {!! Form::open(['action' => ['Admin\WebsiteMenuItemController@destroy', $menu->id], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!}{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
