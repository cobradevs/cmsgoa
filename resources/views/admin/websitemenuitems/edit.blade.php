@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit a Menu Item: {{ $menu_item->title }}</div>

                <div class="panel-body">

                	@include("Admin::errors.form-errors")

					@if($save_type=="duplicate")
                		{!! Form::model($menu_item, ['action' => ['Admin\WebsiteMenuItemController@store', $website->id, $language->id, $menu_id], 'method' => 'POST', 'files' => true]) !!}
                	@else
						{!! Form::model($menu_item, ['action' => ['Admin\WebsiteMenuItemController@update', $website->id, $language->id, $menu_id, $menu_item->id], 'method' => 'PUT', 'files' => true]) !!}
                	@endif

                		<!-- Parent Menu Item Field -->
                		<div class="form-group">
                		    {!! Form::label('parent_id', 'Parent Menu Item:') !!}
                		    {!! Form::select('parent_id', create_nested_selectbox_data($menu_items, 1, ["" => "-"], 1), null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- Title Field -->
                		<div class="form-group">
                		    {!! Form::label('title', 'Title:') !!}
                		    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                		</div>
                		<!-- Link Type Field -->
                		<div class="form-group">
                		    <p><em>Link Type:</em></p>
                		    <div>
	                		    {!! Form::radio('type', 1, null, ['id' => 'type_1']) !!} 
	                		    {!! Form::label('type_1', 'Internal Page') !!}
	                		    &nbsp; &nbsp; 
	                		    {!! Form::radio('type', 2, old('type', false), ['id' => 'type_2']) !!} 
	                		    {!! Form::label('type_2', 'Index of Elements/News') !!}
	                		    &nbsp; &nbsp; 
	                		    {!! Form::radio('type', 3, null, ['id' => 'type_3']) !!}
	                		    {!! Form::label('type_3', 'External URL') !!}
                		    </div>
                		</div>
                		<fieldset class="link_data" rel="1">
                			<legend>Internal Page</legend>
	                		<!-- Page Field -->
	                		<div class="form-group">
	                		    {!! Form::label('page_id', 'Select page') !!}
	                		    {!! Form::select('page_id', create_nested_selectbox_data($pages, 1), null, ['class' => 'form-control']) !!}
	                		</div>
                		</fieldset>
                		<fieldset class="link_data" rel="2">
                			<legend>Index of Elements/News</legend>
	                		<!-- Page Field -->
	                		<div class="form-group">
	                		    {!! Form::label('category_id', 'Select category') !!}
	                		    {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
	                		</div>
                		</fieldset>
                		<fieldset class="link_data" rel="3">
                			<legend>External URL</legend>
	                		<!-- Page Field -->
	                		<div class="form-group">
	                		    {!! Form::label('external_url', 'Enter full external URL') !!}
	                		    {!! Form::text('external_url', null, ['class' => 'form-control']) !!}
	                		    <span class="text-muted"><em>Don't forget to include http://</em></span>
	                		</div>
                		</fieldset>
	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\WebsiteMenuItemController@index', [$website->id, $language->id, $menu_id]) }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to Menu Items List</a>
	                	</div>
                	{!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(document).ready(function(){
	$('input:radio[name=type]').change(function() {
		type = $('input:radio[name=type]:checked').val();
		$('.link_data').addClass("hidden").hide();
		$('.link_data[rel='+type+']').removeClass("hidden").slideDown();
	});
	$('input:radio[name=type]:checked').trigger("change");
});
</script>
@endpush
