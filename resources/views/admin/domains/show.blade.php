@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Domain's Details: {{ $domain->name }}</div>

                <div class="panel-body">
                	<p>Name: {{ $domain->name }}</p>
                </div>

                <div class="panel-footer">
        			{!! action_buttons('edit', $domain, 'Domain') !!}
        			{!! action_buttons('delete', $domain, 'Domain') !!}
            		<div class="pull-right">
                		<a href="{{ action('Admin\DomainController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>  Back to Domains List</a>
                	</div>
	                {!! Form::open(['action' => ['Admin\DomainController@destroy', $domain->id], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!}{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
