@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit a Domain: {{ $domain->name }}</div>

                <div class="panel-body">
                	@include("Admin::errors.form-errors")

                	{!! Form::model($domain, ['action' => ['Admin\DomainController@update', $domain->id], 'method' => 'PUT']) !!}
                		<!-- Name Field -->
                		<div class="form-group">
                		    {!! Form::label('name', 'Name:') !!}
                		    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            {!! Form::hidden('website_id', app('request')->input('website_id') )  !!}
                		</div>
	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\DomainController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to Domain List</a>
	                	</div>
                	{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
