@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	
        	@include("Admin::errors.form-errors")

            <div class="panel panel-default">
                <div class="panel-heading">
                	<div class="pull-right">{!! action_buttons('create', null, 'Domain') !!}</div>
                	<h2 class="panel-title">Domains</h2>
                </div>
            </div>

			<table class="table table-striped" id="domains-table">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th class="text-right"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</tfoot>
			</table>
			{!! Form::open(['action' => ['Admin\DomainController@destroy', 0], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!} {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
    $('#domains-table').DataTable({
        ajax: '{!! action('Admin\DomainController@data') !!}',
        order: [[1, 'asc']],
        @include("partials.datatables-options")
        @include("partials.datatables-filters", ['columns' => "[2]"])
        columns: [
        	{ data: 'id', name: 'domains.id', 'className': 'all' },
            { data: 'name', name: 'name', 'className': 'all' },
            { data: 'action', name: 'action', 'className': 'text-right all', 'orderable': false, 'searchable': false }
        ]
    });
});
</script>
@endpush
