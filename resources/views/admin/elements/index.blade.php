@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	
        	@include("Admin::errors.form-errors")

            <div class="panel panel-default">
                <div class="panel-heading">
                	<div class="pull-right">{!! action_buttons('create', null, 'Element') !!}</div>
                	<h2 class="panel-title">Elements</h2>
                </div>
            </div>

			<table class="table table-striped table-hover" id="elements-table">
			<thead>
				<tr>
					<th>#</th>
					<th>Title</th>
					<th>Category</th>
					<th class="text-right"></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
			</tfoot>
			</table>
			{!! Form::open(['action' => ['Admin\ElementController@destroy', 0], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!} {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
    $('#elements-table').DataTable({
        ajax: '{!! action('Admin\ElementController@data') !!}',
        order: [[1, 'asc']],
        @include("Admin::partials.datatables-options")
        @include("Admin::partials.datatables-filters", ['columns' => "[2]"])
        columns: [
        	{ data: 'id', name: 'elements.id', 'className': 'all' },
            { data: 'title', name: 'title', 'className': 'all' },
            { data: 'category.name', name: 'category.name', 'className': 'min-tablet' },
            { data: 'action', name: 'action', 'className': 'text-right text-nowrap all', 'orderable': false, 'searchable': false }
        ]
    });
});
</script>
@endpush
