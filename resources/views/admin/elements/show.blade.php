@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Element's Details: {{ $element->name }}</div>

                <div class="panel-body">
                	<p>Title: {{ $element->title }}</p>
                	<p>Subtitle: {{ $element->subtitle }}</p>
                	<p>Language: {{ $element->language->name }}</p>
                	<p>Category: {{ $element->category->name }}</p>
                	@if($element->website)
                	<p>Website: {{ $element->website->name }}</p>
                	@endif
                	<p>Excerpt: {{ $element->excerpt }}</p>
                	<p>Image: {{ $element->image }}</p>
                	<p>Thumb: {{ $element->thumb }}</p>
                	<p>Body: {{ $element->body }}</p>
                	<p>Meta Title: {{ $element->meta_title }}</p>
                	<p>Meta Description: {{ $element->meta_description }}</p>
                	<p>Other Meta Tags: <pre>{{ $element->other_meta_tags }}</pre></p>
                	<p>Read Count: {{ $element->read_count }}</p>
                	<p>Rank: {{ $element->rank }}</p>
                	<p>Date: {{ $element->published_date }}</p>
                	<p>Is active: {{ $element->status }}</p>
                </div>

                <div class="panel-footer">
        			{!! action_buttons('edit', $element, 'Element') !!}
        			{!! action_buttons('delete', $element, 'Element') !!}
            		<div class="pull-right">
                		<a href="{{ action('Admin\ElementController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>  Back to Elements List</a>
                	</div>
	                {!! Form::open(['action' => ['Admin\ElementController@destroy', $element->id], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!}{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
