@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add a New Element</div>

                <div class="panel-body">
                	
                	@include("Admin::errors.form-errors")

                	{!! Form::open(['action' => 'Admin\ElementController@store', 'files' => true]) !!}
                		<!-- Category Field -->
                		<div class="form-group">
                		    {!! Form::label('category_id', 'Category:') !!}
                		    {!! Form::select('category_id', $categories, old('category_id'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Title Field -->
                		<div class="form-group">
                		    {!! Form::label('title', 'Title:') !!}
                		    {!! Form::text('title', old('title'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Subtitle Field -->
                		<div class="form-group">
                		    {!! Form::label('subtitle', 'Subtitle:') !!}
                		    {!! Form::text('subtitle', old('subtitle'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Excerpt Field -->
                		<div class="form-group">
                		    {!! Form::label('excerpt', 'Excerpt:') !!}
                		    {!! Form::textarea('excerpt', old('excerpt'), ['class' => 'form-control textarea-sm']) !!}
                		</div>
                		<!-- Image Field -->
                		<div class="form-group">
                		    {!! Form::label('image', 'Image:') !!} <button type="button" class="media-library-button" data-target="image"><span class="glyphicon glyphicon-folder-open"></span></button>
                		    {!! Form::text('image', old('image'), ['class' => 'form-control']) !!}
							@include("Admin::partials.media-library-popup", ["field_id" => "image"])
                		</div>
                		<!-- Content Field -->
                		<div class="form-group">
                		    {!! Form::label('body', 'Content:') !!}
                		    {!! Form::textarea('body', old('body'), ['class' => 'form-control textarea-lg textarea-editor']) !!}
							@include("Admin::partials.media-library-popup", ["field_id" => "body"])
                		</div>
                		<fieldset>
                			<legend class="scheduler-border">SEO</legend>
	                		<!-- Meta Title Field -->
	                		<div class="form-group">
	                		    {!! Form::label('meta_title', 'Meta Title:') !!}
	                		    {!! Form::text('meta_title', old('meta_title'), ['class' => 'form-control']) !!}
	                		</div>
	                		<!-- Meta Description Field -->
	                		<div class="form-group">
	                		    {!! Form::label('meta_description', 'Meta Description:') !!}
	                		    {!! Form::textarea('meta_description', old('meta_description'), ['class' => 'form-control', 'size' => '30x3']) !!}
	                		</div>
	                		<!-- Other Meta Tags Field -->
	                		<div class="form-group">
	                		    {!! Form::label('other_meta_tags', 'Other Meta Tags:') !!}
	                		    {!! Form::textarea('other_meta_tags', old('other_meta_tags'), ['class' => 'form-control textarea-sm']) !!}
	                		</div>
                		</fieldset>
                		<!-- Read Count Field -->
                		<div class="form-group">
                		    {!! Form::label('read_count', 'Read Count:') !!}
                		    {!! Form::number('read_count', old('read_count'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Rank Field -->
                		<div class="form-group">
                		    {!! Form::label('rank', 'Rank:') !!}
                		    {!! Form::number('rank', old('rank'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Date Field -->
                		<div class="form-group">
                		    {!! Form::label('published_date', 'Date:') !!}
                		    {!! Form::text('published_date', old('published_date', \Carbon\Carbon::now()->format("d.m.Y")), ['class' => 'form-control datepicker']) !!}
                		</div>
                		<!-- Is active Field -->
                		<div class="form-group">
                		    {!! Form::checkbox('status', 1, old('status', true), ['data-toggle' => "no-toggle", 'id' => "status"]) !!}
                		    {!! Form::label('status', 'Is active') !!}
                		</div>
	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\ElementController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to Elements List</a>
	                	</div>
                	{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
