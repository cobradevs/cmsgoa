<script>
$(document).ready(function(){
	$('#language_id').change(function() {
		language = $(this).val();
		if(language!="" && language>0){
			populateSelectbox(
				$("#category_id"), 
				"category", 
				"{!! action('Admin\LanguageController@list_categories') !!}/" + language,
				{!! old('category_id') !!}
			);
		}
		if(language!="" && language>0 && language != 1){
			populateSelectbox(
				$("#website_id"), 
				"website", 
				"{!! action('Admin\LanguageController@list_websites') !!}/" + language,
				{!! old('website_id') !!}
			);
			$('#websiteField').collapse('show');
		}else{
			$('#websiteField').collapse('hide');
		}
	});
	$('#language_id').trigger("change");
});
</script>