@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Add a New Language</div>

                <div class="panel-body">
                	
                	@include("Admin::errors.form-errors")

                	{!! Form::open(['action' => 'Admin\LanguageController@store', 'files' => true]) !!}
                		<!-- Name Field -->
                		<div class="form-group">
                		    {!! Form::label('name', 'Name:') !!}
                		    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Country Field -->
                		<div class="form-group">
                		    {!! Form::label('country', 'Country:') !!}
                		    {!! Form::text('country', old('country'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Locale Field -->
                		<div class="form-group">
                		    {!! Form::label('locale', 'Locale:') !!}
                		    {!! Form::text('locale', old('locale'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Slug Field -->
                		<div class="form-group">
                		    {!! Form::label('slug', 'Slug:') !!}
                		    {!! Form::text('slug', old('slug'), ['class' => 'form-control']) !!}
                		</div>
                		<!-- Is active Field -->
                		<div class="form-group">
                		    {!! Form::label('is_active', 'Is active') !!}
                		    {!! Form::checkbox('is_active', 1, old('is_active'), ['data-toggle' => "toggle"]) !!}
                		</div>
	                	<button type="submit" name="submit" class="btn btn-warning"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Save</button>
	                	<div class="pull-right">
	                		<a href="{{ action('Admin\LanguageController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span> Back to Languages List</a>
	                	</div>
                	{!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
