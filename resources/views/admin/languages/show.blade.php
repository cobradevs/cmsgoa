@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Language's Details: {{ $language->name }}</div>

                <div class="panel-body">
                	<p>Name: {{ $language->name }}</p>
                	<p>Country: {{ $language->country }}</p>
                	<p>Locale: {{ $language->locale }}</p>
                	<p>Slug: {{ $language->slug }}</p>
                	<p>Is active: {{ boolean_text($language->is_active) }}</p>
                </div>

                <div class="panel-footer">
        			{!! action_buttons('edit', $language, 'Language') !!}
        			{!! action_buttons('delete', $language, 'Language') !!}
            		<div class="pull-right">
                		<a href="{{ action('Admin\LanguageController@index') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>  Back to Languages List</a>
                	</div>
	                {!! Form::open(['action' => ['Admin\LanguageController@destroy', $language->id], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!}{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
