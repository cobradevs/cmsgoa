@extends('Admin::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	
        	@include("Admin::errors.form-errors")

            <div class="panel panel-default">
                <div class="panel-heading">
                	<div class="pull-right">{!! action_buttons('create', null, 'Language') !!}</div>
                	<h2 class="panel-title">Languages</h2>
                </div>
            </div>

			<table class="table table-striped" id="languages-table">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Country</th>
					<th>Locale</th>
					<th>Slug</th>
					<th class="text-right"></th>
				</tr>
			</thead>
			</table>
			{!! Form::open(['action' => ['Admin\LanguageController@destroy', 0], 'method' => 'DELETE', 'class' => 'form-inline', 'id' => 'form-delete']) !!} {!! Form::close() !!}

        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
    $('#languages-table').DataTable({
        ajax: '{!! action('Admin\LanguageController@data') !!}',
        order: [[1, 'asc']],
        @include("Admin::partials.datatables-options")
        columns: [
        	{ data: 'id', name: 'id', 'className': 'all' },
            { data: 'name', name: 'name', 'className': 'all' },
            { data: 'country', name: 'country', 'className': 'min-tablet' },
            { data: 'locale', name: 'locale', 'className': 'min-tablet' },
            { data: 'slug', name: 'slug', 'className': 'min-tablet' },
            { data: 'action', name: 'action', 'className': 'text-right text-nowrap all', 'orderable': false, 'searchable': false }
        ]
    });
});
</script>
@endpush
