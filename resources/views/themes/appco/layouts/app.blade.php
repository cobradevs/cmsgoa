<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $meta["title"] }}</title>
    <meta name="title" content="{{ $meta["title"] }}">
    <meta name="description" content="{{ $meta["description"] }}">

    <link rel="stylesheet" href="{{ asset('css/themes/'.$website->theme->folder.'/all.css') }}">
</head>
<body id="app-layout">
<header>
	<div class="container">
		<div class="row">
			<div class="col col-sm-6 hidden-xs">
				<div class="logo"><a href="{{ url($lang_slug) }}"><img src="/{{ $website->logo }}" alt="{{ $website->name }}"></a></div>
			</div>
			@if(count($website->languages)>1)
			<div class="col col-sm-6">
				<div class="language-selection text-right">
		    	@foreach($website->languages as $each_language)
		    		@if($each_language->slug == $lang_slug)
					{{ $each_language->name }} &nbsp; 
		    		@else
					<a href="/{{ $each_language->slug }}">{{ $each_language->name }}</a> &nbsp; 
					@endif
		    	@endforeach
	    		</div>
			</div>
	        @endif
		</div>
	</div>
</header>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
    	<div class="navbar-header">
			<div class="nav-right-button">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
				</button>
			</div>
			<a class="navbar-brand visible-xs" href="{{ url($lang_slug) }}"><img src="/{{ $website->logo }}" alt="{{ $website->name }}"></a>
		</div>
        <div id="navbar" class="navbar-collapse collapse">
            <!-- Left Side Of Navbar -->
            {!! html_list($menu_items[1], "menu_item_readonly", $lang_slug) !!}
        </div>
    </div>
</nav>

@yield("content")

<footer>
	<div class="container">
		<div class="col col-sm-9 hidden-xs">
			{!! html_list($menu_items[2], "menu_item_readonly", $lang_slug) !!}
		</div>
		<div class="col col-sm-3 text-right">
			&copy; {{ date("Y") }} {{ $website->name }}
		</div>
	</div>
</footer>

<script src="{{ asset('js/themes/'.$website->theme->folder.'/all.js') }}"></script>
@stack("scripts")

</body>
</html>
