@extends('Theme::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <h2>News Index Page</h2>

					<ul>
                    @foreach($elements as $element)
					<li><a href="{{ action("PageController@render", ["lang"=>$lang_slug, "slug_1"=>$category->slug, "slug_2"=>$element->slug]) }}">{{ $element->title }}</a></li>
                    @endforeach
                    </ul>
					
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
