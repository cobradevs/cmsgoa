@extends('Theme::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <h2>Element Item Page</h2>

                    <pre>
					<?php
					   print_r($element);
					?>
                    </pre>

                    @if(count($website->languages)>1)
                    	<p>Select language (Default: {{ $website->default_language->name }})</p>
                    	<ul>
                    	@foreach($website->languages as $each_language)
							<li><a href="/{{ $each_language->slug }}">{{ $each_language->name }}</a></li>
                    	@endforeach
                    	</a>
                    @endif
					
					<br><br>

					<ul>
                    @foreach($website->pages->where("language_id", $language->id) as $each_page)
					<li><a href="{{ action('PageController@render', [$lang_slug, "slug"=>$each_page->slug]) }}">{{ $each_page->title }}</a></li>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
