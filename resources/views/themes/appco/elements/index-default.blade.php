@extends('Theme::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <h2>Elements Index Page</h2>

                    <pre>
					<?php
					print_r($category->elements);
					?>
                    </pre>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
