@extends('Theme::layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <h2>Welcome to {{ $website->name }}!</h2>

					{!! html_list($pages, "page_readonly", $lang_slug) !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
