@extends('Theme::layouts.app')

@section('content')
<div class="container">
    <div class="row">
    	<div class="col-md-3">
    		{!! html_list($pages, "page_readonly", $lang_slug) !!}
    	</div>
        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <h1>{{ $page->title }}!</h1>
					
					@foreach($regions as $region_id => $region_title)
					<h2>{{ $region_title }}</h2>
					<div>
						@foreach($blocks[$region_id] as $block)

							<?php
							$block_file = "Theme::blocks.".$block->block_type;
							if(!view()->exists($block_file)) $block_file = "Theme::blocks.default";
							?>
							@include($block_file, ["data" => json_decode($block->data)])

						@endforeach
					</div>
					@endforeach

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
