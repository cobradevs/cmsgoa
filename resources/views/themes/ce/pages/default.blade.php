@extends('Theme::layouts.app')

@section('content')

@include('Theme::partials.banner')

<div class="container page-content">
    <div class="row">
        <div class="col col-md-12 main">
            <h1>{{ $page->title }}!</h1>
			
			@if(isset($regions))
				@foreach($regions as $region_id => $region_title)
				<div>
					{!! get_blocks_html($blocks[$region_id]) !!}
				</div>
				@endforeach
			@endif
        </div>
    </div>
</div>

@endsection
