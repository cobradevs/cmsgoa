@extends('Theme::layouts.app')


@section('content')

@include('Theme::partials.banner')


			<div class="row about space-2">
				<div class="col-md-6">
					<h2>About comparison experts</h2>
					<div class="infotext">
						<p>We’re here to make sure your prospective customers find your business online and then walk through your door. From getting you set up and noticed online with a stunning 360° tour of your business, to taking care of every last one of your day-to-day digital marketing needs, Cube Online has your online world completely covered.</p>

	<p>We’re so good at what we do, we’ve achieved ‘Google Trusted agency’ status.</p>

	<p>Every last detail of our business has been thoroughly vetted by Google to ensure we are highly skilled and capable of delivering...</p>
					</div>
					<div class="readmore" ><a href="/" class="btn btn-primary" role="button"><img src="img/themes/ce/readmore.png" alt="read more" />&nbsp;Read more</a></div>
					</div>
				<div class="col-md-6 info-image"><img src="img/themes/ce/saving-money-with-comparison.png" alt="saving money with comparison" /></div>

			</div>
			<div class="row stats space-2">
				<div class="col-md-6 info-image"><img src="img/themes/ce/saving-money-with-comparison.png" alt="saving money with comparison" /></div>
				<div class="col-md-6">
					<h2>About comparison experts</h2>

					<div class="infotext">
						<p>We’re here to make sure your prospective customers find your business online and then walk through your door. From getting you set up and noticed online with a stunning 360° tour of your business, to taking care of every last one of your day-to-day digital marketing needs, Cube Online has your online world completely covered.</p>

	<p>We’re so good at what we do, we’ve achieved ‘Google Trusted agency’ status.</p>

	<p>Every last detail of our business has been thoroughly vetted by Google to ensure we are highly skilled and capable of delivering...</p>
					</div>

					<div class="readmore" ><a href="/" class="btn btn-primary" role="button"><img src="img/themes/ce/readmore.png" alt="read more" />&nbsp;Read more</a></div>

				</div>
			</div>
			<div class="row process space-2">
				<div class="col-md-6 info-image process-mobile"><img src="img/themes/ce/saving-money-with-comparison.png" alt="saving money with comparison" /></div>
				<div class="col-md-6 textbox">
					<h2>About comparison experts</h2>
					<div class="infotext">
						<p>We’re here to make sure your prospective customers find your business online and then walk through your door. From getting you set up and noticed online with a stunning 360° tour of your business, to taking care of every last one of your day-to-day digital marketing needs, Cube Online has your online world completely covered.</p>

	<p>We’re so good at what we do, we’ve achieved ‘Google Trusted agency’ status.</p>

	<p>Every last detail of our business has been thoroughly vetted by Google to ensure we are highly skilled and capable of delivering...</p>
					</div>
					<div class="readmore" ><a href="/" class="btn btn-primary" role="button"><img src="img/themes/ce/readmore.png" alt="read more" />&nbsp;Read more</a></div>
				</div>
				<div class="col-md-6 info-image process-desktop"><img src="img/themes/ce/saving-money-with-comparison.png" alt="saving money with comparison" /></div>

			</div>
			<div class="row clients">
			    <div class="header-text">
					<h2>Our suppliers selection</h2>
				    <div class="clearfix"></div>
				</div>

					<div class="client-gallery">

						<div class="row list-inline">
				 			<div class="col-6 col-md-4 col-lg-2 ">
							     <div class="grey-box">
								     <a href="#">
									     <div class="overlay"></div>
									     <figure>
									     	<img src="img/themes/ce/client-thumb.jpg" alt="gas electricity" class="block-image m-x-auto">
									     </figure>
								     </a>
							     </div>
						    </div>
				 			<div class="col-6 col-md-4 col-lg-2">
							     <div class="grey-box">
								     <a href="#">
									     <div class="overlay"></div>
									     <figure>
									     	<img src="img/themes/ce/client-thumb.jpg" alt="credit cards" class="block-image m-x-auto">
									     </figure>
								     </a>
							     </div>
						    </div>
				 			<div class="col-6 col-md-4 col-lg-2">
							     <div class="grey-box">
								     <a href="#">
									     <div class="overlay"></div>
									     <figure>
									     	<img src="img/themes/ce/client-thumb.jpg" alt="tv, phone &amp; broadband" class="block-image m-x-auto">
									     </figure>
								     </a>
							     </div>
						    </div>
				 			<div class="col-6 col-md-4 col-lg-2 ">
							     <div class="grey-box">
								     <a href="#">
									     <div class="overlay"></div>
									     <figure>
									     	<img src="img/themes/ce/client-thumb.jpg" alt="Loans" class="block-image m-x-auto">
									     </figure>
								     </a>
							     </div>
						    </div>
				 			<div class="col-6 col-md-4 col-lg-2">
							     <div class="grey-box">
								     <a href="#">
									     <div class="overlay"></div>
									     <figure>
									     	<img src="img/themes/ce/client-thumb.jpg" alt="car insurance" class="block-image m-x-auto">
									     </figure>
								     </a>
							     </div>
						    </div>
				 			<div class="col-6 col-md-4 col-lg-2">
							     <div class="grey-box">
								     <a href="#">
									     <div class="overlay"></div>
									     <figure>
									     	<img src="img/themes/ce/client-thumb.jpg" alt="Life insurance" class="block-image m-x-auto">
									     </figure>
								     </a>
							     </div>
						    </div>
				 			<div class="col-6 col-md-4 col-lg-2">
							     <div class="grey-box">
								     <a href="#">
									     <div class="overlay"></div>
									     <figure>
									     	<img src="img/themes/ce/client-thumb.jpg" alt="Pensions" class="block-image m-x-auto">
									     </figure>
								     </a>
							     </div>
						    </div>
				 			<div class="col-6 col-md-4 col-lg-2">
							     <div class="grey-box">
								     <a href="#">
									     <div class="overlay"></div>
									     <figure>
									     	<img src="img/themes/ce/client-thumb.jpg" alt="Mortgages" class="block-image m-x-auto">
									     </figure>
								     </a>
							     </div>
						    </div>
				 			<div class="col-6 col-md-4 col-lg-2 ">
							     <div class="grey-box">
								     <a href="#">
									     <div class="overlay"></div>
									     <figure>
									     	<img src="img/themes/ce/client-thumb.jpg" alt="Mobiles" class="block-image">
									     </figure>
								     </a>
							     </div>
						    </div>
				 			<div class="col-6 col-md-4 col-lg-2">
							     <div class="grey-box">
								     <a href="#">
									     <div class="overlay"></div>
									     <figure>
									     	<img src="img/themes/ce/client-thumb.jpg" alt="Current Account" class="block-image">
									     </figure>
								     </a>
							     </div>
						    </div>
				 			<div class="col-6 col-md-4 col-lg-2">
							     <div class="grey-box">
								     <a href="#">
									     <div class="overlay"></div>
									     <figure>
									     	<img src="img/themes/ce/client-thumb.jpg" alt="travel insurance" class="block-image">
									     </figure>
								     </a>
							     </div>
						    </div>
				 			<div class="col-6 col-md-4 col-lg-2">
							     <div class="grey-box">
								     <a href="#">
									     <div class="overlay"></div>
									     <figure>
									     	<img src="img/themes/ce/client-thumb.jpg" alt="home insurance" class="block-image">
									     </figure>
								     </a>
							     </div>
						    </div>
						</div>
					</div>

			</div>

			<div class="row contacts">

				<div class="col-md-6 contact">
					<h2>Contact us</h2>

					<div class="infotext">
						<p>We’re here to make sure your prospective customers find your business online and then walk through your door. </p>
					</div>


					<div class="contact-table clearfix">
						<div class="contact-image">
							<img src="img/themes/ce/contact-comparison.jpg">
						</div>
						<div class="contact-text">
							<p><a class="btn btn-primary contact-button " href="tel:08002982263" role="button">0800 298 2263</a></p>
							<p><a class="btn btn-primary contact-button " href="tel:02074828485" role="button">0207 482 8485</a></p>
						</div>
					</div>
					<div class="contact-table clearfix">
						<div class="contact-image">
							<img src="img/themes/ce/find-comparison.jpg">
						</div>
						<div class="contact-text">
							<p class="btn btn-primary contact-button ">Highgate Studios, 53-79 Highgate Rd</p>
							<p class="contact-city">London NW5 1TL</p>
						</div>
					</div>
					<div class="contact-table clearfix">
						<div class="contact-image">
							<img src="img/themes/ce/mail-comparison.jpg">
						</div>
						<div class="contact-text">
							<p class="email-text"><a class="btn btn-primary contact-button" href="mailto:customerservice@comparisonexperts.com">customerservice@comparisonexperts.com</a></p>
						</div>
					</div>
				</div>
				<div class="col-md-6 email">

					<div class="form-wrapper">
						<h2>OR SEND US A MESSAGE</h2>
						<form method="post" accept-charset="utf-8" enctype="multipart/form-data" class="form basic-grey" action="/contact">
						  <div class="form-group row1">
						    <input type="text" class="form-control shortfield" id="Name" placeholder="Name">
						    <input type="text" class="form-control shortfield namefield-right" id="Phone" placeholder="Phone number">
						  </div>

						  <div class="form-group">
						    <input type="text" class="form-control" id="Email" placeholder="Email Address">
						  </div>
						  <div class="form-group">
							<textarea class="form-control" id="Message" rows="5" placeholder="Message"></textarea>
						  </div>
						  <div class="form-group-buttons">
						   <button type="submit" class="btn btn-primary contact-button">Submit</button>
						  </div>
						</form>


					</div>


				</div>

			</div>

@endsection
