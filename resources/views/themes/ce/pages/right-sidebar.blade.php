@extends('Theme::layouts.app')

@section('content')

@include('Theme::partials.banner')

<div class="container page-content">
    <div class="row">
        <div class="col col-md-8 main">
		
            <h1>{{ $page->title }}</h1>
			
			{!! get_blocks_html($blocks["content"]) !!}
		
        </div>
    	<div class="col col-md-4 sidebar">
		
    		{!! get_blocks_html($blocks["sidebar"]) !!}
		
    	</div>
    </div>
</div>
@endsection
