        
        	<h3>Most Read</h3>
        	<ul class="list-unstyled">
        	@foreach($top_elements as $top_element)
        	<li>
				<h4><a href="{{ action("PageController@render", ["lang"=>$lang_slug, "slug_1"=>$top_element->category->slug, "slug_2"=>$top_element->slug]) }}">{!! $top_element->title !!}</a></h4>
				<p class="news-date">{{ $top_element->published_date }}</p>
			</li>
        	@endforeach
        	</ul>
