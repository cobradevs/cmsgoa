
@if(Request::is('/'))
		    <div id="banner" class="header-text">
				<h1>COMPARE, SELECT AND SAVE</h1>
			    <div class="clearfix"></div>
			</div>
			<div class="gallery space-3">
				<div class="row list-inline">
		 			<div class="col-6 col-md-4 col-lg-2 dark-orange-box box">
					     <div class="dark-orange-container">
						     <a href="https://comparisonexperts.fmaccount.com/gas-electricity/">
							     <div class="overlay"></div>
							     <figure>
							     	<img src="img/themes/ce/gas-electricity.png" alt="gas electricity" class="block-image m-x-auto">
							     </figure>
						     </a>
					     </div>
				    </div>
		 			<div class="col-6 col-md-4 col-lg-2 medium-orange-box box">
					     <div class="medium-orange-container">
						     <a href="https://comparisonexperts.fmaccount.com/creditcards#/">
							     <div class="overlay"></div>
							     <figure>
							     	<img src="img/themes/ce/credit-card.png" alt="credit cards" class="block-image m-x-auto">
							     </figure>
						     </a>
					     </div>
				    </div>
		 			<div class="col-6 col-md-4 col-lg-2 light-orange-box box">
					     <div class="light-orange-container">
						     <a href="http://comparisonexperts.broadbandplease.co.uk/">
							     <div class="overlay"></div>
							     <figure>
							     	<img src="img/themes/ce/broadband.png" alt="tv, phone &amp; broadband" class="block-image m-x-auto">
							     </figure>
						     </a>
					     </div>
				    </div>
		 			<div class="col-6 col-md-4 col-lg-2  pale-orange-box box">
					     <div class="pale-orange-container">
						     <a href="https://comparisonexperts.fmaccount.com/Loans#/">
							     <div class="overlay"></div>
							     <figure>
							     	<img src="img/themes/ce/loan.png" alt="Loans" class="block-image m-x-auto">
							     </figure>
						     </a>
					     </div>
				    </div>
		 			<div class="col-6 col-md-4 col-lg-2  dark-yellow-box box">
					     <div class="dark-yellow-container">
						     <a href="https://comparisonexperts.fmaccount.com/CarInsurance#/">
							     <div class="overlay"></div>
							     <figure>
							     	<img src="img/themes/ce/car-insurance.png" alt="car insurance" class="block-image m-x-auto">
							     </figure>
						     </a>
					     </div>
				    </div>
		 			<div class="col-6 col-md-4 col-lg-2  yellow-box  box">
					     <div class="yellow-container">
						     <a href="#">
							     <div class="overlay"></div>
							     <figure>
							     	<img src="img/themes/ce/health-insurance.png" alt="Life insurance" class="block-image m-x-auto">
							     </figure>
						     </a>
					     </div>
				    </div>
		 			<div class="col-6 col-md-4 col-lg-2  dark-blue-box box">
					     <div class="dark-blue-container">
						     <a href="/pensions">
							     <div class="overlay"></div>
							     <figure>
							     	<img src="img/themes/ce/savings.png" alt="Pensions" class="block-image m-x-auto">
							     </figure>
						     </a>
					     </div>
				    </div>
		 			<div class="col-6 col-md-4 col-lg-2  medium-blue-box box">
					     <div class="medium-blue-container">
						     <a href="https://comparisonexperts.fmaccount.com/mortgages/">
							     <div class="overlay"></div>
							     <figure>
							     	<img src="img/themes/ce/mortgage.png" alt="Mortgages" class="block-image m-x-auto">
							     </figure>
						     </a>
					     </div>
				    </div>
		 			<div class="col-6 col-md-4 col-lg-2  light-blue-box box">
					     <div class="light-blue-container">
						     <a href="http://comparisonexperts.mobilesplease.co.uk/">
							     <div class="overlay"></div>
							     <figure>
							     	<img src="img/themes/ce/mobile.png" alt="Mobiles" class="block-image">
							     </figure>
						     </a>
					     </div>
				    </div>
		 			<div class="col-6 col-md-4 col-lg-2  pale-blue-box box">
					     <div class="pale-blue-container">
						     <a href="https://comparisonexperts.fmaccount.com/CurrentAccounts">
							     <div class="overlay"></div>
							     <figure>
							     	<img src="img/themes/ce/cash.png" alt="Current Account" class="block-image">
							     </figure>
						     </a>
					     </div>
				    </div>
		 			<div class="col-6 col-md-4 col-lg-2  dark-green-box box">
					     <div class="dark-green-container">
						     <a href="#">
							     <div class="overlay"></div>
							     <figure>
							     	<img src="img/themes/ce/travel-insurance.png" alt="travel insurance" class="block-image">
							     </figure>
						     </a>
					     </div>
				    </div>
		 			<div class="col-6 col-md-4 col-lg-2  green-box box">
					     <div class="green-container">
						     <a href="#">
							     <div class="overlay"></div>
							     <figure>
							     	<img src="img/themes/ce/house-insurance.png" alt="home insurance" class="block-image">
							     </figure>
						     </a>
					     </div>
				    </div>
				</div>
			</div>
@else
<div class="ws-container">
	<div class="banner">
		@if(isset($banner))
			<img src="/websites/{{ $website->id }}/{{ $banner }}" class="img-responsive">
		@elseif(isset($blocks["banner"]) && isset(array_values($blocks["banner"]->toArray())[0]['data']))
			<img src="/websites/{{ $website->id }}/{{ json_decode(array_values($blocks["banner"]->toArray())[0]['data'])->file }}" class="img-responsive">
			@if(isset(array_values($blocks["banner"]->toArray())[1]['data']) || isset(array_values($blocks["banner"]->toArray())[2]['data']))
			<div class="container">
				<div class="banner-caption">
					@if(isset(array_values($blocks["banner"]->toArray())[1]['data']))
						@include('Theme::blocks.title', ['data' => json_decode(array_values($blocks["banner"]->toArray())[1]['data'])])
					@endif
							<div>
					@if(isset(array_values($blocks["banner"]->toArray())[2]['data']))
						@include('Theme::blocks.text', ['data' => json_decode(array_values($blocks["banner"]->toArray())[2]['data'])])
					@endif
					</div>
				</div>
			</div>
			@endif
		@else
			<img src="/websites/{{ $website->id }}/media-library/images/banners/default.jpg" class="img-responsive">
		@endif
	</div>
	@if(isset($breadcrumbs) && count($breadcrumbs) > 1)
	<div class="breadcrumb-wrapper">
		<ol class="breadcrumb">
			@foreach ($breadcrumbs as $slug => $title)
				<li><a href="{!! (substr($slug,0,1)=="/") ? "" : "/" !!}{{ $slug }}">{{ $title }}</a></li>
			@endforeach
		</ol>
	</div>
	@endif
</div>

@endif