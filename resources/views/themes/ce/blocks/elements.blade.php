<?php
switch($data->category){
	case(4):
	case(5):
	default:
?>
<div class="testimonials">

	<h2>Testimonials</h2>

	<div id="testimonials-carousel" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<?php $count = 0 ?>
			@foreach($data->elements as $element)
				<li data-target="#testimonials-carousel" data-slide-to="{{ $count }}" class="{{ ($count==0) ? "active" : "" }}""></li>
				<?php $count++ ?>
			@endforeach
		</ol>
		<div class="carousel-inner" role="listbox">
			<?php $count = 0 ?>
			@foreach($data->elements as $element)
				<?php $count++ ?>
				<div class="item{{ ($count==1) ? " active" : "" }}">
					<p>{!! strip_tags($element->body) !!}</p>
					<h4>{!! $element->title !!}@if($element->subtitle), {!! $element->subtitle !!} @endif</h4>
				</div>
			@endforeach
		</div>
	</div>

</div>
<?php
	break;
}
?>