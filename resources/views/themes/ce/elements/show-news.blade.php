@extends('Theme::layouts.app')

@section('content')

@include('Theme::partials.banner')

<div class="container page-content">
    <div class="row news-details news-item">
        <div class="col-md-9">
        	<div class="row">
        		<div class="col col-sm-12">
            		<h1>{!! $element->title !!}</h1>
					@if($element->image)
					<div class="news-image pull-left">
						<img src="/websites/{{ $website->id }}/{{ $element->image }}" class="img-responsive">
					</div>
					@endif
					<div class="news-body">
						{!! $element->body !!}
					</div>
        		</div>
        	</div>
        </div>
        <div class="col-md-3 news-most-read-list">
			@include("Theme::partials.top-elements")
		</div>
    </div>
</div>
@endsection
