@extends('Theme::layouts.app')

@section('content')

@include('Theme::partials.banner')
<div class="container element-item page-content">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
            <h1>{!! $element->title !!}</h1>
		</div>
	</div>
    <div class="row">
        <div class="col-md-6 col-md-offset-1">
            {!! $element->body !!}
        </div>
        <div class="col-md-4 text-center">
        	<img src="/websites/{{ $website->id }}/{{ $element->image }}" class="img-responsive img-thumbnail">
        </div>
    </div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
            <a href="{!! url()->previous() !!}">{!! trans("cms.Go back") !!}</a>
		</div>
	</div>
</div>
@endsection

