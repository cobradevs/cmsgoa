@extends('Theme::layouts.app')

@section('content')

@include('Theme::partials.banner', ["banner" => $settings["news-banner"]])

<div class="container page-content">
    <div class="row news-list">
        <div class="col-md-9">
            <h1>News</h1>
            @foreach($elements as $element)
            <div class="row news-item">
				<div class="col col-sm-5">
					@if($element->image)
					<div class="news-thumbnail">
						<a href="{{ action("PageController@render", ["lang"=>$lang_slug, "slug_1"=>$category->slug, "slug_2"=>$element->slug]) }}"><img src="/websites/{{ $website->id }}/{{ $element->image }}" class="img-responsive"></a>
					</div>
					@endif
				</div>
				<div class="col col-sm-7">
					<h2><a href="{{ action("PageController@render", ["lang"=>$lang_slug, "slug_1"=>$category->slug, "slug_2"=>$element->slug]) }}">{{ $element->title }}</a></h2>
					<p class="news-date">{{ $element->published_date }}</p>
					<p class="news-excerpt">
						{{ $element->excerpt }}
					</p>
					<p class="news-read-more">
						<a href="{{ action("PageController@render", ["lang"=>$lang_slug, "slug_1"=>$category->slug, "slug_2"=>$element->slug]) }}">{{ trans("cms.Read more") }}</a>
					</p>
				</div>
            </div>
            @endforeach
        </div>
        <div class="col-md-3 news-most-read-list">
			@include("Theme::partials.top-elements")
		</div>
    </div>
</div>
@endsection
