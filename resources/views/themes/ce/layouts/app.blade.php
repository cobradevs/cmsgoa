<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $meta["title"] }}</title>
    <meta name="title" content="{{ $meta["title"] }}">
    <meta name="description" content="{{ $meta["description"] }}">

	@if(isset($website->favicon))
		<link href="/{!! $website->favicon !!}" type="image/x-icon" rel="icon">
	@endif
	@if(empty($lang_slug))
		{{ $lang_slug = '' }}
	@endif
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{ asset('css/themes/'.$website->theme->folder.'/all.css') }}">
</head>
<body id="app-layout" class="app-layout-{{ $lang_slug }} @if(isset($page->template) && $page->template) {{ $page->template }} @endif">
	<div class="mx-auto container">
	    <div class="wrapper vertical-align">
			<header>
				<nav class="navbar navbar-light ">
				  <div class="navbar-header">
				      <a class="navbar-brand ce-brand" href="{{ url($lang_slug) }}"><img src="/{{ $website->logo }}" alt="{{ $website->name }}"></a>
					  <div class="desktop-only menu-buttons">
						  <ul class="menu-list">
							  <li><button type="button" class="btn btn-info "><a href="#">Money Manager</a></button></li>
							  <li><button type="button" class="btn btn-info "><a href="#">Call us on 0207 482 8485</a></button></li>
						  </ul>
					  </div>
				  </div>
				<!-- navbar-toggler ce-button pull-xs-right navbar-toggler-right  -->
				  <button class="navbar-toggler c-hamburger c-hamburger--htx navbar-toggler-right" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar">
				    <span></span>
				  </button>
				  <div class="collapse navbar-toggleable-lg" id="exCollapsingNavbar">
				    <ul class="nav navbar-nav ">
				      {!! html_list($menu_items[1], "menu_item_ce_theme", $lang_slug, [], "", "") !!}
				    </ul>
				  </div>
				</nav>

			</header>

			@yield("content")

		<footer class="row bd-footer">
			<div class="col-md-4 col-sm-12 brandname">{{ $website->name }} &copy; {{ date("Y") }}</div>
			<div class="col-md-8 col-sm-12 links">
				{!! html_list($menu_items[2], "footer_menu", $lang_slug) !!}
			</div>
		</footer>
		</div>
	</div>
<script src="{{ asset('js/themes/'.$website->theme->folder.'/all.js') }}"></script>
@stack("scripts")

</body>
</html>
