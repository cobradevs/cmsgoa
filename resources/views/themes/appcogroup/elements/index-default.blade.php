@extends('Theme::layouts.app')

@section('content')

@include('Theme::partials.banner')

<div class="container page-content">
    <div class="row news-list">
        <div class="col-md-10 col-md-offset-1">
            <h1>{{ $category->name }}</h1>
            @foreach($elements as $element)
            <div class="row news-item">
				<div class="col col-xs-3 col-sm-2">
					@if($element->image)
					<div class="news-thumbnail">
						@if(!$element->alttext)
							 <?php $element->alttext ="image" ?>
						@endif
						<a href="{{ action("PageController@render", ["lang"=>$lang_slug, "slug_1"=>$category->slug, "slug_2"=>$element->slug]) }}"><img src="/websites/{{ $website->id }}/{{ $element->image }}" class="img-responsive" alt="{{ $element->alttext }}"></a>
					</div>
					@endif
				</div>
				<div class="col col-xs-9 col-sm-10">
					<h2><a href="{{ action("PageController@render", ["lang"=>$lang_slug, "slug_1"=>$category->slug, "slug_2"=>$element->slug]) }}">{{ $element->title }}</a></h2>
					<p class="news-excerpt">
						{{ $element->excerpt }}
					</p>
					<p class="news-read-more">
						<a href="{{ action("PageController@render", ["lang"=>$lang_slug, "slug_1"=>$category->slug, "slug_2"=>$element->slug]) }}">{{ trans("cms.Read more") }}</a>
					</p>
				</div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
