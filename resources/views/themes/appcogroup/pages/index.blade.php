@extends('Theme::layouts.app')

@section('content')

@include('Theme::partials.banner')

<div class="container page-content">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2>Welcome to {{ $website->name }}!</h2>

					{!! html_list(list_nested($website->pages->where("parent_id", 0)->where("status", 100)), "page_readonly", $lang_slug) !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
