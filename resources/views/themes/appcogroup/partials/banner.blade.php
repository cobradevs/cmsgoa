<div class="ws-container">
	<div class="banner">
		@if(isset($banner))
			<img src="/websites/{{ $website->id }}/{{ $banner }}" class="img-responsive" alt="{{ $alttext}}">
		@elseif(isset($blocks["banner"]) && isset(array_values($blocks["banner"]->toArray())[0]['data']))
			<img src="/websites/{{ $website->id }}/{{ json_decode(array_values($blocks["banner"]->toArray())[0]['data'])->file }}" class="img-responsive" alt="{{ trans('cms.News Banner text') }}">
			@if(isset(array_values($blocks["banner"]->toArray())[1]['data']) || isset(array_values($blocks["banner"]->toArray())[2]['data']))
			<div class="container">
				<div class="banner-caption">
					@if(isset(array_values($blocks["banner"]->toArray())[1]['data']))
						@include('Theme::blocks.title', ['data' => json_decode(array_values($blocks["banner"]->toArray())[1]['data'])])
					@endif
							<div>
					@if(isset(array_values($blocks["banner"]->toArray())[2]['data']))
						@include('Theme::blocks.text', ['data' => json_decode(array_values($blocks["banner"]->toArray())[2]['data'])])
					@endif
					</div>
				</div>
			</div>
			@endif
		@else
			<img src="/websites/{{ $website->id }}/media-library/images/banners/default.jpg" class="img-responsive" alt="{{ trans('cms.News Banner text') }}">
		@endif
	</div>
	@if(isset($breadcrumbs) && count($breadcrumbs) > 1)
	<div class="breadcrumb-wrapper">
		<ol class="breadcrumb">
			@foreach ($breadcrumbs as $slug => $title)
				<li><a href="{!! (substr($slug,0,1)=="/") ? "" : "/" !!}{{ $slug }}">{{ $title }}</a></li>
			@endforeach
		</ol>
	</div>
	@endif
</div>
