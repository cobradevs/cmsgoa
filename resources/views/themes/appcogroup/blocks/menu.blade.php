
<div class="side-menu">
	@if($pages->where("id", $data->parent_id)->first())
		<h3><a href="{!! get_full_url($pages->where("id", $data->parent_id)->first(), $lang_slug) !!}">{!! ($data->parent_id) ? $pages->where("id", $data->parent_id)->first()->title : "" !!}</a></h3>
	@endif

	@if(count($pages->where("parent_id", $data->parent_id)))
		{!! html_list(list_nested($pages->where("parent_id", $data->parent_id)), "page_readonly", $lang_slug, ($data->parent_id) ? [$pages->where("id", $data->parent_id)->first()->slug] : []) !!}
	@endif
</div>
