<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Media Library</title>
    <link rel="stylesheet" href="{{ asset('vendor/jquery-ui/themes/smoothness/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset($dir . '/css/elfinder.min.css') }}">
    <link rel="stylesheet" href="{{ asset($dir . '/css/theme-bootstrap-libreicons-svg.css') }}">
    @if($locale)
        <script src="{{ asset($dir . '/js/i18n/elfinder.'.$locale.'.js') }}"></script>
    @endif
</head>
<body>

<div id="elfinder"></div>

<script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset($dir . '/js/elfinder.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function () {
    var elf = $('#elfinder').elfinder({
        @if($locale)
            lang: '{{ $locale }}',
        @endif
        height: "560",
        customData: { 
            _token: '{{ csrf_token() }}'
        },
        url: '{{ route("elfinder.connector", [$website_id, $language_id]) }}',
        dialog: {width: 900, modal: true, title: 'Select a file'},
        resizable: false,
        getFileCallback: function (file) {
            window.parent.processSelectedFile(file, '{{ $input_id }}');
            window.parent.$('#{{ $input_id }}MediaLibrary').modal('hide');
        }
    }).elfinder('instance');
});
</script>
</body>
</html>
