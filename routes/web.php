<?php
Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

/**
 * Admin & Editor routes
 */


Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {

	/**
	 * Admin & Editor routes
	 */
	Route::group(['middleware' => ['auth']], function () {

		Route::get('/', 'StaticPageController@home');

		Route::get('languages/list_categories/{language_id?}', 'LanguageController@list_categories');
	    Route::get('languages/list_websites/{language_id?}', 'LanguageController@list_websites');

	});

	/**
	 * Admin routes
	 */

	Route::group(['middleware' => ['auth','admin']], function () {

	    Route::any('users/data', 'UserController@data');
	    Route::resource('users', 'UserController');

	    Route::any('websites/data', 'WebsiteController@data');
	    Route::resource('websites', 'WebsiteController');
	    Route::get('websites/{websites}/duplicate', 'WebsiteController@duplicate');

	    Route::any('domains/data', 'DomainController@data');
	    Route::resource('domains', 'DomainController');

	    Route::any('themes/data', 'ThemeController@data');
	    Route::resource('themes', 'ThemeController');

	    Route::any('languages/data', 'LanguageController@data');
	    Route::resource('languages', 'LanguageController');

	    Route::any('elements/data', 'ElementController@data');
	    Route::resource('elements', 'ElementController');

	    Route::any('categories/data', 'CategoryController@data');
	    Route::resource('categories', 'CategoryController');

	    Route::post('assets/upload_image', 'AssetController@upload_image');
	    Route::get('logs/{username}/activity', 'ActivitiesController@show');
	    Route::get('logs/activity/{userid}', 'ActivitiesController@show');

	});

	/**
	 * Editor routes
	 */
	Route::group(['prefix' => 'website/{website_id}/{language_id}', 'middleware' =>  ['auth','editor']], function () {

		Route::any('pages/data', 'WebsitePageController@data');
		Route::resource('pages', 'WebsitePageController');
		Route::get('pages/{pages}/duplicate', 'WebsitePageController@duplicate');
		Route::post('pages/save_order', 'WebsitePageController@save_order');

		Route::any('elements/data', 'WebsiteElementController@data');
		Route::resource('elements', 'WebsiteElementController');
		Route::get('elements/{elements}/duplicate', 'WebsiteElementController@duplicate');

		Route::any('news/data', 'WebsiteNewsController@data');
		Route::resource('news', 'WebsiteNewsController');
		Route::get('news/{news}/duplicate', 'WebsiteNewsController@duplicate');

		Route::resource('menu.menu_items', 'WebsiteMenuItemController');
		Route::get('menu/{menu}/menu_items/{menu_items}/duplicate', 'WebsiteMenuItemController@duplicate');
		Route::post('menu/{menu}/save_order', 'WebsiteMenuItemController@save_order');

		Route::get('settings', 'WebsiteThemeOptionController@index');
		Route::post('settings', 'WebsiteThemeOptionController@save');
		Route::get('media-library', 'WebsiteAssetController@index');

	});

});

		/**
		 * Front-end routes
		 */

		 /** multilingual */
		$multilingual = function() {
			Route::group(['prefix' => '{lang?}'], function () {
				Route::get('{slug_1?}/{slug_2?}/{slug_3?}/{slug_4?}/{slug_5?}/', 'PageController@render')->where('lang', '[a-z]{1,2}');

			});
	 	};
		Route::group(['domain' => 'cms.cobracreative.co.uk'], $multilingual );
		Route::group(['domain' => 'goacms.app'], $multilingual );

		/**  monolingual */
		$monolang = function() {
			Route::get('{slug_1?}/{slug_2?}/{slug_3?}/{slug_4?}/{slug_5?}/', 'PageController@render')->where('lang', '[a-z]{1,2}');
		};

		Route::group(['domain' => 'ce.local'], $monolang );

		Route::group(['domain' => 'appcogroup.local.com'], $monolang );


