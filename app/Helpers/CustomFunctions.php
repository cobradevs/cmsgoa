<?php

function flash($type="success", $title=null, $message=null)
{
	session()->flash('status',
		[
			'type'		=> $type,
			'title'		=> $title,
			'message'	=> $message
		]
	);
}

function action_buttons($type="all", $action_parameters, $model)
{
	$buttons = "";
	$model_display_name = str_replace(["WebsitePage", "WebsiteElement", "WebsiteMenuItem", "WebsiteNews"], ["Page", "Element", "Menu Item", "News"], $model);
	if($type=="create" || in_array("create", explode(",",$type))){
		if($action_parameters)
			$target = action('Admin\\'.$model.'Controller@create', $action_parameters);
		else
			$target = action('Admin\\'.$model.'Controller@create');
		$buttons .= '<a href="'.$target.'" class="btn btn-xs btn-default"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span><span class=""> Add '.str_singular($model_display_name).'</span></a> ';
	}
	if($type=="all" || $type=="show" || in_array("show", explode(",",$type)))
		$buttons .= '<a href="'.action('Admin\\'.$model.'Controller@show', $action_parameters).'" class="btn btn-xs btn-info" data-toggle="tooltip" data-placement="top" title="View Details"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><span class="hidden-xs hidden-sm"></span></a> ';
	if($type=="all" || $type=="edit" || in_array("edit", explode(",",$type)))
		$buttons .= '<a href="'.action('Admin\\'.$model.'Controller@edit', $action_parameters).'" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span><span class="hidden-xs hidden-sm"></span></a> ';
	if($type=="all" || $type=="duplicate" || in_array("duplicate", explode(",",$type)))
		$buttons .= '<a href="'.action('Admin\\'.$model.'Controller@duplicate', $action_parameters).'" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="top" title="Duplicate"><span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span><span class="hidden-xs hidden-sm"></span></a> ';
	if($type=="all" || $type=="delete" || in_array("delete", explode(",",$type)))
		$buttons .= '<button type="button" data-action="'.action('Admin\\'.$model.'Controller@destroy', $action_parameters).'" class="btn btn-xs btn-danger btn-delete-warning" data-toggle="tooltip" data-placement="top" title="Delete"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span><span class="hidden-xs hidden-sm"></span></button> ';
	return $buttons;
}

function boolean_text($value=0){
	return ($value) ? "Yes" : "No";
}

function default_templates(){
	return json_decode('[{"template":{"id":"front-page","name":"Front Page","regions":{"page-title":"Page Title","welcome":"Welcome","address":"Address"}}},{"template":{"id":"full-width","name":"Full Width","regions":{"banner":"Banner","left-menu":"Left Menu","content":"Content"}}},{"template":{"id":"simple-page","name":"Simple Page","regions":{"content":"Content"}}}]');
}

function default_theme_options(){
	return json_decode('[{"option":{"id":"","name":"","type":""}}]');
}

function item_statuses()
{
	return [
		0		=> "Draft",
		100 	=> "Published",
		-100	=> "Move to Trash"
	];
}

function theme_option_types()
{
	return [
		"string"	=> "String",
		"text" 		=> "Text",
		"file"		=> "File"
	];
}

function menus($menu_id=null)
{
	$menus = [
		1	=>	"Main Menu",
		2	=>	"Footer Menu",
		3	=>	"Social Menu"
	];
	if($menu_id) return $menus[$menu_id];
	return $menus;
}

function block_types()
{
	return [
		[
			"type"	=> [
				"id"		=> "title",
				"name"		=> "Title",
				"fields"	=> [
					[
						"id"			=> "tag",
						"name"			=> "Tag",
						"type"			=> "select",
						"options"		=> [
								"h1" 		=> "h1",
								"h2" 		=> "h2",
								"h3" 		=> "h3",
								"h4" 		=> "h4",
								"h5" 		=> "h5",
								"h6" 		=> "h6",
								"strong" 	=> "strong"
							]
					],
					[
						"id"			=> "content",
						"name"			=> "Content",
						"type"			=> "string",
						"max-length"	=> 60
					]
				],
				"template"	=> "<%(block_data.tag)s>%(block_data.content)s</%(block_data.tag)s>"
			]
		],
		[
			"type"	=> [
				"id"		=> "text",
				"name"		=> "Text",
				"fields"	=> [
					[
						"id"			=> "content",
						"name"			=> "Content",
						"type"			=> "text"
					]
				],
				"template"	=> "%(block_data.content)s"
			]
		],
		[
			"type"	=> [
				"id"		=> "image",
				"name"		=> "Image",
				"fields"	=> [
					[
						"id"			=> "file",
						"name"			=> "Image",
						"type"			=> "file"
					],
					[
						"id"			=> "alt",
						"name"			=> "Alt Text",
						"type"			=> "string",
						"max-length"	=> 60
					]
				],
				"template"	=> "<img src=\"/websites/%(block_meta.website_id)s/%(block_data.file)s\" class=\"img-responsive\">"
			]
		],
		[
			"type"	=> [
				"id"		=> "elements",
				"name"		=> "Elements",
				"fields"	=> [
					[
						"id"			=> "category",
						"name"			=> "Category",
						"type"			=> "elements_category"
					],
					[
						"id"			=> "list",
						"name"			=> "Elements",
						"type"			=> "elements_list"
					]
				],
				"template"	=> "<h5><strong>Elements</strong> / %(block_data.category_text)s</h5><hr><p>%(block_data.list_text)j</p>"
			]
		],
		[
			"type"	=> [
				"id"		=> "menu",
				"name"		=> "Menu",
				"fields"	=> [
					[
						"id"			=> "parent_id",
						"name"			=> "Main Parent Page",
						"type"			=> "page"
					]
				],
				"template"	=> "<h5><strong>Menu</strong> / %(block_data.parent_id_text)s</h5>"
			]
		],
		[
			"type"	=>	[
				"id"		=> "video",
				"name"		=> "Video",
				"fields"	=> [
					[
						"id"			=> "youtube_id",
						"name"			=> "Youtube ID",
						"type"			=> "string"
					],
					[
						"id"			=> "embed_code",
						"name"			=> "-or- Embed Code",
						"type"			=> "string"
					],
				],
				"template"	=> "%(block_data.youtube_id)s %(block_data.embed_code)s"
			]
		],
		[
			"type"	=> [
				"id"		=> "widget",
				"name"		=> "Widget",
				"fields"	=> [
					[
						"id"			=> "title",
						"name"			=> "Title",
						"type"			=> "string",
						"max-length"	=> 100
					],
					[
						"id"			=> "image",
						"name"			=> "Image",
						"type"			=> "file"
					],
					[
						"id"			=> "text",
						"name"			=> "Text",
						"type"			=> "text-sm"
					],
					[
						"id"			=> "url",
						"name"			=> "URL",
						"type"			=> "string"
					]
				],
				"template"	=> "%(block_data.title)s"
			]
		],
		// [
		// 	"type"	=> [
		// 		"id"		=> "banner",
		// 		"name"		=> "Banner",
		// 		"fields"	=> [
		// 			[
		// 				"id"			=> "content",
		// 				"name"			=> "Content",
		// 				"type"			=> "text"
		// 			]
		// 		],
		// 		"template"	=> "%(block_data.content)s"
		// 	]
		// ],
	];
}

function item_class($status){
	switch ($status) {
		case '100':
			return "published";
		case '-100':
			return "trash";
		case '0':
		default:
			return "draft";
	}
}

function get_full_url($page, $lang_slug=""){
	$slugs = [];
	if($lang_slug) $slugs[] = $lang_slug;

	/*
	ToDo: Fix this shit, refactor in a function
	 */
	if(isset($page->parent->parent->parent->parent)) $slugs[] = $page->parent->parent->parent->parent->slug;
	if(isset($page->parent->parent->parent)) $slugs[] = $page->parent->parent->parent->slug;
	if(isset($page->parent->parent)) $slugs[] = $page->parent->parent->slug;
	if(isset($page->parent)) $slugs[] = $page->parent->slug;

	$slugs[] = $page->slug;
	return action("PageController@render", $slugs);
}

function list_nested($items, $segments=[])
{
	$data = [];
	foreach($items as $item)
	{
		$children = null;
		if(count($item->children)) $children = list_nested($item->children);

		$class = "";
		$slug = "";
		switch($item->type){
			case(1): // Internal page
				$slug = $item->page->slug;
				if(isset($segments[1]) && $segments[1] == $slug) $class = "active";
			break;
			case(2): // Elements/News index
				$slug = $item->category->slug;
				if(isset($segments[1]) && $segments[1] == $slug) $class = "active";
			break;
			case(3): // External URL
				$slug = $item->external_url;
			break;
			default: // Not menu item, Page etc.
				$slug = $item->slug;
				$class = item_class($item->status);
				if(Request::is('*'.$slug)) $class .= " active";
			break;
		}

		$data[] = [
			'id'			=> $item->id,
			'parent_id'		=> $item->parent_id,
			'website_id'	=> $item->website_id,
			'language_id'	=> $item->language_id,
			'title' 		=> $item->title,
			'slug' 			=> $slug,
			'status' 		=> $item->status,
			'class' 		=> $class,
			'type' 			=> $item->type,
			'children' 		=> $children
		];
	}
	return $data;
}

function html_list(Array $list, $list_type = "page_readonly", $extra_parameter = "", $parent_slugs = [], $additional_tags = "", $ul_class = "")
{
	if(!count($list)) return "";

	switch($list_type){
		case("menu_item_ce_theme"):
			$html = '<ul class="nav navbar-nav '.$ul_class.'">';
			$li_class = "";
			$children_list_type = "submenu_item_readonly";
		break;
		case("footer_menu"):
			$html = '<ul class="bd-footer-links '.$ul_class.'">';
			$li_class = "";
			$children_list_type = "submenu_item_readonly";
		break;
		case("menu_item_readonly"):
			$html = '<ul class="nav navbar-nav '.$ul_class.'">';
			$li_class = "";
			$children_list_type = "submenu_item_readonly";
		break;
		case("submenu_item_readonly"):
			$html = '<ul class="dropdown-menu '.$ul_class.'" role="menu">';
			$li_class = "";
			$children_list_type = $list_type;
		break;
		default:
			$html = '<ul class="list-group '.$ul_class.'" data-parent_id="'.$list[0]["parent_id"].'">';
			$li_class = "list-group-item";
			$children_list_type = $list_type;
		break;
	}

	$i = 0;
	foreach($list as $item){

		switch($list_type){
			case("page"):
				$html .= '<li id="'.$item['id'].'" class="'.$li_class.' '.$item['class'].'">
					<div>
						<p class="pull-right">
							<a href="'.action("Admin\WebsitePageController@edit", [$item['website_id'], $item['language_id'], $item['id']]).'" data-toggle="tooltip" data-placement="top" title="Edit"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span><span class="hidden-xs hidden-sm"></span></a>
							<a href="'.action("Admin\WebsitePageController@duplicate", [$item['website_id'], $item['language_id'], $item['id']]).'" data-toggle="tooltip" data-placement="top" title="Duplicate"><span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span><span class="hidden-xs hidden-sm"></span></a>
						</p>
						<span class="glyphicon glyphicon-move"></span> <a href="'.action("Admin\WebsitePageController@edit", [$item['website_id'], $item['language_id'], $item['id']]).'">'.$item['title'].'</a>
					</div>';
			break;
			case("page_readonly"):
				$parameters = array_merge([$extra_parameter], $parent_slugs, [$item['slug']]);
				$html .= '<li id="'.$item['id'].'" class="'.$li_class.' '.$item['class'].'"><div><a href="'.action("PageController@render", $parameters).'">'.$item['title'].'</a></div>';
			break;
			case("menu_item"):
				$html .= '<li id="'.$item['id'].'" class="'.$li_class.' '.$item['class'].'">
					<div>
						<p class="pull-right">
							<a href="'.action("Admin\WebsiteMenuItemController@edit", [$item['website_id'], $item['language_id'], $extra_parameter, $item['id']]).'" data-toggle="tooltip" data-placement="top" title="Edit"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span><span class="hidden-xs hidden-sm"></span></a>
							<a href="'.action("Admin\WebsiteMenuItemController@duplicate", [$item['website_id'], $item['language_id'], $extra_parameter, $item['id']]).'" data-toggle="tooltip" data-placement="top" title="Duplicate"><span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span><span class="hidden-xs hidden-sm"></span></a>
							<a data-action="'.action("Admin\WebsiteMenuItemController@destroy", [$item['website_id'], $item['language_id'], $extra_parameter, $item['id']]).'" class="btn-delete-warning" data-toggle="tooltip" data-placement="top" title="Delete"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><span class="hidden-xs hidden-sm"></span></button>
						</p>
						<span class="glyphicon glyphicon-move"></span> <a href="'.action("Admin\WebsiteMenuItemController@edit", [$item['website_id'], $item['language_id'], $extra_parameter, $item['id']]).'">'.$item['title'].'</a>
					</div>';
			break;
			case("menu_item_ce_theme"):
				if($item["type"]==3){
					$target_url = $item['slug'];
				}else{
					$parameters = array_merge([$extra_parameter], $parent_slugs, [$item['slug']]);
					$target_url = action("PageController@render", $parameters);
				}
				if(is_array($item['children'])){
					$html .= '<li id="'.$item['id'].'" class="nav-item "'.$additional_tags.'><a href="#" class="nav-link" data-toggle="dropdown" >'.$item['title'].'</a>';
				}else{
					$html .= '<li id="'.$item['id'].'" class="'.$item['class'].'"'.$additional_tags.'><a class="nav-link" href="'.$target_url.'">'.$item['title'].'</a>';
				}

			break;
			case("footer_menu"):
				if($item["type"]==3){
					$target_url = $item['slug'];
				}else{
					$parameters = array_merge([$extra_parameter], $parent_slugs, [$item['slug']]);
					$target_url = action("PageController@render", $parameters);
				}

				if($i == 1) {
					$seperator = '<li class="desktop-only">|</li>';
				} else if($i != 0 ) {
					$seperator = '';
				} else {
					$seperator = '<li class="mobile">|</li>';
				}
				if(is_array($item['children'])){
					$html .= '<li id="'.$item['id'].'" class="dropdown"'.$additional_tags.'><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'.$item['title'].' <span class="caret"></span></a>';
				}else{
					$html .= '<li id="'.$item['id'].'" class="footer-link '.$item['class'].'"'.$additional_tags.'><a href="'.$target_url.'">'.$item['title'].'</a>'.$seperator;
				}
				$i++;
			break;
			case("menu_item_readonly"):
			case("submenu_item_readonly"):
			default:
				if($item["type"]==3){
					$target_url = $item['slug'];
				}else{
					$parameters = array_merge([$extra_parameter], $parent_slugs, [$item['slug']]);
					$target_url = action("PageController@render", $parameters);
				}
				if(is_array($item['children'])){
					$html .= '<li id="'.$item['id'].'" class="dropdown"'.$additional_tags.'><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'.$item['title'].' <span class="caret"></span></a>';
				}else{
					$html .= '<li id="'.$item['id'].'" class="'.$item['class'].'"'.$additional_tags.'><a href="'.$target_url.'">'.$item['title'].'</a>';
				}
			break;

		}
		if(is_array($item['children'])) $html .= html_list($item['children'], $children_list_type, $extra_parameter, array_merge($parent_slugs, [$item["slug"]]));
		$html .= '</li>';
	}
	$html .= '</ul>';
	if($list_type=="page" || $list_type=="menu_item") $html .= '<input type="hidden" name="list_order['.$list[0]["parent_id"].']" value="">';
	return $html;
}

function create_nested_selectbox_data(Array $list, $level = 1, $data = [], $max_level = 0){
	foreach($list as $item){
		$data[$item['id']] = str_repeat("- ", $level) . $item['title'];
		if(is_array($item['children']) && ($max_level==0 || $level < $max_level)) $data += create_nested_selectbox_data($item['children'], $level+1);
	}
	return $data;
}

function get_blocks_html($blocks){
	$html = "";
	foreach($blocks as $block){
		$block_file = "Theme::blocks.".$block->block_type;
		if(!view()->exists($block_file)) $block_file = "Theme::blocks.default";
		$data = json_decode($block->data);
		$data->elements = $block->elements;
		$html .= view($block_file, ["data" => $data]);
	}
	return $html;
}
