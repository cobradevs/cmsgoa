<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model 
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'website_id'
    ];

    public function website()
    {
        return $this->belongsTo('App\Website');
    }
}
