<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    
    protected $fillable = ['website_id', 'language_id', 'menu_id', 'parent_id', 'page_id', 'category_id', 'external_url', 'title', 'type', 'rank'];

	public function website()
    {
        return $this->belongsTo('App\Website');
    }

    public function parent()
    {
        return $this->belongsTo('App\MenuItem', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\MenuItem', 'parent_id')->orderBy("rank");
    }

    public function page()
    {
        return $this->belongsTo('App\Page');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

}
