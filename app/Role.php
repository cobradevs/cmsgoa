<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	public function users()
    {
        return $this->hasMany('App\User');
    }

    public static function list_for_selectbox($list = [])
    {
        return $list + static::orderBy('name','asc')->get()->pluck('name','id')->toArray();
    }
}
