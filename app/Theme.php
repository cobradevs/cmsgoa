<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'folder', 'screenshot', 'datasource', 'templates', 'options', 'rank'
    ];

    public function setTemplatesAttribute($templates)
    {
    	$templates_sanitized = [];
    	foreach($templates as $template){
    		$template_sanitized["template"] = [];
    		$template_sanitized["template"]["id"] = str_slug($template["name"]);
    		$template_sanitized["template"]["name"] = $template["name"];
    		foreach($template["regions"] as $region_name){
    			$template_sanitized["template"]["regions"][str_slug($region_name)] = $region_name;
    		}
    		$templates_sanitized[] = $template_sanitized;
    	}
        $this->attributes['templates'] = json_encode($templates_sanitized);
    }

    public function setOptionsAttribute($options)
    {
    	$options_sanitized = [];
    	foreach($options as $option){
    		if($option["name"]){
	    		$option_sanitized["option"] = [];
	    		$option_sanitized["option"]["id"] = str_slug($option["name"]);
	    		$option_sanitized["option"]["name"] = $option["name"];
	    		$option_sanitized["option"]["type"] = $option["type"];
	    		$options_sanitized[] = $option_sanitized;
    		}
    	}
        $this->attributes['options'] = json_encode($options_sanitized);
    }

	public function websites()
    {
        return $this->hasMany('App\Website');
    }

    public static function list_for_selectbox($list = [])
    {
        return $list + static::orderBy('rank','asc')->orderBy('name','asc')->get()->pluck('name','id')->toArray();
    }
}
