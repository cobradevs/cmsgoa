<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['language_id', 'name', 'slug'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

	public function language()
    {
        return $this->belongsTo('App\Language');
    }

	public function elements()
    {
        return $this->hasMany('App\Element');
    }

    public static function list_for_selectbox($list = [], $language_id = null)
    {
    	$query = static::where("id", "<>", 1)->orderBy('name','asc');
    	if($language_id) $query->where("language_id", $language_id);
        return $list + $query->pluck('name','id')->toArray();
    }
}
