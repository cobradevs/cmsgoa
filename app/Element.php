<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;

class Element extends Model
{
    use Sluggable;
    // use RecordsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['language_id', 'website_id', 'category_id', 'title', 'subtitle', 'slug', 'excerpt', 'body', 'image','alttext', 'thumb', 'meta_title', 'meta_description', 'other_meta_tags', 'read_count', 'rank', 'status', 'published_date'];

    public function setPublishedDateAttribute($published_date)
    {
        $this->attributes['published_date'] = date("Y-m-d", strtotime($published_date));
    }

    public function getPublishedDateAttribute($published_date)
    {
        return date("d/m/Y", strtotime($published_date));
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

	public function website()
    {
        return $this->belongsTo('App\Website');
    }

	public function language()
    {
        return $this->belongsTo('App\Language');
    }

	public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function scopeList($query, $take = 3) {
        return $query->orderBy('created_at', 'desc')->take($take)->get();
    }

}
