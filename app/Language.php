<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'country', 'locale', 'slug', 'is_active'
    ];

	public function default_websites()
    {
        return $this->hasMany('App\Website', 'default_language_id');
    }

	public function websites()
    {
        return $this->belongsToMany('App\Website')->withPivot('homepage_id','settings');
    }

	public function categories()
    {
        return $this->hasMany('App\Category');
    }

	public function elements()
    {
        return $this->hasMany('App\Element');
    }

    public function pages()
    {
        return $this->hasMany("App\Page");
    }

    public function menus()
    {
        return $this->hasMany("App\Menu");
    }

    public static function list_for_selectbox($list = [])
    {
        return $list + static::orderBy('rank','asc')->orderBy('name','asc')->get()->pluck('name','id')->toArray();
    }
}
