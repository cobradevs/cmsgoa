<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct (Request $request)
    {
		$domain = Domain::where("name", "=", parse_url($request->root())["host"])->get()->first();
    	if(!$domain)
    		App::abort(404, 'This domain is not hosted on this application');

    	if(!$domain->website->theme->folder)
    		App::abort(404, 'The theme set up for this website is broken');

    	$theme_folder = base_path() . '/resources/views/themes/' . $domain->website->theme->folder;
    	if(!File::exists($theme_folder))
    		App::abort(404, 'The theme set up for this website is broken');

    	view()->addNamespace('Theme', $theme_folder);
    }
}
