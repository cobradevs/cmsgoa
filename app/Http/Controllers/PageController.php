<?php

namespace App\Http\Controllers;

use App\Page;
use App\Element;
use App\Category;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;

class PageController extends BaseFrontendController
{
    public function render()
    {

    	$this->breadcrumbs[$this->lang_slug] = trans('cms.Home');

    	if($this->segments[1]){

	    	$element_category = Category::where("slug", $this->segments[1])->first();

	    	if($element_category){

				$this->breadcrumbs[implode("/",[$this->lang_slug,$this->segments[1]])] = $element_category->name;

	    		if($this->segments[2]){
		    		$element = Element::where("website_id", $this->website->id)
		    							->where("language_id", $this->language->id)
		    							->where("category_id", $element_category->id)
		    							->where("slug", $this->segments[2])
		    							->first();
		    		if(!$element) abort(404, "Page not found");
		    		$this->breadcrumbs[implode("/",[$this->lang_slug,$this->segments[1],$this->segments[2]])] = $element->title;
		    		return $this->element_show($element);
	    		}

	    		return $this->element_index($element_category);

	    	}

    		$page = Page::where("website_id", $this->website->id)
    						->where("language_id", $this->language->id)
    						->where("parent_id", 0)
    						->where("slug", $this->segments[1])
    						->first();

    		if(!$page) abort(404, "Page not found");
    		$this->breadcrumbs[implode("/",[$this->lang_slug,$this->segments[1]])] = $page->title;
			$count = count(array_filter($this->segments)) +1;

	    	for($i=$count; $i<=5; $i++){
	    		if($this->segments[$i]){
	    			$page = $page->children->where("slug", $this->segments[$i])->first();
	    			$this->breadcrumbs[implode("/",array_merge([$this->lang_slug], array_slice($this->segments,0,$i)))] = $page->title;
	    			if(!$page) abort(404, "Page not found");
	    		}
	    	}
    		return $this->page_show($page);

	    }

	    if(isset($this->language->pivot->homepage_id) || json_decode($this->website->languages->find($this->language->id)->pivot->homepage_id, true)){
	    	$homepage_id = json_decode($this->website->languages->find($this->language->id)->pivot->homepage_id, true)?json_decode($this->website->languages->find($this->language->id)->pivot->homepage_id, true):$this->language->pivot->homepage_id;
            $homepage = Page::find($homepage_id);
	    	if(!$homepage) abort(404, "Page not found");
	    	return $this->page_show($homepage);
	    }

    	return $this->page_index();
    }

    /**
     * Show index of pages when no homepage is set
     */
    public function page_index()
    {

    	$meta["title"] = $this->website->name;
    	$meta["description"] = $this->website->name;

    	return view("Theme::pages.index", compact("meta"));
    }

    /**
     * Show page
     */
    public function page_show(Page $page)
    {

    	$template_regions = array_pluck(json_decode($this->website->theme->templates, true), 'template.regions', 'template.id');
    	if(!isset($template_regions[$page->template]))
    		abort(404, "The template set up for this page is broken");

		$regions = $template_regions[$page->template];
    	foreach($regions as $region_id => $region_title){
    		$blocks[$region_id] = $page->blocks->where("region", $region_id);
    	}
		$element_blocks = $page->blocks->where("block_type", "elements");
		foreach($element_blocks as $element_block){
			$data = json_decode($element_block->data);
			$element_block->elements = $this->website->elements->where("category_id", $data->category)->whereIn("id", $data->list);
		}

    	$meta["title"] = (($page->meta_title) ? ($page->meta_title) : $page->title) . " | " . $this->website->name;
    	$meta["description"] = (($page->meta_description) ? ($page->meta_description) : $page->title . " | " . $this->website->name);
    	$breadcrumbs = $this->breadcrumbs;

    	$view_file = "Theme::pages.".$page->template;
    	if(!view()->exists($view_file)) $view_file = "Theme::pages.default";
    	return view($view_file, compact("page","regions","blocks","meta","breadcrumbs"));
    }

    /**
     * Show index of elements (news list page etc.)
     */
    public function element_index(Category $category)
    {
    	$elements = $category->elements->where("website_id", $this->website->id)->where("language_id", $this->language->id);
    	$top_elements = $elements->sortByDesc("rand")->slice(0, 5);

    	$meta["title"] = $category->name . " | " . $this->website->name;
    	$meta["description"] = $category->name . " | " . $this->website->name;
    	$breadcrumbs = $this->breadcrumbs;

    	$view_file = "Theme::elements.index-".$category->slug;
		if(!view()->exists($view_file)) $view_file = "Theme::elements.index-default";
    	return view($view_file, compact("category","elements","top_elements","meta","breadcrumbs"));
    }

    /**
     * Show element details page (news details page etc.)
     */
    public function element_show(Element $element)
    {
    	$meta["title"] = (($element->meta_title) ? ($element->meta_title) : $element->title) . " | " . $this->website->name;
    	$meta["description"] = (($element->meta_description) ? ($element->meta_description) : $element->title) . " | " . $this->website->name;
    	$breadcrumbs = $this->breadcrumbs;

    	if(!$this->request->session()->has("viewed.element.".$element->id)){
    		$element->read_count = $element->read_count + 1;
    		$element->save();
    		$this->request->session()->put("viewed.element.".$element->id, true);
    	}

    	$top_elements = $element->category->elements->where("website_id", $this->website->id)->where("language_id", $this->language->id)->sortByDesc("rand")->slice(0, 5);

    	$view_file = "Theme::elements.show-".$element->category->slug;
		if(!view()->exists($view_file)) $view_file = "Theme::elements.show-default";
    	return view($view_file, compact("element","top_elements","meta","breadcrumbs"));
    }
}
