<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;

class BaseFrontendController extends Controller
{
	protected $website, $language, $lang_slug, $segments, $breadcrumbs;

    public function __construct (Request $request)
    {
    	/*
    		This is needed to use session in functions in the controllers that extends this BaseFrontendController
    	 */
    	$this->request = $request;

		$domain = Domain::where("name", "=", parse_url($request->root())["host"])->get()->first();

    	if(!$domain)
    		App::abort(404, 'This domain is not hosted on this application');

    	if(!$domain->website->theme->folder || !$domain->website->theme->templates)
    		App::abort(404, 'The theme set up for this website is broken');

    	$theme_folder = base_path() . '/resources/views/themes/' . $domain->website->theme->folder;
    	if(!File::exists($theme_folder))
    		App::abort(404, 'The theme set up for this website is broken');

    	$this->website = $domain->website;
    	view()->addNamespace('Theme', $theme_folder);

    	if(count($this->website->languages) > 1){ // Website is multi-lingual

    		// If the first slug of the URL is not a valid language, redirect to website's default language
    		if(!in_array($request->lang, $this->website->languages->pluck('slug')->toArray()))
    			return redirect($this->website->default_language->slug)->send();

    		// Else, assign the language to the application
    		$this->language = $this->website->languages->where("slug", $request->lang)->first();
    		$this->lang_slug = $this->language->slug;
    		$slugs_start_at = 1;

    	}else{ // Website is monolingual, no slug needed

    		$this->language = $this->website->default_language;
    		$this->lang_slug = null;
    		$slugs_start_at = 0;

    	}

    	for($i=1; $i<=5; $i++){
    		$this->segments[$i] = $request->segment($slugs_start_at + $i);
    	}

    	App::setLocale($this->language->slug);

    	$this->pages = $this->website->pages->where("language_id", $this->language->id)->where("status", 100);

    	View::share('website', $this->website);
    	View::share('pages', $this->pages);
    	View::share('settings', json_decode($this->website->languages->find($this->language->id)->pivot->settings, true));
    	View::share('language', $this->language);
    	View::share('lang_slug', $this->lang_slug);
    	View::share('segments', $this->segments);

    	$menu_items = [];
    	foreach(menus() as $menu_id => $menu_title){
    		$menu_items[$menu_id] = list_nested($this->website->menu_items->where("language_id", $this->language->id)->where("menu_id", $menu_id)->where("parent_id", 0), $this->segments);
    	}
    	View::share('menu_items', $menu_items);

    }

}
