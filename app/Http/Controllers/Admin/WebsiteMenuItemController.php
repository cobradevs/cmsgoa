<?php

namespace App\Http\Controllers\Admin;

use Image;
use Datatables;
use App\Category;
use App\MenuItem;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Requests\WebsiteMenuItemRequest;

class WebsiteMenuItemController extends BaseEditorController
{
	protected $menu_id;

	public function __construct(Request $request)
	{
    	parent::__construct($request);
		$this->menu_id = intval($request->menu);
    	View::share('menu_id', $this->menu_id);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$menu_items = $this->get_menu_items();
        return view("Admin::websitemenuitems.index", compact('menu_items'));
    }

    /**
     * Save order of menu items
     *
     * @return \Illuminate\Http\Response
     */
    public function save_order(Request $request)
    {
    	foreach($request->input("list_order") as $parent_id => $items_order){
    		$item_ids = explode(",", $items_order);
    		$order = 0;
    		foreach($item_ids as $item_id){
    			if($item = MenuItem::find($item_id)){
	    			$item->rank = ++$order;
	    			$item->save();
    			}
    		}
    	}

    	flash('success', 'Congrats!', 'Menu order saved successfully');
    	return redirect()->action("Admin\WebsiteMenuItemController@index", [$this->website->id, $this->language->id, $this->menu_id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$menu_items = $this->get_menu_items();
    	$pages = list_nested($this->website->pages->where("parent_id", 0)->where("language_id", $this->language->id));
    	$categories = Category::list_for_selectbox([1 => "News"], $this->language->id);

        return view("Admin::websitemenuitems.create", compact('menu_items','pages','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WebsiteMenuItemRequest $request)
    {
    	$menu_item = new MenuItem($request->all());
    	$menu_item->website_id = $this->website->id;
    	$menu_item->language_id = $this->language->id;
    	$menu_item->menu_id = $this->menu_id;
    	$menu_item->save();

    	flash('success', 'Congrats!', 'Menu Item created successfully');
    	return redirect()->action("Admin\WebsiteMenuItemController@index", [$this->website->id, $this->language->id, $this->menu_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($website_id, $language_id, $id)
    {
    	$menu_item = MenuItem::findOrFail($id);
        return view("Admin::websitemenuitems.show", compact('menu_item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($website_id, $language_id, $menu_id, $id, $save_type="update")
    {
    	$menu_item = MenuItem::findOrFail($id);

    	$menu_items = $this->get_menu_items();
    	$pages = list_nested($this->website->pages->where("parent_id", 0)->where("language_id", $this->language->id));
    	$categories = Category::list_for_selectbox([1 => "News"], $this->language->id);
    	
        return view("Admin::websitemenuitems.edit", compact('menu_item','menu_items','pages','categories','save_type'));
    }

    /**
     * Show the form for editing the specified resource and saving it as a new record.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function duplicate($website_id, $language_id, $menu_id, $id)
    {
    	return $this->edit($website_id, $language_id, $menu_id, $id, "duplicate");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WebsiteMenuItemRequest $request, $website_id, $language_id, $menu_id, $id)
    {
        $menu_item = MenuItem::findOrFail($id);
        $menu_item->fill($request->all())->save();

    	flash('success', 'Congrats!', 'Menu Item updated successfully');
    	return redirect()->action("Admin\WebsiteMenuItemController@edit", [$this->website->id, $this->language->id, $this->menu_id, $menu_item->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($website_id, $language_id, $menu_id, $id)
    {
        MenuItem::findOrFail($id)->delete();
        flash('success', 'Congrats!', 'Menu item deleted successfully');
        return redirect()->action("Admin\WebsiteMenuItemController@index", [$this->website->id, $this->language->id, $this->menu_id]);
    }

    private function get_menu_items()
    {
    	return list_nested($this->website->menu_items->where("language_id", $this->language->id)->where("menu_id", $this->menu_id)->where("parent_id", 0));
    }
    
}
