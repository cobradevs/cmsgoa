<?php

namespace App\Http\Controllers\Admin;

use Image;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\WebsitePageRequest;

class WebsiteAssetController extends BaseEditorController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("Admin::websiteassets.index");
    }

}
