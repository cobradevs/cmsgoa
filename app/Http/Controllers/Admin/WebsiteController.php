<?php

namespace App\Http\Controllers\Admin;

use App\Website;
use App\Theme;
use App\Language;
use App\Domain;

use Image;
use Datatables;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\WebsiteRequest;

class WebsiteController extends Controller
{
	/**
	 * Datatables data getting method
	 * @return json data in json format
	 */
	public function data()
	{
		$datatables = Datatables::of(Website::with('theme')->select('*'));
        $datatables->addColumn('action', function ($item) {
            return action_buttons("show,edit,duplicate,delete", $item, "Website");
        });
        $datatables->editColumn('logo', function ($item) {
            return '<img src="'.asset($item->logo).'" class="img-thumbnail img-negative">';
        });
        return $datatables->make(true);
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $websites = Website::all();
        return view("Admin::websites.index", compact('websites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$themes = Theme::list_for_selectbox(['' => 'Select a theme']);
    	$languages = Language::list_for_selectbox();
    	$languages_with_selector = Language::list_for_selectbox(['' => 'Select a language']);
        return view("Admin::websites.create", compact('themes','languages','languages_with_selector'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WebsiteRequest $request)
    {
    	$website = new Website($request->all());
    	$website->save();
    	$website->languages()->attach($request->get('languages'));

    	if($request->hasFile("logo") && $request->file("logo")->isValid()) $this->upload_file($website, $request->file("logo"), "logo");
    	if($request->hasFile("image") && $request->file("image")->isValid()) $this->upload_file($website, $request->file("image"), "image");
    	if($request->hasFile("favicon") && $request->file("favicon")->isValid()) $this->upload_file($website, $request->file("favicon"), "favicon");

    	if($request->get("clone_pages")){
    		$cloned_ids = [];
    		$cloned_ids[0] = 0;
    		$clone_website = Website::findOrFail($request->get("clone_pages"));
    		foreach($clone_website->pages->sortBy("parent_id") as $page){
    			$cloned_page = $website->pages()->create($page->toArray());
    			$cloned_ids[$page->id] = $cloned_page->id;
    			$cloned_page->parent_id = $cloned_ids[$page->parent_id];
    			$cloned_page->save();
    		}
    	}

    	if($request->get("clone_elements")){
    		$clone_website = Website::findOrFail($request->get("clone_elements"));
    		foreach($clone_website->elements->sortBy("parent_id") as $element){
    			$cloned_element = $website->elements()->create($element->toArray());
    		}
    	}

    	flash('success', 'Congrats!', 'Website created successfully');
    	return redirect()->action("Admin\WebsiteController@index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try
        {
            $website = Website::findOrFail($id);
            $domains = $website->domains->pluck('id', 'name')->toArray();  
            return view("Admin::websites.show", compact('website', 'domains'));
        }
        catch(ModelNotFoundException $e)
        {
             flash('error', 'Error!', 'Please try again');
             return view("Admin::websites.show");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $save_type="update")
    {
    	$themes = Theme::list_for_selectbox(['' => 'Select a theme']);
    	$languages = Language::list_for_selectbox();
    	$languages_with_selector = Language::list_for_selectbox(['' => 'Select a language']);

    	$website = Website::findOrFail($id);
    	if($save_type=="duplicate") $website->name = "Copy of ".$website->name;

        $domains = $website->domains->pluck('id', 'name')->toArray();  
    	$website_languages = $website->languages->pluck('id')->toArray();
    	foreach($website_languages as $website_language){
    		$website_pages[$website_language] = list_nested($website->pages->where("parent_id", 0)->where("language_id", $website_language));
    	}
        return view("Admin::websites.edit", compact('website','themes','domains', 'languages','languages_with_selector','website_languages','website_pages','save_type'));

    }

    /**
     * Show the form for editing the specified resource and saving it as a new record.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function duplicate($id)
    {
    	return $this->edit($id, "duplicate");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WebsiteRequest $request, $id)
    {
        $website = Website::findOrFail($id);
        if($website->fill($request->all())->save()) {

        	$languages_with_homepage_ids = [];
        	foreach($request->get('languages') as $website_language){
        		$homepage_id = (isset($request->get('homepage_id')[$website_language])) ? $request->get('homepage_id')[$website_language] : 0;
        		$languages_with_homepage_ids[$website_language] = [
        			"homepage_id" => $homepage_id
        		];
        	}
            $website->languages()->sync($languages_with_homepage_ids);

            if($request->hasFile("logo") && $request->file("logo")->isValid()) $this->upload_file($website, $request->file("logo"), "logo");
            if($request->hasFile("image") && $request->file("image")->isValid()) $this->upload_file($website, $request->file("image"), "image");
            if($request->hasFile("favicon") && $request->file("favicon")->isValid()) $this->upload_file($website, $request->file("favicon"), "favicon");

            flash('success', 'Congrats!', 'Website updated successfully');
            return redirect()->action('Admin\WebsiteController@edit', ['id' => $website->id]);

        }
        
        flash('error', 'Error!', 'Please try again');
        return redirect()->action('Admin\WebsiteController@edit', ['id' => $website->id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Website::findOrFail($id)->delete();
        flash('success', 'Congrats!', 'Website deleted successfully');
        return redirect()->action("Admin\WebsiteController@index");
    }

    /**
     * Upload file, resize and thumbnail it
     * @param  object $website      Website
     * @param  object $file File instance
     * @return null             
     */
    public function upload_file($website, $file, $type="logo")
    {
    	$extension = $file->getClientOriginalExtension();
    	$file_directory = 'img/websites';
		$file_filename = 'website-' . $website->id . '-' . $type . '.' . $extension;
		$file->move($file_directory, $file_filename);
    	$website->$type = $file_directory . '/' . $file_filename;
    	$website->save();
    }

}
