<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class WebsiteNewsController extends WebsiteElementController
{
    
    public function __construct (Request $request)
    {
    	$this->is_news = true;
    	$this->item_title = "News";
        parent::__construct($request);
    }
    
}
