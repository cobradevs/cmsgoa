<?php

namespace App\Http\Controllers\Admin;

use App\Website;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StaticPageController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
    	if(Auth::user() && Auth::user()->isEditor()){
    		if(count(Auth::user()->websites) == 1){
	    		$website = Auth::user()->websites[0];
	    		return redirect()->action('Admin\WebsitePageController@index', $website->id);
    		}
    		$websites = Auth::user()->websites;
    	}
    	if(Auth::user() && Auth::user()->isAdmin()){
    		$websites = Website::all();
    	}
        return view("Admin::staticpages.home", compact('websites'));
    }
}
