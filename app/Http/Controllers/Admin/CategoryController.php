<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Language;

use Datatables;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
	/**
	 * Datatables data getting method
	 * @return json data in json format
	 */
	public function data()
	{
		$datatables = Datatables::of(Category::with('language')->select('*'));
        $datatables->addColumn('action', function ($item) {
            return action_buttons("show,edit,delete", $item, "Category");
        });
        return $datatables->make(true);
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view("Admin::categories.index", compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$languages = Language::list_for_selectbox();
        return view("Admin::categories.create", compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
    	$category = new Category($request->all());
    	$category->save();

    	flash('success', 'Congrats!', 'Category created successfully');
    	return redirect()->action("Admin\CategoryController@index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$category = Category::findOrFail($id);
        return view("Admin::categories.show", compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$category = Category::findOrFail($id);
    	$languages = Language::list_for_selectbox();
        return view("Admin::categories.edit", compact('category','languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->fill($request->all())->save();

        flash('success', 'Congrats!', 'Category updated successfully');
        return redirect()->action('Admin\CategoryController@edit', ['id' => $category->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::findOrFail($id)->delete();
        flash('success', 'Congrats!', 'Category deleted successfully');
        return redirect()->action("Admin\CategoryController@index");
    }

}
