<?php

namespace App\Http\Controllers\Admin;

use App\Domain;

use Datatables;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\DomainRequest;
	

/**
* links the website to correct domain
*/
class DomainController extends Controller
{
	
     /**
	 * Datatables data getting method
	 * @return json data in json format
	 */
    public function data() {
		$datatables = Datatables::of(Domain::with('name')->select('*'));
        return $datatables->make(true);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $domains = Domain::all();
        return view("Admin::domains.index", compact('domains'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view("Admin::domains.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DomainRequest $request)
    {

    	$domain = new Domain($request->all());
    	$domain->save();
    	flash('success', 'Congrats!', 'Domain name has been created');
    	return redirect()->action(
    		'Admin\WebsiteController@edit', ['id' => $request->website_id]
		);
 
    }

   /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

    	if(isset($id) && !empty($id)) {
    		$domain = Domain::find($id);
    		return view('Admin::domains.edit', compact('domain'));
    	} else {
	    	flash('alert', 'not found!', 'Couldn\'t find the domain');
	    	return redirect()->action(
	    		'Admin\WebsiteController@index'
			);		
    	}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DomainRequest $request, $id) {
        $domain = Domain::findOrFail($id);

         $domain->fill($request->all())->save();
         flash('success', 'Updated!', 'Domain updated successfully');
         return redirect()->action('Admin\WebsiteController@edit', ['id' => $domain->website_id]);

    }

      /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$domain = Domain::findOrFail($id);
        return view("Admin::domain.show", compact('domain'));
    }

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$domain = Domain::findOrFail($id);
    	$domain->delete();
        flash('success', 'Record Deleted!', 'Website deleted successfully');
        return redirect()->action('Admin\WebsiteController@edit', ['id' => $domain->website_id]);   	
    	
    }

}