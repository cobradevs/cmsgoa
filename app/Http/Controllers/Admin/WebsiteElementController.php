<?php

namespace App\Http\Controllers\Admin;

use Image;
use Datatables;
use App\Element;
use App\Category;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Requests\WebsiteElementRequest;

class WebsiteElementController extends BaseEditorController
{
	protected $is_news = false;
	protected $item_title = "Element";

	public function __construct (Request $request)
	{
		View::share('is_news', $this->is_news);
		View::share('item_title', $this->item_title);
	    parent::__construct($request);
	}
	
	/**
	 * Datatables data getting method
	 * @return json data in json format
	 */
	public function data()
	{
		$query = Element::select('*')->where("elements.website_id", $this->website->id)->where("elements.language_id", $this->language->id)->with('website','language','category');
		if($this->is_news)
			$query->where("category_id", 1);
		else
			$query->where("category_id", "!=", 1);
		$datatables = Datatables::of($query);
        $datatables->addColumn('action', function ($item) {
            return action_buttons("show,edit,duplicate,delete", [$this->website->id, $this->language->id, $item->id], "Website".$this->item_title);
        });
        return $datatables->make(true);
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("Admin::websiteelements.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	if(!$this->is_news) $categories = Category::list_for_selectbox(['' => 'Select a category'], $this->language->id);
	    return view("Admin::websiteelements.create", compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WebsiteElementRequest $request)
    {
    	$element = new Element($request->all());
    	$element->website_id = $this->website->id;
    	$element->language_id = $this->language->id;
    	if($this->is_news) $element->category_id = 1;
    	$element->save();

    	flash('success', 'Congrats!', $this->item_title.' created successfully');
    	return redirect()->action("Admin\Website".$this->item_title."Controller@index", [$this->website->id, $this->language->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($website_id, $language_id, $id)
    {
    	$element = Element::findOrFail($id);
        return view("Admin::websiteelements.show", compact('element'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($website_id, $language_id, $id, $save_type="update")
    {
    	$element = Element::findOrFail($id);
    	if(!$this->is_news) $categories = Category::list_for_selectbox(['' => 'Select a category'], $this->language->id);
        return view("Admin::websiteelements.edit", compact('element','categories','save_type'));
    }


    /**
     * Show the form for editing the specified resource and saving it as a new record.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function duplicate($website_id, $language_id, $id)
    {
    	return $this->edit($website_id, $language_id, $id, "duplicate");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WebsiteElementRequest $request, $website_id, $language_id, $id)
    {
        $element = Element::findOrFail($id);
        $element->fill($request->all())->save();

        flash('success', 'Congrats!', $this->item_title.' updated successfully');
        return redirect()->action('Admin\Website'.$this->item_title.'Controller@edit', [$this->website->id, $this->language->id, $element->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($website_id, $language_id, $id)
    {
        Element::findOrFail($id)->delete();
        flash('success', 'Congrats!', 'Element deleted successfully');
        return redirect()->action("Admin\WebsiteElementController@index", [$this->website->id, $this->language->id]);
    }

    /**
     * Upload file, resize and thumbnail it
     * @param  object $element      Element
     * @param  object $file File instance
     * @return null             
     */
    public function upload_file($element, $file, $type="logo")
    {
    	$extension = $file->getClientOriginalExtension();
    	$file_directory = 'img/elements';
		$file_filename = 'element-' . $element->id . '-' . $type . '.' . $extension;
		$file->move($file_directory, $file_filename);
    	$element->$type = $file_directory . '/' . $file_filename;
    	$element->save();
    }

}
