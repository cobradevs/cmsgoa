<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;
use App\Website;
use Yajra\Datatables\Datatables;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
	/**
	 * Datatables data getting method
	 * @return json data in json format
	 */
	public function data()
	{
		$datatables = Datatables::of(User::with('role')->select('*'));
        $datatables->addColumn('action', function ($item) {
            return action_buttons("show,edit,delete", $item, "User");
        });
        return $datatables->make(true);
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$roles = Role::list_for_selectbox(['' => 'Select a role']);
        $users = User::all();
        return view("Admin::users.index", compact('roles','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$roles = Role::list_for_selectbox(['' => 'Select a role']);
    	$websites = Website::list_for_selectbox();
        return view("Admin::users.create", compact('roles','websites'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
    	$user = new User($request->all());
    	$user->save();
    	if($request->has('websites')) $user->websites()->sync($request->get('websites'));

    	flash('success', 'Congrats!', 'User created successfully');
    	return redirect()->action("Admin\UserController@index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$roles = Role::list_for_selectbox(['' => 'Select a role']);
    	$user = User::findOrFail($id);
        $activities = $user->activity->toArray();
        return view("Admin::users.show", compact('roles','user','activities'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$roles = Role::list_for_selectbox(['' => 'Select a role']);
    	$websites = Website::list_for_selectbox();
    	
    	$user = User::findOrFail($id);
    	$user_websites = $user->websites->pluck('id')->toArray();
        return view("Admin::users.edit", compact('roles','user','websites','user_websites'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
    	$request_to_pass = ($request->input('change_password') && strlen($request->input('password'))>0) ? $request->all() : $request->except('password');

        $user = User::findOrFail($id);
        $user->fill($request_to_pass)->save();
        if($request->has('websites')) $user->websites()->sync($request->get('websites'));

        flash('success', 'Congrats!', 'User updated successfully');
        return redirect()->action('Admin\UserController@edit', ['id' => $user->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();
        flash('success', 'Congrats!', 'User deleted successfully');
        return redirect()->action("Admin\UserController@index");
    }
}
