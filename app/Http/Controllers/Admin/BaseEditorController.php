<?php

namespace App\Http\Controllers\Admin;

use App\Website;
use App\Language;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class BaseEditorController extends Controller
{
	protected $websites, $website, $language;

	public function __construct(Request $request)
	{
		$this->website = Website::find(intval($request->website_id));
		$this->language = Language::find(intval($request->language_id));

    	View::share('website', $this->website);
    	View::share('language', $this->language);
    	
		if(Auth::user()){
			$this->websites = Auth::user()->websites;
    		View::share('websites', $this->websites);
		}
	}
}
