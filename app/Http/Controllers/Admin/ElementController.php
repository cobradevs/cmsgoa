<?php

namespace App\Http\Controllers\Admin;

use Image;
use Datatables;

use App\Element;
use App\Website;
use App\Category;
use App\Language;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\ElementRequest;

class ElementController extends Controller
{
	/**
	 * Datatables data getting method
	 * @return json data in json format
	 */
	public function data()
	{
		$datatables = Datatables::of(Element::with('website','language','category')->where('language_id', 1)->select('*'));
        $datatables->addColumn('action', function ($item) {
            return action_buttons("show,edit,delete", $item, "Element");
        });
        $datatables->editColumn('logo', function ($item) {
            return '<img src="'.asset($item->logo).'" class="img-thumbnail img-negative">';
        });
        return $datatables->make(true);
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("Admin::elements.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$categories = Category::list_for_selectbox(['' => 'Select a category'], 1);
        return view("Admin::elements.create", compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ElementRequest $request)
    {
    	$element = new Element($request->all());
    	$element->language_id = 1;
    	$element->save();

    	flash('success', 'Congrats!', 'Element created successfully');
    	return redirect()->action("Admin\ElementController@index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$element = Element::findOrFail($id);
        return view("Admin::elements.show", compact('element'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$element = Element::findOrFail($id);

    	$categories = Category::list_for_selectbox(['' => 'Select a category'], 1);
        return view("Admin::elements.edit", compact('element','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ElementRequest $request, $id)
    {
        $element = Element::findOrFail($id);
        $element->fill($request->all())->save();

        flash('success', 'Congrats!', 'Element updated successfully');
        return redirect()->action('Admin\ElementController@edit', ['id' => $element->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Element::findOrFail($id)->delete();
        flash('success', 'Congrats!', 'Element deleted successfully');
        return redirect()->action("Admin\ElementController@index");
    }

    /**
     * Upload file, resize and thumbnail it
     * @param  object $element      Element
     * @param  object $file File instance
     * @return null             
     */
    public function upload_file($element, $file, $type="logo")
    {
    	$extension = $file->getClientOriginalExtension();
    	$file_directory = 'img/elements';
		$file_filename = 'element-' . $element->id . '-' . $type . '.' . $extension;
		$file->move($file_directory, $file_filename);
    	$element->$type = $file_directory . '/' . $file_filename;
    	$element->save();
    }

}
