<?php

namespace App\Http\Controllers\Admin;

use App\Theme;

use Image;
use Datatables;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ThemeRequest;

class ThemeController extends Controller
{
	/**
	 * Datatables data getting method
	 * @return json data in json format
	 */
	public function data()
	{
		$datatables = Datatables::of(Theme::select('*'));
        $datatables->addColumn('action', function ($item) {
            return action_buttons("show,edit,delete", $item, "Theme");
        });
        $datatables->editColumn('screenshot_thumbnail', function ($item) {
            return '<img src="'.asset($item->screenshot_thumbnail).'" class="img-thumbnail img-circle">';
        });
        return $datatables->make(true);
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $themes = Theme::all();
        return view("Admin::themes.index", compact('themes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Admin::themes.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ThemeRequest $request)
    {
    	$theme = new Theme($request->all());
    	$theme->save();

    	if($request->hasFile("screenshot") && $request->file("screenshot")->isValid()) $this->upload_screenshot($theme, $request->file("screenshot"));

    	flash('success', 'Congrats!', 'Theme created successfully');
    	return redirect()->action("Admin\ThemeController@index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$theme = Theme::findOrFail($id);
        return view("Admin::themes.show", compact('theme'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$theme = Theme::findOrFail($id);
        return view("Admin::themes.edit", compact('theme'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ThemeRequest $request, $id)
    {
        $theme = Theme::findOrFail($id);
        $theme->fill($request->all())->save();

    	if($request->hasFile("screenshot") && $request->file("screenshot")->isValid()) $this->upload_screenshot($theme, $request->file("screenshot"));

        flash('success', 'Congrats!', 'Theme updated successfully');
        return redirect()->action('Admin\ThemeController@edit', ['id' => $theme->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Theme::findOrFail($id)->delete();
        flash('success', 'Congrats!', 'Theme deleted successfully');
        return redirect()->action("Admin\ThemeController@index");
    }

    /**
     * Upload screenshot file, resize and thumbnail it
     * @param  object $theme      Theme
     * @param  object $screenshot File instance
     * @return null             
     */
    public function upload_screenshot($theme, $screenshot)
    {
    	$extension = $screenshot->getClientOriginalExtension();
		$screenshot_filename = 'img/theme-screenshots/theme-' . $theme->id . '.' . $extension;
		$screenshot_thumbnail_filename = 'img/theme-screenshots/theme-' . $theme->id . '-th.' . $extension;
    	Image::make($screenshot)
    		   ->fit(640, 640)
    		   ->save($screenshot_filename);
    	Image::make($screenshot)
    		   ->fit(120, 120)
    		   ->save($screenshot_thumbnail_filename);
    	$theme->screenshot = $screenshot_filename;
    	$theme->screenshot_thumbnail = $screenshot_thumbnail_filename;
    	$theme->save();
    }
}
