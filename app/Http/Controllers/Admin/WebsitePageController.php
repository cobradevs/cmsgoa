<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use App\Category;

use Image;
use Datatables;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\WebsitePageRequest;

class WebsitePageController extends BaseEditorController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$pages = list_nested($this->website->pages->where("parent_id", 0)->where("language_id", $this->language->id));
        return view("Admin::websitepages.index", compact('pages'));
    }

    /**
     * Save order of menu items
     *
     * @return \Illuminate\Http\Response
     */
    public function save_order(Request $request)
    {
    	foreach($request->input("list_order") as $parent_id => $items_order){
    		$item_ids = explode(",", $items_order);
    		$order = 0;
    		foreach($item_ids as $item_id){
    			if($item = Page::find($item_id)){
	    			$item->rank = ++$order;
	    			$item->save();
    			}
    		}
    	}

    	flash('success', 'Congrats!', 'Page order saved successfully');
    	return redirect()->action("Admin\WebsitePageController@index", [$this->website->id, $this->language->id]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$pages = list_nested($this->website->pages->where("parent_id", 0)->where("language_id", $this->language->id));
    	$templates = json_decode($this->website->theme->templates);
    	$templates_list = array_pluck(json_decode($this->website->theme->templates, true), 'template.name', 'template.id');
    	asort($templates_list);
    	$statuses = item_statuses();
    	$block_types = block_types();
    	$block_types_list = array_pluck($block_types, 'type.name', 'type.id');
    	$categories = Category::where("language_id", $this->language->id)->get();
    	$categories_list = Category::list_for_selectbox(['' => 'Select a category'], $this->language->id);

        return view("Admin::websitepages.create", compact('pages','templates','templates_list','statuses','block_types','block_types_list','categories','categories_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WebsitePageRequest $request)
    {
    	$page = new Page($request->all());
    	$page->website_id = $this->website->id;
    	$page->language_id = $this->language->id;
    	$page->save();

    	if(count($request->blocks[$request->template])){
        	foreach($request->blocks[$request->template] as $region => $blocks){
        		$block_rank = 0;
        		foreach($blocks as $block_json){

        			$block_rank++;
        			$block = json_decode($block_json);
        			$block_data = clone($block);
        			unset($block_data->block_id);

	        		$new_block = $page->blocks()->create([
	        			'block_type' => $block->block_type,
	        			'region' => $region,
	        			'rank' => $block_rank,
	        			'data' => json_encode($block_data)
	        		]);

        		}
        	}
        }

    	flash('success', 'Congrats!', 'Page created successfully');
    	return redirect()->action("Admin\WebsitePageController@index", [$this->website->id, $this->language->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($website_id, $language_id, $id)
    {
    	$page = Page::findOrFail($id);
        return view("Admin::websitepages.show", compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($website_id, $language_id, $id, $save_type="update")
    {
    	$page = Page::findOrFail($id);
    	if($save_type=="duplicate") $page->title = "Copy of ".$page->title;
    	$pages = list_nested($this->website->pages->where("parent_id", 0)->where("language_id", $this->language->id));
    	$templates = array_pluck(json_decode($this->website->theme->templates, true), 'template.name', 'template.id');
    	asort($templates);
    	$statuses = item_statuses();
    	$block_types = block_types();
    	$block_types_list = array_pluck($block_types, 'type.name', 'type.id');
    	$categories = Category::where("language_id", $this->language->id)->get();
    	$categories_list = Category::list_for_selectbox(['' => 'Select a category'], $this->language->id);

        return view("Admin::websitepages.edit", compact('page','pages','templates','statuses','block_types','block_types_list','categories','categories_list','save_type'));
    }

    /**
     * Show the form for editing the specified resource and saving it as a new record.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function duplicate($website_id, $language_id, $id)
    {
    	return $this->edit($website_id, $language_id, $id, "duplicate");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(WebsitePageRequest $request, $website_id, $language_id, $id)
    {
        $page = Page::findOrFail($id);
        $page->fill($request->all())->save();
        if(count($request->blocks[$request->template])){
        	foreach($request->blocks[$request->template] as $region => $blocks){
        		$region_block_ids = [];
        		$block_rank = 0;
        		foreach($blocks as $block_json){

        			$block_rank++;
        			$block = json_decode($block_json);
        			$block_data = clone($block);
        			unset($block_data->block_id);

        			if($page->blocks()->find($block->block_id)){
        				$page->blocks()->find($block->block_id)->fill([
		        			'block_type' => $block->block_type,
		        			'region' => $region,
		        			'rank' => $block_rank,
		        			'data' => json_encode($block_data)
		        		])->save();
		        		$region_block_ids[] = $block->block_id;
        			}else{
		        		$new_block = $page->blocks()->create([
		        			'block_type' => $block->block_type,
		        			'region' => $region,
		        			'rank' => $block_rank,
		        			'data' => json_encode($block_data)
		        		]);
		        		$region_block_ids[] = $new_block->id;
        			}

        		}

        		$deleted_blocks = $page->blocks()->where("region", $region);
        		if(count($region_block_ids)) $deleted_blocks->whereNotIn("id", $region_block_ids);
        		$deleted_blocks->delete();
        	}
        }else{
        	$page->blocks()->delete();
        }

        flash('success', 'Congrats!', 'Page updated successfully');
        return redirect()->action('Admin\WebsitePageController@edit', [$this->website->id, $this->language->id, $page->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($website_id, $language_id, $id)
    {
        Page::findOrFail($id)->delete();
        flash('success', 'Congrats!', 'Page deleted successfully');
        return redirect()->action("Admin\WebsitePageController@index");
    }
}
