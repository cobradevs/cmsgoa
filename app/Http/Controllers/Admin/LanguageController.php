<?php

namespace App\Http\Controllers\Admin;

use App\Language;

use Datatables;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\LanguageRequest;

class LanguageController extends Controller
{
	/**
	 * Datatables data getting method
	 * @return json data in json format
	 */
	public function data()
	{
		$datatables = Datatables::of(Language::select('*')->get());
        $datatables->addColumn('action', function ($item) {
            return action_buttons("show,edit,delete", $item, "Language");
        });
        return $datatables->make(true);
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = Language::all();
        return view("Admin::languages.index", compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("Admin::languages.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LanguageRequest $request)
    {
    	$language = new Language($request->all());
    	$language->save();

    	flash('success', 'Congrats!', 'Language created successfully');
    	return redirect()->action("Admin\LanguageController@index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$language = Language::findOrFail($id);
        return view("Admin::languages.show", compact('language'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$language = Language::findOrFail($id);
        return view("Admin::languages.edit", compact('language'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LanguageRequest $request, $id)
    {
        $language = Language::findOrFail($id);
        $language->fill($request->all())->save();

        flash('success', 'Congrats!', 'Language updated successfully');
        return redirect()->action('Admin\LanguageController@edit', ['id' => $language->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Language::findOrFail($id)->delete();
        flash('success', 'Congrats!', 'Language deleted successfully');
        return redirect()->action("Admin\LanguageController@index");
    }

    public function list_categories($language_id = 0)
    {
    	if($language_id == 0) return null;
    	return Language::find($language_id)->categories()->pluck("name", "id")->toArray();
    }

    public function list_websites($language_id = 0)
    {
    	if($language_id == 0) return null;
    	return Language::find($language_id)->websites()->pluck("name", "id")->toArray();
    }
}
