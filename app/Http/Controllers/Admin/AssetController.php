<?php

namespace App\Http\Controllers\Admin;

use Image;
use App\Asset;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\ImageRequest;
use Illuminate\Support\Facades\Auth;

class AssetController extends Controller
{
    public function upload_image(ImageRequest $request)
    {
    	if($request->hasFile("file") && $request->file("file")->isValid()) {

    		$file = $request->file("file");
	    	$extension = $file->getClientOriginalExtension();
	    	$original_name = $file->getClientOriginalName();
	    	$name = str_slug(substr(str_replace(".".$extension, "", $original_name), 0, 16)) . '-' . str_random(8);

	    	$file_directory = 'img/assets/' . date("Y") . '/' . date("m");
			$file_filename = $name . '.' . $extension;
			$file_thumbnail_filename = $name . '-th.' . $extension;
			$file_path = $file_directory."/".$file_filename;
			$file_thumbnail_path = $file_directory."/".$file_thumbnail_filename;

	    	$file->move($file_directory, $file_filename);

	    	Image::make($file_path)
	    		   ->fit(120, 120)
	    		   ->save($file_thumbnail_path);
	    	Asset::create([
	    			'user_id' => Auth::user()->id,
	    			'name' => $file_filename,
	    			'original_name' => $original_name,
	    			'path' => $file_path,
	    			'thumb_path' => $file_thumbnail_path,
	    			'type' => 'image:'.$extension,
	    			'is_published' => 1
	    		]);

	    	return asset($file_path);
    	}
    }
}
