<?php

namespace App\Http\Controllers\Admin;

use Image;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\WebsitePageRequest;

class WebsiteThemeOptionController extends BaseEditorController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$option_values = json_decode($this->website->languages->find($this->language->id)->pivot->settings, true);
        return view("Admin::websitethemeoptions.index", compact('option_values'));
    }

    public function save(Request $request)
    {
		$this->website->languages()->updateExistingPivot($this->language->id, ["settings" => json_encode($request->except('_token','submit'))]);

    	flash('success', 'Congrats!', 'Website settings saved successfully');
    	return redirect()->action("Admin\WebsiteThemeOptionController@index", [$this->website->id, $this->language->id]);
    }

}
