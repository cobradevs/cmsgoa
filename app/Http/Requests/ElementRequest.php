<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ElementRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$this->merge(['status' => $this->input('status', 0)]);

		return [
            'category_id'      		=> 'required|integer',
            'title'					=> 'required'
        ];
    }
}
