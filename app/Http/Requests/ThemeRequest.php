<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ThemeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		return [
            'name'          => 'required|max:255',
            'folder'  		=> 'required|max:255',
            'screenshot'   	=> 'sometimes|file|image|max:6400'
        ];
    }
}
