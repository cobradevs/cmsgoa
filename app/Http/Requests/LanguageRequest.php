<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LanguageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$this->merge(['is_active' => $this->input('is_active', 0)]);

		return [
            'name'          => 'required|max:255',
            'country'  		=> 'required|max:255',
            'locale'  		=> 'required|max:5',
            'slug'   		=> 'required|max:5',
        ];
    }
}
