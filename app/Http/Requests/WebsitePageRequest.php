<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class WebsitePageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ($this->user()->isEditor() || $this->user()->isAdmin());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		return [
			'template'		=> 'required',
            'title'         => 'required|max:255'
        ];
    }
}
