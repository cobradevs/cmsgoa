<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Http\JsonResponse;

class ImageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'file|image|max:6400',
        ];
    }

    public function response(array $errors)
    {
    	$data["status"] = "failed";
    	$data["errors"] = $errors;
		return new JsonResponse($data, 200);
    }
}
