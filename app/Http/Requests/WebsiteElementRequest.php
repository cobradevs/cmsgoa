<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class WebsiteElementRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ($this->user()->isEditor() || $this->user()->isAdmin());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$this->merge(['status' => $this->input('status', 0)]);

		if($this->is('*/news') || $this->is('*/news/*')) return ['title' => 'required'];

		return [
            'category_id'      		=> 'required|integer',
            'title'					=> 'required'
        ];

    }
}
