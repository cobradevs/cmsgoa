<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class WebsiteMenuItemRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return ($this->user()->isEditor() || $this->user()->isAdmin());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		return [
            'title'         => 'required|max:255'
        ];
    }
}
