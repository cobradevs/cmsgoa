<?php

namespace App\Http\Requests;

use App\User;
use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$user = User::find($this->users);

	    switch($this->method())
	    {
	        case 'GET':
	        case 'DELETE':
	        {
	            return [];
	        }
	        case 'POST':
	        {
		        return [
		            'role_id'       => 'required|integer',
		            'name'          => 'required|max:255',
		            'email'         => 'required|email|max:255|unique:users,email',
		            'password'      => 'required|min:8|max:255'
		        ];
	        }
	        case 'PUT':
	        case 'PATCH':
	        {
	            return [
		            'role_id'       => 'required|integer',
		            'name'          => 'required|max:255',
		            'email'         => 'required|email|max:255|unique:users,email,'.$user->id,
		            'password'		=> 'required_if:change_password,checked|min:8|max:255'
	            ];
	        }
	        default:break;
	    }
    }
}
