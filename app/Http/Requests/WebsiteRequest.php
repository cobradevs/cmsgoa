<?php

namespace App\Http\Requests;

use App\Http\Requests\Request; 

class WebsiteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	$this->merge(['is_live' => $this->input('is_live', 0)]);

		return [
            'theme_id'      		=> 'required|integer',
            'default_language_id' 	=> 'required|integer',
            'name'          		=> 'required|max:50',
            'logo'   				=> 'sometimes|file|image|max:6400',
            'image'   				=> 'sometimes|file|image|max:6400',
            'news_per_page'			=> 'integer',
            'news_latest_limit'		=> 'integer',
            'news_mostread_limit'	=> 'integer'
        ];
    }
}
