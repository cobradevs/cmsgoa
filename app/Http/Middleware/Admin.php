<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( ($request->user() == null) || ($request->user()->role->status != 100) ) {
            return redirect()->guest('/');
        }

        return $next($request);
    }
}
