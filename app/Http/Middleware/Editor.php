<?php

namespace App\Http\Middleware;

use Closure;

class Editor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	// Admin has the right to edit everything
    	if( $request->user()->role->status == 100 ){
    		return $next($request);
    	}

    	// Editor has the right to edit only the websites they have access to
        if ( ($request->user() == null) || ($request->user()->role->status != 10) || (in_array($request->website_id, $request->user()->websites->pluck('id')->toArray()) == false) ) {
            return redirect()->guest('/');
        }
        
        return $next($request);
    }
}
