<?php

namespace App;

use Illuminate\Notifications\Notifiable as Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function websites()
    {
        return $this->belongsToMany("App\Website");
    }

    public function isAdmin()
    {
    	return $this->role->status == 100;
    }

    public function isEditor()
    {
    	return $this->role->status == 10;
    }

    public function activity()
    {
        return $this->hasMany('App\Activity')->with(['user', 'subject'])->latest();
    }

}
