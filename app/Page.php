<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;

class Page extends Model
{
    use Sluggable;
    // use RecordsActivity;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['language_id', 'website_id', 'parent_id', 'title', 'subtitle', 'slug', 'meta_title', 'meta_description', 'style_classes', 'template', 'status', 'rank'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function parent()
    {
        return $this->belongsTo('App\Page', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Page', 'parent_id')->orderBy("rank");
    }

	public function website()
    {
        return $this->belongsTo('App\Website');
    }

	public function language()
    {
        return $this->belongsTo('App\Language');
    }

	public function blocks()
    {
        return $this->hasMany('App\Block')->orderBy("rank");
    }

}
