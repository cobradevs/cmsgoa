<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    /**
     * The attributes that are mass assignable. 
     *
     * @var array
     */
    protected $fillable = [
        'theme_id', 'default_language_id', 'domain_id', 'name', 'logo', 'image', 'additional_header', 'news_per_page', 'news_latest_limit', 'news_mostread_limit', 'news_order', 'is_live'
    ];

	public function theme()
    {
        return $this->belongsTo('App\Theme');
    }
  
    
	public function default_language()
    {
        return $this->belongsTo('App\Language', 'default_language_id');
    }

    public function domains()
    {
        return $this->hasMany('App\Domain');
    }

	public function elements()
    {
        return $this->hasMany('App\Element');
    }

    public function pages()
    {
        return $this->hasMany('App\Page')->orderBy('rank');
    }

    public function menu_items()
    {
        return $this->hasMany('App\MenuItem')->orderBy('rank');
    }

    public function languages()
    {
        return $this->belongsToMany("App\Language")->withPivot('homepage_id','settings');
    }

    public function users()
    { 
        return $this->belongsToMany("App\User");
    }

    public static function list_for_selectbox()
    {
        return static::orderBy('name','asc')->get()->pluck('name','id')->toArray();
    }
    
}
