<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Block extends Model
{

    // use RecordsActivity;

	protected static function boot() {
		parent::boot();
		static::addGlobalScope('order', function (Builder $builder) {
		    $builder->orderBy('rank');
		});
	}
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['page_id', 'block_type', 'region', 'name', 'data', 'link', 'style', 'rank', 'active'];

	public function page()
    {
        return $this->belongsTo('App\Page');
    }

}
