<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'website_id', 'name', 'original_name', 'title', 'path', 'thumb_path', 'type', 'is_published'
    ];
}
