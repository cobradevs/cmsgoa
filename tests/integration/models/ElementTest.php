<?php

use App\Element;
use App\User;
use App\Language;
use App\Website;
use App\Category;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ElementTest extends PHPUnit_Extensions_Selenium2TestCase
{
	//use DatabaseTransactions;
	use WithoutMiddleware;

	/** @test */ 
	
	public function setUp()
	{
		parent::setUp(); // performs set up	
        $this->setBrowser('firefox');
        $this->setBrowserUrl('http://cmsgoa.app/');
	}
	/** @test */ 
	
	public function it_lists_the_elements()
	{
		$an_element = factory(Element::class, 3)->create(['website_id' => 2, 'language_id'=> 2, 'category_id' => 2]);

		$list = Element::list();

		$this->assertCount(3, $list);
	}

	/** @test */ 
	
	public function it_allows_editor_to_add_an_element()
	{
    	$an_editor = factory(User::class)->create(['role_id' => 10]);

   		$add_element = factory(Element::class)->create(['website_id' => 2, 'language_id'=> 2, 'category_id' => 2]);
   		$this->actingAs($an_editor);

	}


	public function it_hides_global_elements_from_the_editor() {
    	$an_editor = factory(User::class)->create(['role_id' => 10]);

    	$list_global_elements = factory(Language::class)->first();

    	$this->assertEquals(1, $list_global_elements->id);
	    	
	}
	
	/** @test */ 
	
	public function it_can_add_an_element()
	{

		$website = factory(Website::class)->create(['theme_id' => 1, 'default_language_id'=> 2]);
		$category = factory(Category::class)->create(['language_id' => 1]);
		//unable to do the test because of AJAX. It could be overkill
		$an_editor = factory(User::class)->create(['role_id' => 10]);
	
    	$this->actingAs($an_editor)
    		->withSession(['foo' => 'bar'])
    	 	->visit('admin/elements');

	}
  
  /** @test */ 
  
  public function it_can_delete_an_element()
  {
     	$an_element = factory(Element::class)->create(['website_id' => 2, 'language_id'=> 2, 'category_id' => 2]);

		// $an_editor = factory(User::class)->create(['role_id' => 10]);
    	
  //   	$this->actingAs($an_editor)
  //   		->visit('admin/elements');
  }

  /** @test */ 
  
  public function it_can_edit_an_element()
  {
    	$an_element = factory(Element::class)->create(['website_id' => 2, 'language_id'=> 2, 'category_id' => 2]);
    	$this->visit('admin/elements');
  }

/** @test */ 

public function it_checks_form_rules()
{
	$mandatory_fields =['language_id','title'];

	return $mandatory_fields;
}




}
