<?php

use App\User;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
	use WithoutMiddleware;
	// use DatabaseTransactions;

    /** @test */
    public function can_list_users()
    {
    	$an_editor = factory(User::class)->create();
    	$test_user_name = $an_editor->name;

        $this->visit('admin/users')
        	 ->see($test_user_name);
    }

    /** @test */
    public function can_add_a_user()
    {
    	$an_editor = factory(User::class)->make();

    	$this->visit('admin/users/create')
    		 ->type($an_editor->name, "name")
    		 ->type($an_editor->email, "email")
    		 ->type(str_random(10), "password")
    		 ->press('submit')
    		 ->seePageIs('users');

    	$check_user = User::where("email", $an_editor->email)->first();

    	$this->assertEquals($check_user->name, $an_editor->name);
    }

    /** @test */
    public function can_edit_a_user()
    {
    	$an_editor = factory(User::class)->create();
    	$a_different_editor = factory(User::class)->make();

    	$this->visit('admin/users/'.$an_editor->id.'/edit')
    		 ->type($a_different_editor->name, "name")
    		 ->type($a_different_editor->email, "email")
    		 ->press('submit')
    		 ->seePageIs('users/'.$an_editor->id.'/edit');

    	$updated_user = User::find($an_editor->id);

    	$this->assertEquals($updated_user->name, $a_different_editor->name);
    }

    /** @test */
    public function can_view_a_users_details()
    {
    	$an_editor = factory(User::class)->create();

    	$this->visit('admin/users/'.$an_editor->id)
    		 ->see($an_editor->name)
    		 ->see($an_editor->email); 
    }

    /** @test */
    public function can_delete_a_user()
    {
    	$an_editor = factory(User::class)->create();

    	$this->visit('admin/users/'.$an_editor->id)
    		 ->press('delete')
    		 ->seePageIs('users');

    	$check_deleted_user = User::find($an_editor->id);
    	$this->assertEquals($check_deleted_user, null);
    }

    /** @test */
    public function an_editor_cant_see_user_administration()
    {
    	$an_editor = factory(User::class)->create(['role_id' => 10]);
    	
    	$this->actingAs($an_editor)
    		 ->visit('admin/users')
    		 ->seePageIs('/');

    	$this->actingAs($an_editor)
    		 ->visit('admin/users/create')
    		 ->seePageIs('/');
    }

}
